<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package My Theme
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function es_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => false,
		'wrapper' => false,
		'render' => 'es_infinite_scroll_render',
	) );
}
add_action( 'after_setup_theme', 'es_jetpack_setup' );


function es_infinite_scroll_render(){
	while ( have_posts() ) : the_post();
		if ( is_category() ) :
			get_template_part( 'content', 'post-short' );
		
		elseif ( is_search() ) :
			get_template_part( 'content', 'search' );

		else :
			get_template_part( 'content', get_post_format() );
		endif;
	endwhile;
}