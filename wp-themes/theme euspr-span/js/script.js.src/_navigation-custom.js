jQuery(document).ready(function($){

  // var menu_ul = '.main-navigation .menu';

  var menu_ul = $('.main-navigation').find('ul');
  menu_ul.children('.menu-item-has-children').children('a').append('<span class="mytheme_menu_switch"><i class="fa fa-plus"></i></span>');

  var mobile_menu_ul = $('.mobile-navigation').first();
  mobile_menu_ul.find("ul").children('.menu-item-has-children').children('a').append('<span class="mytheme_menu_switch"><i class="fa fa-plus"></i></span>');

  $('.navicon').click(function(){
    var $navicon = $(this);
    if ( mobile_menu_ul.css('display') == 'none' ) {

      mobile_menu_ul.addClass('show');
      $navicon.removeClass('closed').addClass('open');
      $navicon.children('.fa').removeClass('fa-navicon').addClass('fa-close');

    } else {

      mobile_menu_ul.removeClass('show');
      $navicon.removeClass('open').addClass('closed');
      $navicon.children('.fa').removeClass('fa-close').addClass('fa-navicon');

    }

  });

  // Touch friendly expanded nav
  // $(menu_ul + ' li span').click(function(event){
  $('.mytheme_menu_switch').click(function(event){

    event.preventDefault();

    // get the child of the clicked menu switch
    var child_menu = $(this).parent().parent().children('ul');

    // get the parent link of the clicked menu switch
    var parent_link = $(this).parent();

    // check if it's currently open or closed
    if ( child_menu.hasClass('childopen') ) {
      // if it's open, close it

      // remove any "active" class from parent item
      $(parent_link).removeClass('active');
      // hide child menu
      $(child_menu).removeClass('childopen');
      // set this open menu switch to +
      $(this).html('<i class="fa fa-plus"></i>');

    } else {
      // if it's closed, open it

      // hide any open child menus
      // $(menu_ul + ' ul').removeClass('childopen');
      menu_ul.children('ul').removeClass('childopen');
      // set any open menu switch symbols back to +
      // $(menu_ul + ' li span').html('+');
      menu_ul.children('li').children('span').html('<i class="fa fa-plus"></i>');

      // show correct child menu
      $(child_menu).addClass('childopen');
      // set this open menu switch to -
      $(this).html('<i class="fa fa-minus"></i>');

      return false;

    }

  });

});