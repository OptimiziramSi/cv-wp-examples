(function($){


	$(document).ready(function() {

		$('.select').not('.disabled').material_select();

		$('.scrollspy').scrollSpy();

		$('.modal-trigger').leanModal();

		$(window).scroll(function() {
			
			if ($(this).scrollTop() > 1000){
				$('#go-to-the-top').addClass("show-go");
			}
			else
			{
				$('#go-to-the-top').removeClass("show-go");
			}
		});
	});

	$(window).load(function(){
		$('#loading').addClass('hide-loading');
		setTimeout( function(){
			$('#loading').remove();
		}, 400 );
	});


})(jQuery);