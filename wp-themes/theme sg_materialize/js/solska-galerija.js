(function($){

	var
		$pswp = $('.pswp')[0],
		images
	;

	function init(){
		images = list_images();

		$(document).on( 'click', 'a.sg-img', img_click );
	}

	function list_images(){
		var items = [];
		var img_index = 0;
		$("a.sg-img").each(function() {
			var $href   = $(this).attr('href'),
				$size   = $(this).data('size').split('x'),
				$width  = $size[0],
				$height = $size[1];

			var item = {
				src 	: $href,
				w   	: $width,
				h   	: $height,
				el		: $(this),
				msrc	: $(this).find('img').attr('src'),
				title	: $(this).attr('data-caption')
			}

			$(this).data('sg-img-index', img_index );
			img_index++;

			items.push(item);
		});
		return items;
	}

	function img_click(event) {
		event.preventDefault();
		var $index = $(this).data('sg-img-index');

		var options = {
			index: $index,
			bgOpacity: 0.9,
			showHideOpacity: false,
			getThumbBoundsFn: function(index) {
				var image = images[index].el.find('img'),
				offset = image.offset();
				return {x:offset.left, y:offset.top, w:image.width()};
			}
		}

		var gallery = new PhotoSwipe( $pswp, PhotoSwipeUI_Default, images, options );
		gallery.init();
	}

	init();

})(jQuery);