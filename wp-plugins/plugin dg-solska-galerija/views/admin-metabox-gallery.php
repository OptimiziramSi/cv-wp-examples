<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

wp_nonce_field( '_sg_images', '_sg_images_nonce' );

?>
<ul class="images-preview" data-input-name="_sg_images">
	<?php
	foreach ( $images as $image_id ) {
		$image_attributes = wp_get_attachment_image_src( $image_id, 'thumbnail' );
		if( $image_attributes ){
			$image_preview_url = esc_url( $image_attributes[0] );
			?>
			<li class="image-preview">
				<input
					type="hidden"
					class="spe-image"
					name="_sg_images[]"
					value="<?php echo esc_attr( $image_id ); ?>"

					data-preview-created="0"
					data-preview-url="<?php echo esc_attr( $image_preview_url ); ?>"
					>
			</li>
			<?php
		}
	} ?>
</ul>
<p>
	<a href="#remove" id="meta-image-button" class="remove spe-remove-images-btn" data-input-name="_sg_images"><i class="material-icons">clear</i>Odstrani vse slike</a>

	<a href="#add" id="meta-image-button" class="add spe-select-images-btn" data-input-name="_sg_images"><i class="material-icons">photo_library</i>Dodaj slike</a>
</p>
