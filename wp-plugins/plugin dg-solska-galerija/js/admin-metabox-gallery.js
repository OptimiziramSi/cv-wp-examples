(function( $ ){
	
	var $spe;

	function init(){

		$spe = $("#sg-galerija-metabox");

		if( ! $spe.length ){
			return;
		}

		$spe.on('click','a.spe-select-images-btn', onSelectImagesClick );
		$spe.on('click','a.spe-remove-images-btn', onRemoveAllImagesClick );

		$spe.on('click','.images-preview .image-preview a', removeImage );

		$spe.find("ul.images-preview").sortable();

		createImagePreviews();
	}

	var select_images_frame;
	var select_images_name_attr;
	function onSelectImagesClick( event ){
		// Prevents the default action from occuring.
		event.preventDefault();

		var $btn = $(this);
		select_images_name_attr = $btn.attr('data-input-name');
		
		// If the frame already exists, re-open it.
		if ( select_images_frame ) {
			select_images_frame.open();
			return;
		}
		
		// Sets up the media library frame
		select_images_frame = wp.media.frames.meta_image_frame = wp.media({
			title: 'Slike',
			button: { text:  'Dodaj' },
			library: { type: 'image' },
			multiple: true,
		});
		
		// Runs when an image is selected.
		select_images_frame.on('select', function(){
		
			// Grabs the attachment selection and creates a JSON representation of the model.
			var media_attachments = select_images_frame.state().get('selection').toJSON();

			var $ul = $spe.find("ul.images-preview[data-input-name='"+select_images_name_attr+"']");

			var media_attachments_ids = [];

			for (var i = 0; i < media_attachments.length; i++) {
				var media_attachment = media_attachments[i];

				media_attachments_ids.push( media_attachment.id );

				if( $ul.find("input[value='"+media_attachment.id+"']").length ){
					continue;
				}

				$ul.append(
					prepImagePreviewLi( select_images_name_attr, media_attachment.sizes.thumbnail.url, media_attachment.id )
				);
			};

			createImagePreviews();
		});
		
		// Opens the media library frame.
		select_images_frame.open();
	}

	function prepImagePreviewLi( name_attr, image_url, media_id ){
		return $('<li>')
						.addClass('image-preview')
						.append(
							$('<input/>')
								.addClass('spe-image')
								.attr( 'type','hidden')
								.attr( 'name',name_attr + '[] ')
								.attr( 'data-preview-created', '0' )
								.attr( 'data-preview-url',image_url )
								.val( media_id )
						);
	}

	function onRemoveAllImagesClick( event ){
		event.preventDefault();

		var select_images_name_attr = $(this).attr('data-input-name');
		var $li_s = $spe.find("ul.images-preview[data-input-name='"+select_images_name_attr+"'] li");

		if( $li_s.length != 0 && ! confirm( 'Ste prepričani, da želite odstraniti vse slike?' ) ){
			return;
		}

		$li_s.remove();
	}

	function removeImage( event ){
		event.preventDefault();

		var $delete_a = $(this);

		var $image_preview = $delete_a.closest(".image-preview");

		var preview_tag = $image_preview.prop("tagName");

		if( "SPAN" == preview_tag ){

			var input_name = $image_preview.attr( 'data-input-name');

			$spe.find("input.spe-image[name='"+input_name+"']")
				.attr( 'data-preview-created', '0' )
				.attr( 'data-preview-url', '' )
				.val( '' );

			$image_preview.remove();

		} else if( "LI" == preview_tag ){
			$image_preview.remove();
		}

	}

	function createImagePreviews(){
		$spe.find("ul.images-preview li input.spe-image[data-preview-created!=1][data-preview-url!='']").each(function( i, e ){
			var $e = $(e);

			var img_src = $e.attr("data-preview-url");

			var $img_span = $("<span/>")
					.append(
						$("<img/>")
							.attr('src',img_src)
					).append(
						$("<br/>")
					)
					.append(
						$("<a>")
							.attr('href', '#remove')
							.text( 'Odstrani' )
					)
				;

			$e.closest(".image-preview").prepend( $img_span );
			$e.attr('data-preview-created', '1');
		});
	}









	$(document).ready( init );

})(jQuery);