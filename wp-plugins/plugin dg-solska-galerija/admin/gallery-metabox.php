<?php

class DgSolskaGalerija_Admin_GalleryMetabox {


	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		
		/*******************  admin section  *******************/
		if( ! is_admin() ){
			return;
		}

		// metabox init
		add_action( 'load-post.php', array( $this, 'page_gallery_metabox' ) );
		add_action( 'load-post-new.php', array( $this, 'page_gallery_metabox' ) );
	}

	function page_gallery_metabox(){
		add_action( 'add_meta_boxes', array( $this, 'page_gallery_metabox_add' ) );
		add_action('save_post', array( $this, 'page_gallery_metabox_save' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'page_gallery_metabox_styles_and_scripts' ) );
	}

	function page_gallery_metabox_add(){
		add_meta_box('sg-galerija-metabox', 'Galerija', array( $this, 'page_gallery_metabox_view' ), array('page'), 'normal' );
	}

	function page_gallery_metabox_view( $post ){
		$images = get_post_meta( $post->ID, SgParams::META_IMAGES, true );
		if( ! is_array( $images ) ){
			$images = array();
		}
		include dirname( SG_FILE ) . '/views/admin-metabox-gallery.php';
	}

	function page_gallery_metabox_save( $post_id ){
		/*
		 * We need to verify this came from our screen and with proper authorization,
		 * because the save_post action can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['_sg_images_nonce'] ) ) {
			return;
		}

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['_sg_images_nonce'], '_sg_images' ) ) {
			return;
		}

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// Check the user's permissions.
		if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return;
			}

		} else {
			return;
		}

		$images = array();
		if( isset( $_POST['_sg_images'] ) && is_array( $_POST['_sg_images'] ) ){
			$images = array_map('intval', $_POST['_sg_images'] );
			$images = array_unique( $images );
			$images = array_filter( $images );
		}

		// save data
		update_post_meta( $post_id, SgParams::META_IMAGES, $images );
	}

	function page_gallery_metabox_styles_and_scripts() {
		global $post;
		if( 'page' !== $post->post_type ){
			return;
		}

		wp_register_style( 'sg-admin-metabox-gallery', plugin_dir_url( SG_FILE ) . '/css/admin-metabox-gallery.css', array(), SG_VERSION );
		wp_register_style( 'sg-admin-material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
		wp_register_script( 'sg-admin-metabox-gallery', plugin_dir_url( SG_FILE ) . '/js/admin-metabox-gallery.js', array('jquery'), SG_VERSION );


		wp_enqueue_media();

		wp_enqueue_script('jquery-ui-sortable');

		wp_enqueue_style( 'sg-admin-metabox-gallery' );
		wp_enqueue_style( 'sg-admin-material-icons' );

		wp_enqueue_script( 'sg-admin-metabox-gallery' );
	}


}