<?php

/**
 * helper class za splošne informacije o parametrih galerije
 */
class SgParams {
	const SG_OPTION = 'sg_options';

	const META_IMAGES = '_sg_images';

	const IMG_SIZE_THUMB = '_sg_img_size_thumb';
	const IMG_SIZE_LARGE = '_sg_img_size_large';
}