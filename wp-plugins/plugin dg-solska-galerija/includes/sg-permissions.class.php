<?php

/**
 * helper class, ki vsebuje bližnjice za pravice
 */
class SgPermissions {
	const MANAGE_DISLOCATED_LOCATIONS = 'sg_manage_dislocated_locations';
	const MANAGE_GALLERIES = 'sg_manage_galleries';
	const ADD_IMAGES = 'sg_add_images';
}