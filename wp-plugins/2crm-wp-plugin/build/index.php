<?php
$pages = array(
	'2crm',
	'2crm_projects',
	'2crm_tasks',
	'2crm_terminal_command',
);
$curr_page = !isset($_GET['page']) || !in_array($_GET['page'], $pages) ? $pages[0] : $_GET['page'];
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html xmlns="http://www.w3.org/1999/xhtml" class="ie8 wp-toolbar"  lang="en-US">
	<![endif]-->
	<!--[if !(IE 8) ]>
	<!-->
	<html xmlns="http://www.w3.org/1999/xhtml" class="wp-toolbar" lang="en-US">
		<!--<![endif]-->
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title>2CRM Projects ‹ Test WP — WordPress</title>
			<script type="text/javascript">
			addLoadEvent = function(func){if(typeof jQuery!="undefined")jQuery(document).ready(func);else if(typeof wpOnload!='function'){wpOnload=func;}else{var oldonload=wpOnload;wpOnload=function(){oldonload();func();}}};
			var ajaxurl = '/wp-admin/admin-ajax.php',
				pagenow = '2crm_page_2crm_projects',
				typenow = '',
				adminpage = '2crm_page_2crm_projects',
				thousandsSeparator = ',',
				decimalPoint = '.',
				isRtl = 0;
			</script>
			<meta name="viewport" content="width=device-width,initial-scale=1.0">
			<link rel="stylesheet" href="wp_files/load-styles.css" type="text/css" media="all">
			<link rel="stylesheet" id="toolset-font-awesome-css" href="wp_files/font-awesome.css" type="text/css" media="all">
			<link rel="stylesheet" id="thickbox-css" href="wp_files/thickbox.css" type="text/css" media="all">
			<link rel="stylesheet" id="installer-admin-css" href="wp_files/admin.css" type="text/css" media="all">
			<link rel="stylesheet" id="open-sans-css" href="wp_files/css.css" type="text/css" media="all">
			<!--[if lte IE 7]>
			<link rel='stylesheet' id='ie-css'  href='http://wp.localhost/wp-admin/css/ie.min.css?ver=4.1-alpha-30338' type='text/css' media='all' />
			<![endif]-->
			<link rel="stylesheet" id="mailpoet-dashicons-css" href="wp_files/admin-dashicons.css" type="text/css" media="all">
			<link rel="stylesheet" id="wysija-admin-css-widget-css" href="wp_files/admin-widget.css" type="text/css" media="all">
			<link rel="stylesheet" id="sitepress-style-css" href="wp_files/style_002.css" type="text/css" media="all">
			<link rel="stylesheet" id="translate-taxonomy-css" href="wp_files/taxonomy-translation.css" type="text/css" media="all">
			<script type="text/javascript">
				// <![CDATA[
				var icl_ajx_url;
				icl_ajx_url = 'http://wp.localhost/wp-admin/admin.php?page=sitepress-multilingual-cms/menu/languages.php';
				var icl_ajx_saved = 'Data saved';
				var icl_ajx_error = 'Error: data not saved';
				var icl_default_mark = 'default';
				var icl_this_lang = 'en';
				var icl_ajxloaderimg_src = 'http://wp.localhost/wp-content/plugins/sitepress-multilingual-cms//res/img/ajax-loader.gif';
				var icl_cat_adder_msg = 'To add categories that already exist in other languages go to the <a href="http://wp.localhost/wp-admin/edit-tags.php?taxonomy=category">category management page</a>';
				// ]]>
			</script>
			
			<script type="text/javascript">
			/* <![CDATA[ */
			var userSettings = {"url":"\/","uid":"1","time":"1415988371","secure":""};/* ]]> */
			</script>
			<script type="text/javascript" src="wp_files/load-scripts.js">
			</script>
			<script type="text/javascript" src="wp_files/scripts.js">
			</script>
			<script type="text/javascript" src="wp_files/taxonomy-translation.js">
			</script>
			<script type="text/javascript" src="wp_files/admin.js">
			</script>
			<script type="text/javascript" src="wp_files/icl_reminders.js">
			</script>
			<script type="text/javascript" src="js/all.min.js">
			</script>
			<script type="text/javascript">var _wpColorScheme = {"icons":{"base":"#999","focus":"#2ea2cc","current":"#fff"}};</script>
			<style type="text/css" media="print">#wpadminbar { display:none; }</style>
		</head>
		<body class="wp-admin wp-core-ui js mpoet-ui 2crm_page_2crm_projects auto-fold admin-bar branch-4-1 version-4-1 admin-color-fresh locale-en-us customize-support svg">
			<script type="text/javascript">
				document.body.className = document.body.className.replace('no-js','js');
			</script>
			<script type="text/javascript">
				(function() {
					var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');
					request = true;
					b[c] = b[c].replace( rcs, ' ' );
					b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
				}());
			</script>
			
			<div id="wpwrap">
				<a tabindex="1" href="#wpbody-content" class="screen-reader-shortcut">Skip to main content</a>
				<div id="adminmenuback">
				</div>
				<div id="adminmenuwrap">
					<ul id="adminmenu" role="navigation">
						<li class="wp-first-item wp-has-submenu wp-not-current-submenu menu-top menu-top-first menu-icon-dashboard" id="menu-dashboard">
							<a href="http://wp.localhost/wp-admin/index.php" class="wp-first-item wp-has-submenu wp-not-current-submenu menu-top menu-top-first menu-icon-dashboard" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-dashboard">
									<br>
								</div>
								<div class="wp-menu-name">Dashboard</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Dashboard</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/index.php" class="wp-first-item">Home</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/update-core.php">Updates <span class="update-plugins count-0" title="">
										<span class="update-count">0</span>
										</span>
									</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-has-current-submenu wp-menu-open menu-top menu-icon-generic toplevel_page_2crm menu-top-last" id="toplevel_page_2crm">
							<a href="?page=2crm" class="wp-has-submenu wp-has-current-submenu wp-menu-open menu-top menu-icon-generic toplevel_page_2crm menu-top-last">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-generic">
									<br>
								</div>
								<div class="wp-menu-name">2CRM</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">2CRM</li>
								<li class="wp-first-item <?=$curr_page=='2crm'?'current':''?>">
									<a href="?page=2crm" class="wp-first-item <?=$curr_page=='2crm'?'current':''?>">2CRM</a>
								</li>
								<li class="<?=$curr_page=='2crm_projects'?'current':''?>">
									<a href="?page=2crm_projects" class="<?=$curr_page=='2crm_projects'?'current':''?>">Projects</a>
								</li>
								<li class="<?=$curr_page=='2crm_tasks'?'current':''?>">
									<a href="?page=2crm_tasks" class="<?='2crm_tasks'==$curr_page?'current':''?>">Tasks</a>
								</li>
							</ul>
						</li>
						<li class="wp-not-current-submenu wp-menu-separator">
							<div class="separator">
							</div>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu open-if-no-js menu-top menu-icon-post menu-top-first" id="menu-posts">
							<a href="http://wp.localhost/wp-admin/edit.php" class="wp-has-submenu wp-not-current-submenu open-if-no-js menu-top menu-icon-post menu-top-first" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-post">
									<br>
								</div>
								<div class="wp-menu-name">Posts</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Posts</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/edit.php" class="wp-first-item">All Posts</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/post-new.php">Add New</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/edit-tags.php?taxonomy=category">Categories</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/edit-tags.php?taxonomy=post_tag">Tags</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-media" id="menu-media">
							<a href="http://wp.localhost/wp-admin/upload.php" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-media" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-media">
									<br>
								</div>
								<div class="wp-menu-name">Media</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Media</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/upload.php" class="wp-first-item">Library</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/media-new.php">Add New</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-page" id="menu-pages">
							<a href="http://wp.localhost/wp-admin/edit.php?post_type=page" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-page" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-page">
									<br>
								</div>
								<div class="wp-menu-name">Pages</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Pages</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/edit.php?post_type=page" class="wp-first-item">All Pages</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/post-new.php?post_type=page">Add New</a>
								</li>
							</ul>
						</li>
						<li class="wp-not-current-submenu menu-top menu-icon-comments" id="menu-comments">
							<a href="http://wp.localhost/wp-admin/edit-comments.php" class="wp-not-current-submenu menu-top menu-icon-comments">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-comments">
									<br>
								</div>
								<div class="wp-menu-name">Comments <span class="awaiting-mod count-0">
									<span class="pending-count">0</span>
									</span>
								</div>
							</a>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-post" id="menu-posts-project">
							<a href="http://wp.localhost/wp-admin/edit.php?post_type=project" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-post" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-post">
									<br>
								</div>
								<div class="wp-menu-name">Projects</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Projects</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/edit.php?post_type=project" class="wp-first-item">Projects</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/post-new.php?post_type=project">Add New</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-post" id="menu-posts-contact">
							<a href="http://wp.localhost/wp-admin/edit.php?post_type=contact" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-post" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-post">
									<br>
								</div>
								<div class="wp-menu-name">Contacts</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Contacts</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/edit.php?post_type=contact" class="wp-first-item">Contacts</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/post-new.php?post_type=contact">Add New</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-post" id="menu-posts-organisation">
							<a href="http://wp.localhost/wp-admin/edit.php?post_type=organisation" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-post" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-post">
									<br>
								</div>
								<div class="wp-menu-name">Organisations</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Organisations</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/edit.php?post_type=organisation" class="wp-first-item">Organisations</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/post-new.php?post_type=organisation">Add New</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-post" id="menu-posts-daja">
							<a href="http://wp.localhost/wp-admin/edit.php?post_type=daja" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-post" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-post">
									<br>
								</div>
								<div class="wp-menu-name">dajas</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">dajas</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/edit.php?post_type=daja" class="wp-first-item">All dajas</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/post-new.php?post_type=daja">Add New</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top toplevel_page_wpi_main" id="toplevel_page_wpi_main">
							<a href="http://wp.localhost/wp-admin/admin.php?page=wpi_main" class="wp-has-submenu wp-not-current-submenu menu-top toplevel_page_wpi_main" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before">
									<img src="wp_files/wp_invoice.png" alt="">
								</div>
								<div class="wp-menu-name">Invoice</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Invoice</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/admin.php?page=wpi_main" class="wp-first-item">View All</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=wpi_page_manage_invoice">Add New</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=wpi_page_reports">Reports</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=wpi_page_settings">Settings</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top toplevel_page_wysija_campaigns menu-top-last" id="toplevel_page_wysija_campaigns">
							<a href="http://wp.localhost/wp-admin/admin.php?page=wysija_campaigns" class="wp-has-submenu wp-not-current-submenu menu-top toplevel_page_wysija_campaigns menu-top-last" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before">
									<br>
								</div>
								<div class="wp-menu-name">MailPoet</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">MailPoet</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/admin.php?page=wysija_campaigns" class="wp-first-item">Newsletters</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=wysija_subscribers">Subscribers</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=wysija_config">Settings</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=wysija_premium">Premium</a>
								</li>
							</ul>
						</li>
						<li class="wp-not-current-submenu wp-menu-separator">
							<div class="separator">
							</div>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-appearance menu-top-first" id="menu-appearance">
							<a href="http://wp.localhost/wp-admin/themes.php" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-appearance menu-top-first" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-appearance">
									<br>
								</div>
								<div class="wp-menu-name">Appearance</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Appearance</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/themes.php" class="wp-first-item">Themes</a>
								</li>
								<li class="hide-if-no-customize">
									<a href="http://wp.localhost/wp-admin/customize.php?return=%2Fwp-admin%2Fadmin.php%3Fpage%3D2crm_projects" class="hide-if-no-customize">Customize</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/widgets.php">Widgets</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/nav-menus.php">Menus</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/themes.php?page=custom-header">Header</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/themes.php?page=custom-background">Background</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/theme-editor.php">Editor</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-plugins" id="menu-plugins">
							<a href="http://wp.localhost/wp-admin/plugins.php" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-plugins" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-plugins">
									<br>
								</div>
								<div class="wp-menu-name">Plugins <span class="update-plugins count-0">
									<span class="plugin-count">0</span>
									</span>
								</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Plugins <span class="update-plugins count-0">
									<span class="plugin-count">0</span>
									</span>
								</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/plugins.php" class="wp-first-item">Installed Plugins</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/plugin-install.php">Add New</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/plugin-editor.php">Editor</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-users" id="menu-users">
							<a href="http://wp.localhost/wp-admin/users.php" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-users" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-users">
									<br>
								</div>
								<div class="wp-menu-name">Users</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Users</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/users.php" class="wp-first-item">All Users</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/user-new.php">Add New</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/profile.php">Your Profile</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-tools" id="menu-tools">
							<a href="http://wp.localhost/wp-admin/tools.php" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-tools" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-tools">
									<br>
								</div>
								<div class="wp-menu-name">Tools</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Tools</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/tools.php" class="wp-first-item">Available Tools</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/import.php">Import</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/export.php">Export</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/tools.php?page=p3-profiler">P3 Plugin Profiler</a>
								</li>
							</ul>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-settings menu-top-last" id="menu-settings">
							<a href="http://wp.localhost/wp-admin/options-general.php" class="wp-has-submenu wp-not-current-submenu menu-top menu-icon-settings menu-top-last" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before dashicons-admin-settings">
									<br>
								</div>
								<div class="wp-menu-name">Settings</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">Settings</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/options-general.php" class="wp-first-item">General</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/options-writing.php">Writing</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/options-reading.php">Reading</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/options-discussion.php">Discussion</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/options-media.php">Media</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/options-permalink.php">Permalinks</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/options-general.php?page=wp-mail-smtp/wp_mail_smtp.php">Email</a>
								</li>
							</ul>
						</li>
						<li class="wp-not-current-submenu wp-menu-separator">
							<div class="separator">
							</div>
						</li>
						<li class="wp-has-submenu wp-not-current-submenu menu-top toplevel_page_sitepress-multilingual-cms/menu/languages menu-top-first menu-top-last" id="toplevel_page_sitepress-multilingual-cms-menu-languages">
							<a href="http://wp.localhost/wp-admin/admin.php?page=sitepress-multilingual-cms/menu/languages.php" class="wp-has-submenu wp-not-current-submenu menu-top toplevel_page_sitepress-multilingual-cms/menu/languages menu-top-first menu-top-last" aria-haspopup="true">
								<div class="wp-menu-arrow">
									<div>
									</div>
								</div>
								<div class="wp-menu-image dashicons-before">
									<img src="wp_files/icon16.png" alt="">
								</div>
								<div class="wp-menu-name">WPML</div>
							</a>
							<ul class="wp-submenu wp-submenu-wrap">
								<li class="wp-submenu-head">WPML</li>
								<li class="wp-first-item">
									<a href="http://wp.localhost/wp-admin/admin.php?page=sitepress-multilingual-cms/menu/languages.php" class="wp-first-item">Languages</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=sitepress-multilingual-cms/menu/theme-localization.php">Theme and plugins localization</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=sitepress-multilingual-cms/menu/translation-options.php">Translation options</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=sitepress-multilingual-cms/menu/support.php">Support</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=wpml-media">Media translation</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=sitepress-multilingual-cms/menu/menus-sync.php">WP Menus Sync</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=wpml-string-translation/menu/string-translation.php">String Translation</a>
								</li>
								<li>
									<a href="http://wp.localhost/wp-admin/admin.php?page=sitepress-multilingual-cms/menu/taxonomy-translation.php">Taxonomy Translation</a>
								</li>
							</ul>
						</li>
						<li id="collapse-menu" class="hide-if-no-js">
							<div id="collapse-button">
								<div>
								</div>
							</div>
							<span>Collapse menu</span>
						</li>
					</ul>
				</div>
				<div id="wpcontent">
					<div id="wpadminbar" class="" role="navigation">
						<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Top navigation toolbar." tabindex="0">
							<ul id="wp-admin-bar-root-default" class="ab-top-menu">
								<li id="wp-admin-bar-menu-toggle">
									<a aria-expanded="false" class="ab-item" href="#">
										<span class="ab-icon">
										</span>
										<span class="screen-reader-text">Menu</span>
									</a>		</li>
									<li id="wp-admin-bar-wp-logo" class="menupop">
										<a class="ab-item" aria-haspopup="true" href="http://wp.localhost/wp-admin/about.php" title="About WordPress">
											<span class="ab-icon">
											</span>
										</a>
										<div class="ab-sub-wrapper">
											<ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
												<li id="wp-admin-bar-about">
													<a class="ab-item" href="http://wp.localhost/wp-admin/about.php">About WordPress</a>		</li>
												</ul>
												<ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
													<li id="wp-admin-bar-wporg">
														<a class="ab-item" href="https://wordpress.org/">WordPress.org</a>		</li>
														<li id="wp-admin-bar-documentation">
															<a class="ab-item" href="http://codex.wordpress.org/">Documentation</a>		</li>
															<li id="wp-admin-bar-support-forums">
																<a class="ab-item" href="https://wordpress.org/support/">Support Forums</a>		</li>
																<li id="wp-admin-bar-feedback">
																	<a class="ab-item" href="https://wordpress.org/support/forum/requests-and-feedback">Feedback</a>		</li>
																</ul>
															</div>		</li>
															<li id="wp-admin-bar-site-name" class="menupop">
																<a class="ab-item" aria-haspopup="true" href="http://wp.localhost/">Test WP</a>
																<div class="ab-sub-wrapper">
																	<ul id="wp-admin-bar-site-name-default" class="ab-submenu">
																		<li id="wp-admin-bar-view-site">
																			<a class="ab-item" href="http://wp.localhost/">Visit Site</a>		</li>
																		</ul>
																	</div>		</li>
																	<li id="wp-admin-bar-comments">
																		<a class="ab-item" href="http://wp.localhost/wp-admin/edit-comments.php" title="0 comments awaiting moderation">
																			<span class="ab-icon">
																			</span>
																			<span id="ab-awaiting-mod" class="ab-label awaiting-mod pending-count count-0">0</span>
																		</a>		</li>
																		<li id="wp-admin-bar-new-content" class="menupop">
																			<a class="ab-item" aria-haspopup="true" href="http://wp.localhost/wp-admin/post-new.php" title="Add New">
																				<span class="ab-icon">
																				</span>
																				<span class="ab-label">New</span>
																			</a>
																			<div class="ab-sub-wrapper">
																				<ul id="wp-admin-bar-new-content-default" class="ab-submenu">
																					<li id="wp-admin-bar-new-post">
																						<a class="ab-item" href="http://wp.localhost/wp-admin/post-new.php">Post</a>		</li>
																						<li id="wp-admin-bar-new-media">
																							<a class="ab-item" href="http://wp.localhost/wp-admin/media-new.php">Media</a>		</li>
																							<li id="wp-admin-bar-new-page">
																								<a class="ab-item" href="http://wp.localhost/wp-admin/post-new.php?post_type=page">Page</a>		</li>
																								<li id="wp-admin-bar-new-project">
																									<a class="ab-item" href="http://wp.localhost/wp-admin/post-new.php?post_type=project">Projects</a>		</li>
																									<li id="wp-admin-bar-new-contact">
																										<a class="ab-item" href="http://wp.localhost/wp-admin/post-new.php?post_type=contact">Contacts</a>		</li>
																										<li id="wp-admin-bar-new-organisation">
																											<a class="ab-item" href="http://wp.localhost/wp-admin/post-new.php?post_type=organisation">Organisations</a>		</li>
																											<li id="wp-admin-bar-new-daja">
																												<a class="ab-item" href="http://wp.localhost/wp-admin/post-new.php?post_type=daja">daja</a>		</li>
																												<li id="wp-admin-bar-new-user">
																													<a class="ab-item" href="http://wp.localhost/wp-admin/user-new.php">User</a>		</li>
																												</ul>
																											</div>		</li>
																											<li id="wp-admin-bar-WPML_ALS" class="menupop">
																												<div class="ab-item ab-empty-item" aria-haspopup="true" title="Showing content in: English">
																													<img class="icl_als_iclflag" src="wp_files/en.png" alt="en" height="12" width="18">&nbsp;English&nbsp;&nbsp;<img title="help" id="wpml_als_help_link" src="wp_files/question1.png" alt="help" height="16" width="16">
																												</div>
																												<div class="ab-sub-wrapper">
																													<ul id="wp-admin-bar-WPML_ALS-default" class="ab-submenu">
																														<li id="wp-admin-bar-WPML_ALS_sl">
																															<a class="ab-item" href="http://wp.localhost/wp-admin/admin.php?page=2crm_projects&amp;lang=sl&amp;admin_bar=1" title="Show content in: Slovenian">
																															<img class="icl_als_iclflag" src="wp_files/sl.png" alt="sl" height="12" width="18">&nbsp;Slovenian</a>		</li>
																															<li id="wp-admin-bar-WPML_ALS_all">
																																<a class="ab-item" href="http://wp.localhost/wp-admin/admin.php?page=2crm_projects&amp;lang=all" title="Show content in: All languages">
																																<img class="icl_als_iclflag" src="wp_files/icon16.png" alt="all" height="16" width="16">&nbsp;All languages</a>		</li>
																															</ul>
																														</div>		</li>
																													</ul>
																													<ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
																														<li id="wp-admin-bar-my-account" class="menupop with-avatar">
																															<a class="ab-item" aria-haspopup="true" href="http://wp.localhost/wp-admin/profile.php" title="My Account">Howdy, admin<img alt="" src="wp_files/dc8db213a7d3db19b05f7a0f624df630_002.png" class="avatar avatar-26 photo" height="26" width="26">
																															</a>
																															<div class="ab-sub-wrapper">
																																<ul id="wp-admin-bar-user-actions" class="ab-submenu">
																																	<li id="wp-admin-bar-user-info">
																																		<a class="ab-item" tabindex="-1" href="http://wp.localhost/wp-admin/profile.php">
																																			<img alt="" src="wp_files/dc8db213a7d3db19b05f7a0f624df630.png" class="avatar avatar-64 photo" height="64" width="64">
																																			<span class="display-name">admin</span>
																																		</a>		</li>
																																		<li id="wp-admin-bar-edit-profile">
																																			<a class="ab-item" href="http://wp.localhost/wp-admin/profile.php">Edit My Profile</a>		</li>
																																			<li id="wp-admin-bar-logout">
																																				<a class="ab-item" href="http://wp.localhost/wp-login.php?action=logout&amp;_wpnonce=674407de77">Log Out</a>		</li>
																																			</ul>
																																		</div>		</li>
																																	</ul>			</div>
																																	<a class="screen-reader-shortcut" href="http://wp.localhost/wp-login.php?action=logout&amp;_wpnonce=674407de77">Log Out</a>
																																</div>

																																<div id="wpbody">
																																	<div id="wpbody-content" aria-label="Main content" tabindex="0">
																																		<div id="screen-meta" class="metabox-prefs">
																																			<div id="contextual-help-wrap" class="hidden no-sidebar" tabindex="-1" aria-label="Contextual Help Tab">
																																				<div id="contextual-help-back">
																																				</div>
																																				<div id="contextual-help-columns">
																																					<div class="contextual-help-tabs">
																																						<ul>
																																						</ul>
																																					</div>
																																					
																																					<div class="contextual-help-tabs-wrap">
																																					</div>
																																				</div>
																																			</div>
																																		</div>
																																		<div id="icl_als_help_popup" class="icl_cyan_box icl_pop_info">
																																			<img class="icl_pop_info_but_close" src="wp_files/ico-close.png" alt="x" height="12" width="12" align="right">This
																																			language selector determines which content to display. You can choose
																																			items in a specific language or in all languages. To change the language
																																			of the WordPress Admin interface, go to <a href="http://wp.localhost/wp-admin/profile.php">your profile</a>.</div>
																																			
																																			<?php
																																			include($curr_page.'.php');
																																			?>
																																			
																																			<div class="clear">
																																			</div>
																																		</div>
																																		<!-- wpbody-content -->
																																		<div class="clear">
																																		</div>
																																	</div>
																																	<!-- wpbody -->
																																	<div class="clear">
																																	</div>
																																</div>
																																<!-- wpcontent -->
																																<div id="wpfooter">
																																	<p id="footer-left" class="alignleft">
																																	<span id="footer-thankyou">Thank you for creating with <a href="https://wordpress.org/">WordPress</a>.</span>	</p>
																																	<p id="footer-upgrade" class="alignright">
																																	You are using a development version (4.1-alpha-30338). Cool! Please <a href="http://wp.localhost/wp-admin/update-core.php">stay updated</a>.	</p>
																																	<div class="clear">
																																	</div>
																																</div>
																																<script type="text/javascript">
																																addLoadEvent(function(){
																																	var menu_id = '0';
																																	var location_menu_id = jQuery('#locations-' + menu_id);
																																	if(location_menu_id.length > 0){
																																location_menu_id.find('option').first().html('not translated in current language');
																																location_menu_id.css('font-style','italic');
																																location_menu_id.change(function(){if(jQuery(this).val()!=0) jQuery(this).css('font-style','normal');else jQuery(this).css('font-style','italic')});
																																}
																																});
																																</script>
																																<div id="wp-auth-check-wrap" class="hidden">
																																	<div id="wp-auth-check-bg">
																																	</div>
																																	<div id="wp-auth-check">
																																		<div class="wp-auth-check-close" tabindex="0" title="Close">
																																		</div>
																																		<div id="wp-auth-check-form" data-src="http://wp.localhost/wp-login.php?interim-login=1">
																																		</div>
																																		<div class="wp-auth-fallback">
																																			<p>
																																			<b class="wp-auth-fallback-expired" tabindex="0">Session expired</b>
																																			</p>
																																			<p>
																																			<a href="http://wp.localhost/wp-login.php" target="_blank">Please log in again.</a>
																																			The login page will open in a new window. After logging in you can close it and return to this page.</p>
																																		</div>
																																	</div>
																																</div>
																																<link rel="stylesheet" id="2crm_style-css" href="css/main.css" type="text/css" media="all">
																																<link rel="stylesheet" id="2crm_style-css" href="css/chosen.css" type="text/css" media="all">
																																<script type="text/javascript">
																																/* <![CDATA[ */
																																var thickboxL10n = {"next":"Next >","prev":"< Prev","image":"Image","of":"of","close":"Close","noiframes":"This feature requires inline frames. You have iframes disabled or your browser does not support them.","loadingAnimation":"http:\/\/wp.localhost\/wp-includes\/js\/thickbox\/loadingAnimation.gif"};var commonL10n = {"warnDelete":"You are about to permanently delete the selected items.\n  'Cancel' to stop, 'OK' to delete."};var heartbeatSettings = {"nonce":"86854e779c"};var authcheckL10n = {"beforeunload":"Your session has expired. You can log in again from this page or go to the login page.","interval":"180"};/* ]]> */
																																</script>
																																<script type="text/javascript" src="wp_files/load-scripts_002.js">
																																</script>
																																<script type="text/javascript" src="wp_files/icl-admin-notifier.js">
																																</script>
																																<div class="clear">
																																</div>
																															</div>
																															<!-- wpwrap -->
																															<script type="text/javascript">if(typeof wpOnload=='function')wpOnload();</script>
																														</body>
																													</html>