<?php
/**
* Plugin Name: E-Mail Security Requirement Workaround for PHP 5.6
* Description: For sending mails via SMTP in PHP 5.6 requires secure connection - this is a workaround
* Plugin URI: https://www.2gika.si/projects/php-5-6-with-phpmailer-security-requirement-workaround-for-wordpress/
* Author: 2gika
* Author URI: http://2gika.si
* Version: 1.0
* License: GPL2
* Text Domain: dg-phpmailer-php56-fix
* Domain Path: languages
*/

/*
Copyright (C) 2016  2gika  info@2gika.si

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/**
 * Snippet Wordpress Plugin Boilerplate based on:
 *
 * - https://github.com/purplefish32/sublime-text-2-wordpress/blob/master/Snippets/Plugin_Head.sublime-snippet
 * - http://wordpress.stackexchange.com/questions/25910/uninstall-activate-deactivate-a-plugin-typical-features-how-to/25979#25979
 *
 * By default the option to uninstall the plugin is disabled,
 * to use uncomment or remove if not used.
 *
 * This Template does not have the necessary code for use in multisite. 
 *
 * Also delete this comment block is unnecessary once you have read.
 *
 * Version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'plugins_loaded', array( 'DgPhpMailerSecurityRequirementWorkaround', 'getInstance' ) );

class DgPhpMailerSecurityRequirementWorkaround {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {

		add_action( 'phpmailer_init', array( $this, 'phpmailer_init' ), 999 );

	}

	function phpmailer_init( $phpmailer ){

		if( "smtp" == $phpmailer->Mailer ){
			$phpmailer->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
		}

	}
}