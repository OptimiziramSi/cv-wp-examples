=== Plugin Name ===
Contributors: 2GIKA
Donate link: https://www.2gika.si/projects/php-5-6-with-phpmailer-security-requirement-workaround-for-wordpress/
Tags: php5.6, php, 5.6, PhpMailer, fix, SMTP, mail, mail form, form
Requires at least: 3.0.1
Tested up to: 4.2
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enables sending mails via SMTP using non secure connection that PHP 5.6 requires by default

== Description ==

Enables sending mails via SMTP using non secure connection that PHP 5.6 requires by default

A really unconventional fix / workaround regarding new security requirement in PHP 5.6 that forces you to have a secure connection via SMTP. This fix sets default PhpMailer configurations to not verify secure connection to your SMTP mail account.

Use this only as temporary fix as this is not a secure workaround.


== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/dg-phpmailer-security-requirement-workaround` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Plugin does not have any additional settings and it is "on" on activation of the plugin


== Frequently Asked Questions ==

= What doesn it accually do? =

If you have php5.6 on your server, you can not at the moment send SMTP mails via WordPress forms (such as Contact Form 7)

== Changelog ==

= 1.0 =
* Initial release.