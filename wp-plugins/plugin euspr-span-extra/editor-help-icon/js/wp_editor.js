(function( $ ) {
	var shortcode_tag = "ese_euspr_span_help";
	var settings = ese_wpe[ shortcode_tag ] ? ese_wpe[ shortcode_tag ] : {};
	// $(document),on('click', 'div.ese_wpe.'+shortcode_tag, )

	tinymce.PluginManager.add(shortcode_tag, function( editor, url ) {
		//add button
		editor.addButton(shortcode_tag, {
			icon: 'wp_help',
			tooltip: settings.button_tooltip,
			onclick: function() {
				var win = window.open( settings.help_url, '_blank');
				win.focus();
			}
		});
	});
})( jQuery );