<?php

class EusprSpanExtra_ToolAddTargetToFileAnchors {

	const OPTION_REPLACED = "ese_tattfa_replaced";

	private $slug = "ese-add-target-to-file-anchors";

	private $active_mimetypes;

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() || ! current_user_can( 'manage_options' ) ){
			return;
		}

		// create submenu page
		add_action( 'admin_menu', array( $this, 'addSubmenuToToolsMenu' ) );

	}

	function addSubmenuToToolsMenu(){
		// create submenu under TOOLS menu
		add_submenu_page(
			'tools.php',
			__( 'Add target to document links', 'ese' ),
			__( 'Add target to document links', 'ese' ),
			'manage_options',
			$this->slug,
			array( $this, 'renderPage' )
		);
	}

	function renderPage(){
		$data = $this->processRequest();

		include dirname( __FILE__ ) . '/views/tool.php';
	}

	private function processRequest(){
		$data = array(
			'state' => '',
			'errors' => array(),
			'messages' => array(),
		);

		$data['state'] = '';


		// DEVELOP
		// $data['state'] = 'scan_report';
		// $data['scan_report'] = $this->scanContents( $this->scanContentsQuery() );
		// return $data;


		if( isset( $_POST['action'] ) && $this->slug."-scan" == $_POST['action'] ){

			$data['state'] = 'scan_report';

			if( ! isset( $_POST[$this->slug.'-scan-nonce'] ) || ! wp_verify_nonce( $_POST[$this->slug.'-scan-nonce'], $this->slug.'-scan' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', 'ese' );
				$data['state'] = '';

				return $data;
			}

			$data['scan_report'] = $this->scanContents( $this->scanContentsQuery() );

			return $data;
		}




		if( isset( $_POST['action'] ) && $this->slug."-process" == $_POST['action'] ){

			$data['state'] = 'process_report';

			if( ! isset( $_POST[$this->slug.'-process-nonce'] ) || ! wp_verify_nonce( $_POST[$this->slug.'-process-nonce'], $this->slug.'-process' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', 'ese' );
				$data['state'] = '';

				return $data;
			}

			$data['process_report'] = $this->processContents( $this->scanContentsQuery() );
			
			return $data;
		}

		$data['replaced'] = $this->get_replaced();

		return $data;
	}


	private function scanContents( $query ){

		$this->set_active_mimetypes();

		$contents = array();

		foreach ( $query->posts as $post ) {
			if( ! $this->documentAnchorsExist( $post->post_content ) ){
				continue;
			}

			$new_content = $this->fixDocumentAnchors( $post->post_content );

			$contents[] = array(
				'title' => strtoupper($post->post_type) . ':' . esc_html( $post->post_title ),
				'view_url' => get_the_permalink( $post->ID ),
				'edit_url' => add_query_arg(array('post' => $post->ID), '/wp-admin/post.php?post=6602&action=edit'),
				'old_content' => $post->post_content,
				'new_content' => $new_content,
			);
		}

		return $contents;
	}


	private function processContents( $query ){

		$this->set_active_mimetypes();

		$contents = array();
		$replaced = $this->get_replaced();

		foreach ( $query->posts as $post ) {
			if( ! $this->documentAnchorsExist( $post->post_content ) ){
				continue;
			}

			$new_content = $this->fixDocumentAnchors( $post->post_content );

			$contents[] = array(
				'title' => strtoupper($post->post_type) . ':' . esc_html( $post->post_title ),
				'view_url' => get_the_permalink( $post->ID ),
				'edit_url' => add_query_arg(array('post' => $post->ID), '/wp-admin/post.php?post=6602&action=edit'),
				'old_content' => $post->post_content,
				'new_content' => $new_content,
			);

			wp_update_post( array(
				'ID' => $post->ID,
				'post_content' => $new_content,
			) );

			if( ! isset( $replaced[ $post->ID ] ) ){
				$replaced[ $post->ID ] = array();
			}

			$replaced[ $post->ID ][] = date('Y-m-d H:i:s');
		}

		$this->set_replaced( $replaced );


		return $contents;
	}

	private function fixDocumentAnchors( $content, $check_only = false ){
		$found = false;

		if( "" == trim( $content ) ){
			if( $check_only ){
				// no bad content here
				return false;
			} else {
				// we don't need to remove anything
				return "";
			}
		}

		$dom = new DOMDocument();
		$html_loaded = @$dom->loadHTML( '<?xml encoding="UTF-8">' . $content );
		if( ! $html_loaded ){
			return null;
		}

		foreach ( $dom->getElementsByTagName('a') as $a ) {

			$href = $a->getAttribute('href');
			$target = $a->getAttribute('target');
			// if div contains POSITION ABSOLUTE, we take it as hacked content
			if( $this->isDocumentLink( $href ) && "_blank" != $target ){
				
				if( $check_only ){
					return true;
				}

				$found = true;
				$a->setAttribute('target', '_blank');
			}
		}

		if( $check_only ){
			return false;
		}

		if( $found ){
			$mock = new DOMDocument;
			$body = $dom->getElementsByTagName('body')->item(0);
			foreach ($body->childNodes as $child){
				$mock->appendChild($mock->importNode($child, true));
			}

			$content = $mock->saveHTML();
			$content = html_entity_decode( $content );
		}

		return $content;
	}

	private function documentAnchorsExist( $content ){
		return $this->fixDocumentAnchors( $content, true );
	}





	private function scanContentsQuery(){
		$query_args = array(
			'nopaging' => true,
			'post_type' => array('post','page'),
			'post_status' => 'any',
		);

		$query = new WP_Query( $query_args );

		return $query;
	}

	private function get_replaced(){
		$replaced = get_option( self::OPTION_REPLACED );
		if( ! is_array( $replaced )){
			$replaced = array();
		}
		return $replaced;
	}

	private function set_replaced( $replaced ){
		update_option( self::OPTION_REPLACED, $replaced );
	}

	private function isDocumentLink( $href ){
		$ext = pathinfo($href, PATHINFO_EXTENSION);
		return in_array( $ext, $this->active_mimetypes );
	}

	private function set_active_mimetypes(){
		$mimetypes_option = get_option( 'mimetype_link_icon_options' );

		$this->active_mimetypes = array();

		if( ! is_array( $mimetypes_option ) ){
			return;
		}

		$mime_types = array(
			'3g2', '3gp',
			'ai', 'air', 'asf', 'avi',
			'bib',
			'cls', 'csv',
			'deb', 'djvu', 'dmg', 'doc', 'docx', 'dwf', 'dwg',
			'eps', 'epub', 'exe',
			'f', 'f77', 'f90', 'flac', 'flv',
			'gif', 'gz',
			'ico', 'indd', 'iso',
			'jpg', 'jpeg',
			'log',
			'm4a', 'm4v', 'midi', 'mkv', 'mov', 'mp3', 'mp4', 'mpeg', 'mpg', 'msi',
			'odp', 'ods', 'odt', 'oga', 'ogg', 'ogv',
			'pdf', 'png', 'pps', 'ppsx', 'ppt', 'pptx', 'psd', 'pub', 'py',
			'qt',
			'ra', 'ram', 'rar', 'rm', 'rpm', 'rtf', 'rv',
			'skp', 'spx', 'sql', 'sty',
			'tar', 'tex', 'tgz', 'tiff', 'ttf', 'txt',
			'vob',
			'wav', 'wmv',
			'xls', 'xlsx', 'xml', 'xpi',
			'zip',
		);
		
		foreach ( $mime_types as $mime_type ) {
			if ( true === $mimetypes_option[ 'enable_' . $mime_type ] ) {
				$this->active_mimetypes[] = $mime_type;
			}
		}
		unset( $mime_type );
	}

}
