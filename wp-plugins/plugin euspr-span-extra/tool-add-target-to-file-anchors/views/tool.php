<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}
?>
<div class="wrap">
	<h1><?php _e( 'Add target to file document anchors', 'ese' ); ?></h1>
	<p>
		<?php _e( 'This tool add target="_blank" atrribute to anchors which link to documents.', 'ese' ); ?>
	</p>

	<?php
	// TODO - echo errors and messages
	?>

	<?php switch ( $data['state'] ) {

		// when we complete scanning
		case 'scan_report':
			?>
			<h3><?php _e( 'Scan report', 'ese' ); ?></h3>

			<table class="form-table">
				<tbody>
					<?php
					foreach ( $data['scan_report'] as $content ) {
						?>
						<tr>
							<td scope="row">
								<?php echo $content['title']; ?><br>
								<a href="<?php echo esc_attr( $content['edit_url'] ); ?>"><?php esc_html_e( 'Edit', 'ese' ); ?></a>
								<a href="<?php echo esc_attr( $content['view_url'] ); ?>"><?php esc_html_e( 'View', 'ese' ); ?></a>
							</td>
							<td>
								<p class="description">
									<?php echo htmlentities( $content['old_content'] ); ?>
								</p>
							</td>
							<td>
								<p class="description">
									<?php echo htmlentities( $content['new_content'] ); ?>
								</p>
							</td>
						</tr>
						<?php
					}
					?>

					<?php if( 0 == count( $data['scan_report'] ) ){ ?>
						<p>
							<?php _e( 'No contents found', 'ese' ); ?>
						</p>
					<?php } ?>
				</tbody>
			</table>

			<h3><?php _e( 'Start with the process', 'ese' ); ?></h3>
			<p><?php _e( 'Keep in mind that this can take some time', 'ese' ); ?></p>
			<form method="post">
				<input type="hidden" name="action" value="<?php echo esc_attr( $this->slug ); ?>-process" />
				<?php wp_nonce_field( $this->slug.'-process', $this->slug.'-process-nonce' ); ?>

				<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php esc_attr_e( 'Start processing', 'ese' ); ?>" /></p>
			</form>
			<?php
			break;

		// when we complete processing
		case 'process_report':
			?>
			<h3><?php _e( 'Process report', 'ese' ); ?></h3>

			<table class="form-table">
				<tbody>
					<?php
					foreach ( $data['process_report'] as $content ) {
						?>
						<tr>
							<td scope="row">
								<?php echo $content['title']; ?><br>
								<a href="<?php echo esc_attr( $content['edit_url'] ); ?>"><?php esc_html_e( 'Edit', 'ese' ); ?></a>
								<a href="<?php echo esc_attr( $content['view_url'] ); ?>"><?php esc_html_e( 'View', 'ese' ); ?></a>
							</td>
							<td>
								<p class="description">
									<?php echo htmlentities( $content['old_content'] ); ?>
								</p>
							</td>
							<td>
								<p class="description">
									<?php echo htmlentities( $content['new_content'] ); ?>
								</p>
							</td>
						</tr>
						<?php
					}
					?>

					<?php if( 0 == count( $data['process_report'] ) ){ ?>
						<p>
							<?php _e( 'No contents found', 'ese' ); ?>
						</p>
					<?php } ?>
				</tbody>
			</table>

			<h3><?php _e( 'Start again?', 'ese' ); ?></h3>
			<p><?php _e( 'Keep in mind that this can take some time', 'ese' ); ?></p>
			<form method="post">
				<input type="hidden" name="action" value="<?php echo esc_attr( $this->slug ); ?>-scan" />
				<?php wp_nonce_field( $this->slug.'-scan', $this->slug.'-scan-nonce' ); ?>

				<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php esc_attr_e( 'Rescan', 'ese' ); ?>" /></p>
			</form>
			<?php
			break;
		
		// default start screen
		default:
			?>
			<h3><?php _e( 'Start with scanning the contents', 'ese' ); ?></h3>
			<p><?php _e( 'Keep in mind that this can take some time', 'ese' ); ?></p>
			<form method="post">
				<input type="hidden" name="action" value="<?php echo esc_attr( $this->slug ); ?>-scan" />
				<?php wp_nonce_field( $this->slug.'-scan', $this->slug.'-scan-nonce' ); ?>

				<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php esc_attr_e( 'Scan contents', 'ese' ); ?>" /></p>
			</form>

			<h3><?php _e( 'History log:', 'ese' ); ?></h3>
			<ul>
				<?php foreach ($data['replaced'] as $post_id => $replaced_dates ) { ?>
					<li>
						<b><?php echo get_the_title( $post_id ); ?></b><br>
						<a href="<?php echo esc_attr( add_query_arg(array('post' => $post_id), '/wp-admin/post.php?post=6602&action=edit') ); ?>"><?php esc_html_e( 'Edit', 'ese' ); ?></a>
						<a href="<?php echo esc_attr( get_the_permalink( $post_id ) ); ?>"><?php esc_html_e( 'View', 'ese' ); ?></a>
						<ul>
							<?php foreach ($replaced_dates as $replaced_date) { ?>
								<li><?php echo date( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), strtotime( $replaced_date ) ); ?></li>
							<?php } ?>
						</ul>
					</li>
				<?php } ?>
				<?php if ( 0 == count( $data['replaced'] ) ) { ?>
					<li><?php _e( 'No contents replaced', 'ese' ); ?></li>
				<?php } ?>
			</ul>
			<?php
			break;
	} ?>
</div>