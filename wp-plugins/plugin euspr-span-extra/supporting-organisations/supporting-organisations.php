<?php

class EusprSpanExtra_SupportingOrganisations {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		// register custom post type
		add_action( 'init', array( $this, 'register_custom_post_type' ) );
		// register meta boxes - with titan framework
		add_action( 'tf_create_options', array( $this, 'add_meta_boxes' ) );
		// manage members list columns
		add_filter( 'manage_ese_sup_organisation_posts_columns', array( $this, 'manage_ese_sup_organisation_posts_columns') );
		// manage members list columns content
		add_filter( 'manage_ese_sup_organisation_posts_custom_column', array( $this, 'manage_ese_sup_organisation_custom_column'), 10, 2 );
		// register member image size
		add_action( 'init', array( $this, 'add_image_size' ) );

		// shortcode for listing members
		add_shortcode( 'supporting_organisations', array( $this, 'shortcode_supporting_organisations_process' ) );

	}


	/*************************************************
			SUPPORTING ORGANISATION CUSTOM POST TYPE - START
	*************************************************/
	
	/**
	* Registers a new post type
	* @uses $wp_post_types Inserts new post type object into the list
	*
	* @param string  Post type key, must not exceed 20 characters
	* @param array|string  See optional args description above.
	* @return object|WP_Error the registered post type object, or an error object
	*/
	function register_custom_post_type() {

		$labels = array(
			'name'                => __( 'Supporting Organisations', 'ese' ),
			'singular_name'       => __( 'Supporting Organisation', 'ese' ),
			'add_new'             => _x( 'Add New Supporting Organisation', 'ese', 'ese' ),
			'add_new_item'        => __( 'Add New Supporting Organisation', 'ese' ),
			'edit_item'           => __( 'Edit Supporting Organisation', 'ese' ),
			'new_item'            => __( 'New Supporting Organisation', 'ese' ),
			'view_item'           => __( 'View Supporting Organisation', 'ese' ),
			'search_items'        => __( 'Search Supporting Organisations', 'ese' ),
			'not_found'           => __( 'No Supporting Organisations found', 'ese' ),
			'not_found_in_trash'  => __( 'No Supporting Organisations found in Trash', 'ese' ),
			'parent_item_colon'   => __( 'Parent Supporting Organisation:', 'ese' ),
			'menu_name'           => __( 'Supporting Organisations', 'ese' ),
		);

		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'Supporting Organisations CPT',
			'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => 'edit.php?post_type=ese_bmember',
			'show_in_admin_bar'   => false,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-groups',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array( 'title', 'thumbnail','revisions' )
		);

		register_post_type( 'ese_sup_organisation', $args );
	}

	public function manage_ese_sup_organisation_posts_columns( $columns ){
		$use_columns = array();
		foreach ( $columns as $column_key => $column_title ) {
			// skip / remove columns
			if( in_array( $column_key, array('date','uam_access') ) ){
				continue;
			}

			// copy default column
			$use_columns[ $column_key ] = $column_title;

			// rename title label
			if( "title" == $column_key ){
				$use_columns[ $column_key ] = __( 'Supporting Organisation Name', 'ese' );
			}

			// add image column after CB column
			if( "cb" == $column_key ){
				$use_columns["organisation_image"] = __( 'Image', 'ese' );
			}

			// add member title column after member name
			if( "title" == $column_key ){
				$use_columns["link"] = __( 'Link', 'ese' );
			}
		}

		return $use_columns;
	}

	public function manage_ese_sup_organisation_custom_column( $column, $post_id ){
		switch ( $column ) {
			case 'organisation_image':
				echo the_post_thumbnail( 'ese-supporting-organisations-list' );
				break;

			case 'link':
				$link = get_post_meta( $post_id, 'ese-sup-organisation-cpt_website', true );
				if( ! empty( $link ) ){
					echo '<a href="'.esc_url( $link ).'" target="_blank">' . esc_url( $link ) . '</a>';
				}
				break;			
		}
	}

	public function add_meta_boxes(){

		$titan = TitanFramework::getInstance( 'ese-sup-organisation-cpt' );

		/*******************  metabox - link  *******************/
		$metaBox_socials = $titan->createMetaBox( array(
			'name' => __( 'More info', 'ese' ),
			'post_type' => 'ese_sup_organisation',
		) );

		$metaBox_socials->createOption( array(
			'name' => __( 'Website', 'ese' ),
			'id' => 'website',
			'type' => 'text',
			'desc' => __( 'Input website url for this Supporting Organisation', 'ese' )
		) );

	}

	public function add_image_size(){
		add_image_size( 'ese-supporting-organisations-list', 100, 100, true );
		add_image_size( 'ese-supporting-organisation', 300, 140, false );
	}
	
	/*************************************************
			SUPPORTING ORGANISATION CUSTOM POST TYPE - END
	*************************************************/



	/*************************************************
			SHORTCODES - START
	*************************************************/
	
	public function shortcode_supporting_organisations_process() {
		// get all contents
		$organisations = get_posts(array(
					'nopaging' => true,
					'post_type' => 'ese_sup_organisation',
					// 'post_status' => 'published',
					'orderby' => 'menu_order',
					'order' => 'ASC',
				));
		foreach ( $organisations as $organisation_key => $organisation_post ) {
			$organisation_post->website = get_post_meta( $organisation_post->ID, 'ese-sup-organisation-cpt_website', true );
			$organisations[ $organisation_key ] = $organisation_post;
		}

		ob_start();
		include dirname( __FILE__ ) . '/views/supporting-organisations.php';
		return ob_get_clean();
	}
	
	/*************************************************
			SHORTCODES - END
	*************************************************/

}