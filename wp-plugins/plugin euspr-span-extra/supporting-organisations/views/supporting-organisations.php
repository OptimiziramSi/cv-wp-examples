<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

?>
<div class="supporting-organisations-wrapper">
	<?php foreach ( $organisations as $members_title => $organisation_post ) { ?>
		<div class="org-part-holder">
		
			<?php if ( ! empty( $organisation_post->website ) ) { ?>

				<?php if( has_post_thumbnail( $organisation_post->ID ) ){
					$organisation_img_id = get_post_thumbnail_id( $organisation_post->ID );
					$organisation_img_src = wp_get_attachment_image_src( $organisation_img_id, 'ese-supporting-organisation' );
					?>
					<a target="_blank" class="third-1x a-org" title="<?php echo esc_attr( $organisation_post->post_title ); ?>" href="<?php echo esc_url( $organisation_post->website ); ?>">
						<img src="<?php echo esc_url( $organisation_img_src[0] ); ?>" title="<?php echo esc_attr( $organisation_post->post_title ); ?>" id="<?php echo esc_attr( $organisation_img_id ); ?>"/>
					</a>
				<?php } ?>

				<div class="part-content-holder">
					<h4><?php echo esc_html( $organisation_post->post_title ); ?></h4>
					<a target="_blank" class="button link" href="<?php echo esc_url( $organisation_post->website ); ?>"><?php echo sprintf( __( 'Visit %s', 'ese' ), esc_html( $organisation_post->post_title ) ); ?></a>
				</div>

			<?php } else { ?>
				
				<?php if( has_post_thumbnail( $organisation_post->ID ) ){
					$organisation_img_id = get_post_thumbnail_id( $organisation_post->ID );
					$organisation_img_src = wp_get_attachment_image_src( $organisation_img_id, 'ese-supporting-organisation' );
					?>
					<div class="a-org third-1x">
						<img src="<?php echo esc_url( $organisation_img_src[0] ); ?>" title="<?php echo esc_attr( $organisation_post->post_title ); ?>" id="<?php echo esc_attr( $organisation_img_id ); ?>"/>
					</div>
				<?php } ?>
				<div class="part-content-holder">
					<h4><?php echo esc_html( $organisation_post->post_title ); ?></h4>
				</div>
			<?php } ?>

		</div>
	<?php } ?>
</div>