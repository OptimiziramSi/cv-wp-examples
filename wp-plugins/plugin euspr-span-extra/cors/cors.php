<?php

class EusprSpanExtra_Cors {


	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){
		if( ! EusprSpanExtra_Plugin::is_localhost() ){
			return;
		}

		add_action( 'init', array( $this, 'add_cors_header' ) );
		
	}

	public function add_cors_header(){

		header("Access-Control-Allow-Origin: *");
		
	}

}