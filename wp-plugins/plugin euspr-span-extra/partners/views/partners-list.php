<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

?>
<div class="partners-wrapper">
	<ul class="collapsible" data-collapsible="accordion">
	<?php foreach ( $partners as $partner_post ) { ?>
		<li>
			<div class="collapsible-header">
				<i class="fa fa-plus"></i><?php echo esc_html( $partner_post->post_title ); ?>
			</div>
			<div class="collapsible-body">
				<?php echo apply_filters( 'the_content', $partner_post->post_content ); ?>
			</div>
		</li>
	<?php } ?>
	</ul>
</div>