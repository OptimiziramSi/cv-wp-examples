<?php

class EusprSpanExtra_PreventionScienceDatabasesV2_XlsExporter {

	private $id;
	private $databaseId;
	private $databaseName;
	private $encodingCheckString;
	private $columns;
	private $data;

	private $objPHPExcel;

	private $xls_row_index = 1;

	// TODO - delete paramt that you do not need
	private $numColumns = 0;

	function __construct(){
		
	}

	function setId( $id ){
		$this->id = $id;
	}

	function setDatabaseId( $database_id ){
		$this->databaseId = $database_id;
	}

	function setDatabaseName( $database_name ){
		$this->databaseName = $database_name;
	}

	function setColumns( $columns ){
		$this->columns = $columns;
	}

	function setData( $data ){
		$this->data = $data;
	}

	function process(){

		$this->prepXLS();

		$this->prepColumns();

		$this->processHeaders();

		$this->processData();
	}

	function save( $path ){

		// create folder for XLS files
		$folder = dirname( $path );
		if( ! is_dir( $folder ) && ! wp_mkdir_p( $folder ) ){
			wp_die( __('Could not create folder for XLS export. Please contact info@2gika.si for more info.', 'ese'), __( 'Error exporting XLS', 'ese' ) );
		}

		$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel5');
		$objWriter->save( $path );
	}

	function download( $xls_name ){
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$xls_name.'"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0


		$objWriter = PHPExcel_IOFactory::createWriter( $this->objPHPExcel, 'Excel5' );
		$objWriter->save('php://output');
	}

	function output(){
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$xls_name.'"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}

	private function prepXLS(){

		$excel_lib_path = plugin_dir_path( ESE_FILE ) . '/libs/PHPExcel.php';
		require_once $excel_lib_path;

		$this->objPHPExcel = new PHPExcel();

		$this->objPHPExcel->getProperties()->setCreator("EUSPR website")
							 ->setLastModifiedBy("EUSPR website")
							 ->setTitle( "EUSPR database - " . $this->databaseName )
							 ->setSubject( "EUSPR database - " . $this->databaseName )
							 ->setDescription( "EUSPR database - " . $this->databaseName )
							 ->setKeywords( "EUSPR database - " . $this->databaseName )
							 ->setCategory( "EUSPR database - " . $this->databaseName );
	}

	private function processHeaders(){

		$this->xlsRow( array( '#EXPORT_ID', $this->id ) );
		$this->xlsRow( array( '#DATABASE_ID', $this->databaseId ) );
		$this->xlsRow( array( '#DATABASE_NAME', $this->databaseName ) );
		// $this->xlsRow( array( '#ENCODING_CHECK_STRING', $this->encodingCheckString ) );
		$this->xlsRow( array() );

		$headers_arr = array();
		$headers_arr[] = '#ID';

		foreach ($this->columns as $column_data ) {
			if( (int)$column_data['num_fields'] > 1 ){
				for ($i=1; $i <= (int)$column_data['num_fields']; $i++) { 
					$headers_arr[] = "#" . $column_data['label'] . " (" . $i . ")";
				}
			} else {
				$headers_arr[] = "#" . $column_data['label'];
			}
		}

		$this->xlsRow( $headers_arr );
	}

	private function prepColumns(){
		$this->numColumns = 1;	// first header column is ID of the data row
		foreach ($this->columns as $column_data ) {
			$this->numColumns += (int)$column_data['num_fields'];
		}
	}

	private function processData(){

		$data_columns = array();

		foreach ( $this->columns as $column_data ) {
			for ($i=0; $i < (int)$column_data['num_fields']; $i++) { 
				$data_columns[] = $column_data['id'] . '_' . $i;
			}
		}

		foreach ($this->data as $row_id => $row_data ) {
			
			$this->processDataRow( $row_id, $row_data, $data_columns );

		}

	}

	private function processDataRow( $row_id, $row_data, $data_columns ){

		$xls_row_data_arr = array();

		$xls_row_data_arr[] = $row_id;


		foreach ( $data_columns as $data_column ) {
			$xls_row_data_arr[] = $row_data[ $data_column ];
		}

		$this->xlsRow( $xls_row_data_arr );

	}

	private function xlsRow( $values ){

		$xls_sheet = $this->objPHPExcel->setActiveSheetIndex(0);

		for ($i=0; $i < $this->numColumns; $i++) { 
			$values_val = isset( $values[ $i ] ) ? $values[ $i ] : "";
			$xls_sheet->setCellValueByColumnAndRow( $i, $this->xls_row_index, $values_val );
		}

		$this->xls_row_index++;
	}
}