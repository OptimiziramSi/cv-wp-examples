<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

?>
<div class="ese-db-v2-columns">
	<span class="add-column button button-primary button-large right"><?php _e( 'Add column', 'ese' ); ?></span>
	<ul class="database-columns">
		
	</ul>
	<ul class="ese-psd-hot-columns-item-settings-template" style="display: none;">
		<li class="column-item">
			<span class="column-left-content">
				<input class="column-label" type="text" placeholder="<?php esc_attr_e( 'Column name', 'ese' ); ?>" />
				<small><?php _e( 'Nr. of fields', 'ese' ); ?></small> <input class="column-num-fields" type="number" step="1" min="1" max="7" value="1" />
			</span>
			<span class="column-actions">
				<span class="remove"><i class="fa fa-times"></i></span>
				<span class="move"><i class="fa fa-bars"></i></span>
			</span>
			<ul class="columns-settings">
				<li class="column-setting is-view-on-list" data-type="is-view-on-list" title="<?php esc_attr_e( 'Show this column in table', 'ese' ); ?>"><i class="fa fa-table"></i></li>
				<li class="column-setting is-email" data-type="is-email" title="<?php esc_attr_e( 'Show as email link', 'ese' ); ?>"><i class="fa fa-envelope"></i></li>
				<li class="column-setting is-www" data-type="is-www" title="<?php esc_attr_e( 'Show as www link', 'ese' ); ?>"><i class="fa fa-globe"></i></li>
				<li class="column-setting is-search" data-type="is-search" title="<?php esc_attr_e( 'Enable search in this column', 'ese' ); ?>"><i class="fa fa-search"></i></li>
				<li class="column-setting is-filter" data-type="is-filter" title="<?php esc_attr_e( 'Show as filter', 'ese' ); ?>"><i class="fa fa-filter"></i></li>
				<li class="column-setting is-wide" data-type="is-wide" title="<?php esc_attr_e( 'Wider column', 'ese' ); ?>"><i class="fa fa-arrows-h"></i></li>
				<li class="column-setting is-short" data-type="is-short" title="<?php esc_attr_e( 'Shorter column', 'ese' ); ?>"><i class="fa fa-arrows-h fa-smaller"></i></li>
			</ul>
		</li>
	</ul>
</div>