<div class="psd-edit">

	<!--
		TODO Matija - tekst in opis po kmečko

		- Nikakor ne smejo brisati zgornjega dela v izvoženi XLS / XLSX datoteki (eport id, database id, vrstica z imeno stolpcev)
		- ko enkrat izvozijo XLS / XLSX, imajo 30dni časa, da ga uredijo in uvozijo (po 30 dneh pobrišemo izvoz)
		- ko enkrat uspešno naredijo uvoz, ga ne morejo ponovno uvoziti. V tem primeru morajo na novo prenesti XLS / XLSX, ga urediti in upload-ati
		- vrstice brišejo tako, da v XLS / XLSX pobrišejo celo vrstico ali vse podatke iz vrstice, ki jo želijo pobrisati. Polja lahko ostanejo prazna ali pa jih zapolnijo s podatki za novo vrstico.
		- vrstice dodajajo tako, da v prazno vrstico (kjer ni nobenih podatkov / nobenih podatkov več), izpolnijo polja, razen prvega stolpca "#ID"
		- vrstice urejajo tako, da uredijo polja v želeni vrstici, pri čemer MORAJO PUSTITI PRI MIRU prvi stolpec "#ID"
	-->
	
	<!-- TODO - edit text and put it thru translations -->
	<div>
		<h3>Editing database works in the following steps:</h3>
		<ol>
			<li>Download XLS / XLSX file for desired database</li>
			<li>Edit downloaded file in your favourite program like MS Excel.</li>
			<li>Save changes (keep the XLS / XLSX format)</li>
			<li>Upload edited XLS / XLSX and confirm changes</li>
		</ol>	
	</div>
	
	<!-- TODO - edit text and put it thru translations -->
	<div>
		<h4>Editing XLS / XLSX</h4>
		<ul>
			<li>
				In downloaded file you will notice some essential information at the beginning of the XLS / XLSX file. <strong>Do not edit or remove those lines</strong> from file since this information is.
				<div class="card-panel grey lighten-1">
					#EXPORT_ID	123456789<br>
					#DATABASE_ID	1234<br>
					#DATABASE_NAME	downloaded database name
				</div>
			</li>
			<li>
				You can edit anything below the row that starts with column names. Leave row with column names as it is (do not change or delete any column).
			</li>
			<li>If you would like to delete a row from a database, just delete the whole row with data.</li>
			<li>If you would like to edit data in any row, leave first column as it is (this is id of the row) and change any data in next columns.</li>
			<li>If you would like to add new rows to database, fill all the columns you want, just leave first column (ID) empty.</li>
			<!-- <li>If you would like to change sort order of the rows in database, you can sort them the way you want in downloaded file. Just remember to move whole row (all columns, don't forget the ID column) up or down.</li> -->
		</ul>
	</div>


	<div class="">
		
		<div class="half-c">
			
			<h3><?php _e( 'Download database', 'ese' ); ?></h3>
			
			<p>
				Download database you would like to edit.
			</p>
			<ul>
				<?php foreach (	$data['export_urls'] as $download_url => $download_title ) { ?>
				<li>
					
					<?php echo esc_html( $download_title ); ?>
					<a class="button" href="<?php echo esc_attr( $download_url ); ?>" target="_blank"><?php _e( 'Download', 'ese' ); ?></a>
				</li>

				<?php } ?>

				<?php if( 0 == count( $data['export_urls'] ) ){ ?>

				<li><?php _e( 'No databases to download', 'ese' ); ?></li>
				
				<?php } ?>
			</ul>
		</div>
		

		<div class="half-c">
			<h3><?php _e( 'Upload edited XLS / XLSX file', 'ese' ); ?></h3>
			<?php
			if( isset( $data['import']['errors'] ) ){
				foreach ( $data['import']['errors'] as $error ) {
					?>
					<div data-scroll-to-me="" class="card-panel red lighten-1"><?php echo esc_html( $error ); ?></div>
					<?php
				}
			}
			?>

			<?php
			if( isset( $data['import']['messages'] ) ){
				foreach ( $data['import']['messages'] as $message ) {
					?>
					<div data-scroll-to-me="" class="card-panel green lighten-1"><?php echo esc_html( $message ); ?></div>
					<?php
				}
			}
			?>
			
			<?php if( count( $data['import']['errors'] ) > 0 ){ ?>


				<div data-scroll-to-me="" class="card-panel grey lighten-1">
					<?php _e( 'Looks like there was some problem. Please resolve the issues and start again.', 'ese' ); ?>
					
				</div>
				<a class="button replay" href="<?php echo get_the_permalink(); ?>"><?php _e( 'Start again', 'ese' ); ?></a>


			<?php } else { ?>


				<?php switch ( $data['import']['state'] ) {
					case 'preview':
						?>
						<form method="post" data-scroll-to-me="">
							<?php foreach ($data['import']['hidden_inputs'] as $input_name => $input_value ) { ?>
								<input type="hidden" name="<?php echo esc_attr( $input_name ); ?>" value="<?php echo esc_attr( $input_value ); ?>">
							<?php } ?>
							
							<h5><?php _e( 'Preview pending changes', 'ese' ); ?></h5>
							<div class="card-panel grey lighten-1">
								<?php echo __( 'No. of removed rows:', 'ese' ) . ' ' . $data['import']['report']['remove_rows_ids']; ?><br>
								<?php echo __( 'No. of changed rows:', 'ese' ) . ' ' . $data['import']['report']['update_rows']; ?><br>
								<?php echo __( 'No. of added rows:', 'ese' ) . ' ' . $data['import']['report']['insert_rows']; ?>
							</div>
							<p>
								
								<input type="submit" value="<?php esc_attr_e( 'Confirm changes', 'ese' ); ?>" class="submit-btn-orange">
							</p>
						</form>
						<?php
						break;

					case 'process':
						?>
							<h5><?php _e( 'That is it, thank you!', 'ese' ); ?></h5>
							<div class="card-panel grey lighten-1">
								<?php echo __( 'No. of removed rows:', 'ese' ) . ' ' . $data['import']['report']['remove_rows_ids']; ?><br>
								<?php echo __( 'No. of changed rows:', 'ese' ) . ' ' . $data['import']['report']['update_rows']; ?><br>
								<?php echo __( 'No. of added rows:', 'ese' ) . ' ' . $data['import']['report']['insert_rows']; ?>
							</div>
							<p>
								<a class="button replay" href="<?php echo get_the_permalink(); ?>"><?php _e( 'Start again', 'ese' ); ?></a>
							</p>
						<?php
						break;
					
					default:
						?>
						<form method="post" enctype="multipart/form-data">
							
							<label for="ese-databases-import-xls-file">First "Choose the file" and then click "Upload"</label><br>
							<input type="hidden" name="ese-databases-import-xls-action" value="upload_xls">
							<input type="file" value="Choose XLS / XLSX file" name="ese-databases-import-xls-file"><br><br>
							
							<input type="submit" value="<?php esc_attr_e( 'Upload the XLS / XLSX file', 'ese' ); ?>" class="submit-btn-orange">
							<?php wp_nonce_field( 'ese-databases-import-xls-' . get_current_user_id(), 'ese-databases-import-xls-nonce' ); ?>

						</form>
						<?php
						break;
				} ?>


			<?php } ?>
		</div>
	</div>



</div>