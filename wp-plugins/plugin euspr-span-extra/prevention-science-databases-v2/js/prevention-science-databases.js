(function($){

	var $psd,
		psd_id,
		table_data,
		show_data,
		rows_pool,
		span_rows_num = 30,	// prepare num rows before and after current view
		ready_filters = [],
		filterRequired,
		searchString,
		$psd_filters,

		filters
		;



	function init(){
		$psd = $(".prevention-science-databses-wrapper");

		if( 0 == $psd.length ){
			return;
		}

		prepListeners( ['psd:databaseSwitch'], switchDatabase, 0 );
		prepListeners( ['psd:databaseSwitch'], initDatabaseFilters, 10 );
		prepListeners( ['psd:filterChanged'], prepDatabaseData, 20 );
		prepListeners( ['psd:databaseSwitch', 'psd:databaseDataChange', 'psd:windowResize'], resizeTableHolder, 30 );
		prepListeners( ['psd:databaseSwitch', 'psd:windowResize'], prepTableRowsPool, 40 );
		prepListeners( ['psd:databaseDataChange', 'psd:scroll', 'psd:tableRowsPoolChange'], renderTableRows, 50 );

		// add listener to window resize
		$(window).resize(function(){ $psd.trigger('psd:windowResize'); });

		// add listener to window scroll
		$(window).scroll(function (event) { $psd.trigger('psd:scroll'); });

		// // prep tooltips
		// $psd.tooltip({
		// 	effect: 'fade',
		// 	position: { my: 'center top', at: 'center bottom+10', collision: 'none' },
		// 	hide: false
		// });

		// add search listeners
		$psd.find(".psd-filter").keyup( function(){ $psd.trigger('psd:filterChanged'); } );
		$psd.find(".psd-filter").change( function(){ $psd.trigger('psd:filterChanged'); } );

		// read more on rows
		$psd.on('click', '.psd-readmore', open_read_more_modal );

		// add database switch listeners on databases list
		$(".psd-list li[data-psd-id]").click( function(event){
			event.preventDefault();
			var li_psd_id = $(this).attr('data-psd-id');
			if( psd_id == li_psd_id ){
				return;
			}
			psd_id = li_psd_id;
			$('span.psd-name').text( $(this).attr('data-psd-name') );
			$psd.trigger('psd:databaseSwitch');
		} );

		// open first database
		$(".psd-list li:first").click();
	}

	function switchDatabase(){

		$(".psd-list li").removeClass('active');
		$(".psd-list li[data-psd-id='"+psd_id+"']").addClass('active');

		$(".ese-psd-filters").removeClass('active');
		$(".ese-psd-filters[data-psd-id='"+psd_id+"']").addClass('active');

		$(".ese-psd-table").removeClass('active');
		$(".ese-psd-table[data-psd-id='"+psd_id+"']").addClass('active');

		$psd.find(".ese-psd-table .Table .Table-row:not(.Table-header)").remove();

		table_data = psd_data.databases[ psd_id ];
		show_data = table_data.rows;
	}

	function initDatabaseFilters(){

		$psd_filters = $psd.find(".filters .ese-psd-filters[data-psd-id='"+psd_id+"'] select.custom-filter");
		if( $.inArray(psd_id, ready_filters) == -1 ){
			$psd_filters
				.select2({
					placeholder: ese_psd.filter_placeholder
				})
				.change( function(){ $psd.trigger('psd:filterChanged'); } )
				;
			ready_filters.push( psd_id );
		}
		// clear filters and search field
		$psd_filters.filter(function(){ return "" != $(this).val(); }).select2("val", "");
		$psd.find(".psd-filter").filter(function(){ return "" != $(this).val(); }).val("");

		$psd.trigger('psd:filterChanged');
	}

	function prepDatabaseData(){
		$psd.find('.filter-loading').show();;

		var filters = {};
		var search = $psd.find('input.psd-filter').val().toLowerCase();

		$psd_filters.each(function(i,e){
			var $e = $(e);
			var select_values = $e.val();
			if( select_values && select_values.length > 0 ){
				filters[ $e.attr('data-column-key') ] = select_values;
			}
		});

		if( 0 == Object.keys(filters).length && "" == search ){
			show_data = table_data.rows;
			$psd.find('.filtered-stats').hide();
		} else {
			show_data = $.grep( table_data.rows, function( row_data, row_index ){

				for (var col in filters ) {
					var filter_values = filters[ col ];
					var col_values = row_data[ col ];

					if( ! col_values ){
						// ta row ni v filtrih, zato ga ne prikazujemo
						return false;
					}

					for (var col_value_index = 0; col_value_index < col_values.length; col_value_index++) {
						var col_value = col_values[col_value_index];
						var found = false;
						if( -1 !== $.inArray( col_value, filter_values ) ){
							found = true;
							break;
						}

						if( ! found ){
							return false;
						}
					};
				};

				// filter also by search value
				if( "" != search ){
					var search_columns = table_data.search_columns;
					for (var sck = search_columns.length - 1; sck >= 0; sck--) {
						var col = search_columns[sck];
						var col_values = row_data[ col ];

						if( ! col_values ){
							// ta row ni v filtrih, zato ga ne prikazujemo
							continue;
						}

						for (var col_value_index = 0; col_value_index < col_values.length; col_value_index++) {
							var col_value = col_values[col_value_index];
							if( col_value.toLowerCase().indexOf( search ) > -1 ){
								return true;
							}
						};
					};
					// if not in search, do not show
					return false;
				} else {
					// no search string, so we show this row
					return true;
				}
			});

			$psd.find('.filtered-stats').show();
			$psd.find('.filtered-stats .num-filtered').text( show_data.length );
		}

		$psd.find('.stats .num-all').text( table_data.rows.length );
		$psd.find('.filter-loading').hide();
		
		$psd.trigger('psd:databaseDataChange');
	}

	function resizeTableHolder(){

		var row_height = getRowHeight();
		var num_rows = show_data.length;

		var table_height = row_height * ( num_rows + 1 );

		var $table = $(".ese-psd-table.active .Table");
		$table.height( table_height );
	}

	function prepTableRowsPool(){

		var window_height = $(window).height();
		var row_height = getRowHeight();

		var num_rows_needed = Math.ceil( window_height / row_height ) + span_rows_num * 2;

		var $table = $psd.find(".ese-psd-table.active .Table");

		$table.find(".Table-row:not(.Table-header)").remove();

		rows_pool = [];
		for (var i = 0; i < num_rows_needed; i++) {
			var $row = $psd.find(".ese-psd-table.active .psd-row-template .Table-row").clone();

			$table.append( $row );
			rows_pool[ i ] = $row;
		};

		$psd.trigger('psd:tableRowsPoolChange');
	}

	function renderTableRows(){

		var $table = $psd.find(".ese-psd-table.active .Table");

		var row_height = getRowHeight();

		var scroll_top = $(window).scrollTop();

		var top_margin = row_height;

		var table_position = $table.offset().top;

		var table_view_y_top = scroll_top - table_position;
		if( table_view_y_top < 1 ){
			table_view_y_top = 1;
		}
		var table_view_y_bottom = table_view_y_top + $(window).height();

		var row_index_start = Math.floor( table_view_y_top / row_height );
		var row_index_end = Math.ceil( table_view_y_bottom / row_height );

		var row_index_span = ( row_index_end - row_index_start ) + span_rows_num * 2;

		row_index_start = row_index_start - span_rows_num;
		if( row_index_start < 0 ){
			row_index_start = 0;
		}
		row_index_end = row_index_start + row_index_span;

		var rows_pool_length = rows_pool.length;

		for (var i = row_index_start; i < row_index_end; i++) {
			var row_data = show_data[ i ] ? show_data[ i ] : false;
			var row_pool_index = i % rows_pool_length;
			var $row = rows_pool[ row_pool_index ];

			if( ! row_data ){
				$row.hide();
				continue;
			}

			$row.show();

			$row.find('.Table-row-item[data-psd-col]').each(function( i, e){
				var $e = $(e);
				var col_index = $e.attr('data-psd-col');
				$e.html( row_data['col_' + col_index + '_html'] );
			});

			$row.data('row_index', i );

			var row_y = i * row_height + top_margin;
			$row.css({ top: row_y + 'px' });

			if( row_data.remove_confirm_active && true == row_data.remove_confirm_active ){
				$row.find(".row-edit-actions").addClass('remove-confirm-active');
			} else {
				$row.find(".row-edit-actions").removeClass('remove-confirm-active');
			}

			if( row_data.removed && true == row_data.removed ){
				$row.find(".row-edit-actions").addClass('removed');
			} else {
				$row.find(".row-edit-actions").removeClass('removed');
			}
		};
	}

	function open_read_more_modal( event ){
		event.preventDefault();

		var row_index = $(this).closest('.Table-row').data('row_index');
		var row_data = show_data[ row_index ] ? show_data[ row_index ] : false;

		if( ! row_data ){
			return;
		}

		var $modal_content = $psd.find("#psd-modal-content");
		$modal_content.html("");

		for( var col in table_data.columns_default ){
			$modal_content.append( $('<h5/>').text( table_data.columns_default[ col ] ) );
			$modal_content.append( $('<p/>').html( row_data[ col ] ? row_data[ col ] : "/" ) );
		}

		if( row_data.other_columns_html ){
			$modal_content.append( $('<h5/>').text( ese_psd.psd_modal_more_columns_title ) );
			$modal_content.append( $('<p/>').html( row_data.other_columns_html ) );
		}


		$psd.find("#psd-modal").openModal();
	}






	function getRowHeight(){
		return $psd.find(".psd-row-template .Table-row").outerHeight();
	}



	function prepListeners( triggers, fun, delay ){
		$.each(triggers, function(trigger_index, trigger_string){
			$psd.on( trigger_string, listenerCaller( fun, delay ) );
		});
	}

	var listenerCallerTimeouts = {};
	function listenerCaller( fun, delay ){
		var fun_name = functionName( fun );

		listenerCallerTimeouts[ fun_name ] = null;

		return function(){
			clearTimeout( listenerCallerTimeouts[ fun_name ] );
			listenerCallerTimeouts[ fun_name ] = setTimeout( fun, delay);
		};
	}

	function functionName(fun) {
		var ret = fun.toString();
		ret = ret.substr('function '.length);
		ret = ret.substr(0, ret.indexOf('('));
		return ret;
	}

	init();

})(jQuery);