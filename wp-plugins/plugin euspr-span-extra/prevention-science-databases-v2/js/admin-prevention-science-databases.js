(function($){

	var $post;

	function init(){
		$post = $("form#post");

		$post.submit( check_hot_before_submit );

		/*******************  icon  *******************/
		$("a.change-database-icon").click( onChangeDatabaseIconClick );
		$("div.database-icons i").click( selectDatabaseIcon );


		/*******************  columns  *******************/
		$("span.button.add-column").click( columnAdd );
		$(document).on( 'click', 'li.column-item .remove', columnRemoveClick );
		$(document).on( 'click', 'li.column-setting', columnSettingClick );
		$("ul.database-columns").sortable({
			handle: '.move',
		});

		setDatabaseIcon( ese_p_s_database_v2_data.icon );
		columnCreateFromData( ese_p_s_database_v2_data.columns );

		/*******************  logs  *******************/
		$(document).on( 'click', '.ese-db-v2-log a.show-logs-action', logShowLogs );
		$(document).on( 'click', '.ese-db-v2-log a.hide-logs-action', logHideLogs );

		$(document).on( 'click', '.ese-db-v2-log a.revert-action', logRevertRow );
		$(document).on( 'click', '.ese-db-v2-log a.revert-cancel-action', logRevertCancelRow );

		$(document).on( 'click', '.ese-db-v2-log a.revert-all', logRevertAll );

	}


	/*************************************************
			DATABASE ICON - START
	*************************************************/
	
	function onChangeDatabaseIconClick( event ){
		event.preventDefault();
		
		toggleDatabaseIconPanel();
	}

	function selectDatabaseIcon( event ){
		event.preventDefault();

		var $selectedIcon = $(this);
		var iconClass = $selectedIcon.attr('data-icon-class');
		
		setDatabaseIcon( iconClass );

		toggleDatabaseIconPanel();
	}

	function setDatabaseIcon( iconClass ){
		$("div.database-icons i").removeClass('selected');
		if( iconClass ){
			$("div.database-icons i." + iconClass ).addClass('selected');
		}

		$('i.database-icon').removeClass().addClass('database-icon fa').addClass( iconClass ).attr('data-icon-class', iconClass );
	}

	function toggleDatabaseIconPanel(){
		var $a = $("a.change-database-icon");
		var $panel = $('div.database-icons');

		if( $a.find('.change').is(':visible') ){
			$a.find('.change').hide();
			$a.find('.close').show();
			$panel.show('slow');
		} else {
			$a.find('.change').show();
			$a.find('.close').hide();
			$panel.hide('slow');
		}
	}
	
	/*************************************************
			DATABASE ICON - END
	*************************************************/



	/*************************************************
			COLUMNS - START
	*************************************************/
	
	function columnCreateFromData( columns ){
		$.each(columns, function( column_index, column_data ){
			var $li = columnItemLi();

			$li.attr( 'data-colum-id', column_data.id );

			$li.find("input.column-label").val( column_data.label );
			$li.find("input.column-num-fields").val( column_data.num_fields );

			if( 'yes' == column_data.isViewOnList ) { $li.find('li.is-view-on-list').addClass('active'); }
			if( 'yes' == column_data.isEmail ) { $li.find('li.is-email').addClass('active'); }
			if( 'yes' == column_data.isWww ) { $li.find('li.is-www').addClass('active'); }
			if( 'yes' == column_data.isSearch ) { $li.find('li.is-search').addClass('active'); }
			if( 'yes' == column_data.isFilter ) { $li.find('li.is-filter').addClass('active'); }
			if( 'yes' == column_data.isWide ) { $li.find('li.is-wide').addClass('active'); }
			if( 'yes' == column_data.isShort ) { $li.find('li.is-short').addClass('active'); }

			$(".database-columns").append( $li );
		});
	}
	
	function columnItemLi(){
		var $column_li = $(".ese-psd-hot-columns-item-settings-template .column-item:first").clone();
		return $column_li;
	}

	function columnAdd(){
		var $li = columnItemLi();

		$li.attr( 'data-colum-id', uniqid() );

		$(".database-columns").append( $li );

		columnsSetTooltips();
	}

	function columnsSetTooltips(){
		$( 'li.column-setting' ).tooltip({
			effect: 'fade',
			position: { my: 'center top', at: 'center bottom+10', collision: 'none' },
			hide: false
		});
	}

	function columnSettingClick( event ){
		event.preventDefault();

		var $li = $(this);

		if( $li.hasClass('active') ){

			$li.removeClass('active');

		} else {

			$li.addClass('active');

			switch( $li.attr('data-type') ){

				case 'is-email':
					$li.closest('ul').find('li.is-www').removeClass('active');
				break;
				case 'is-www':
					$li.closest('ul').find('li.is-email').removeClass('active');
				break;

				case 'is-wide':
					$li.closest('ul').find('li.is-short').removeClass('active');
				break;
				case 'is-short':
					$li.closest('ul').find('li.is-wide').removeClass('active');
				break;
			}

		}
	}

	function columnGetData(){
		var columnsSettings = [];

		$("ul.database-columns > li").each(function( i, e ){
			var $column_li = $(e);

			var columnSettings = {
				id: $column_li.attr('data-colum-id'),

				label: $column_li.find("input.column-label").val(),
				num_fields: $column_li.find("input.column-num-fields").val(),

				isViewOnList: $column_li.find('li.is-view-on-list.active').length > 0 ? 'yes' : 'no',
				isEmail: $column_li.find('li.is-email.active').length > 0 ? 'yes' : 'no',
				isWww: $column_li.find('li.is-www.active').length > 0 ? 'yes' : 'no',
				isSearch: $column_li.find('li.is-search.active').length > 0 ? 'yes' : 'no',
				isFilter: $column_li.find('li.is-filter.active').length > 0 ? 'yes' : 'no',
				isWide: $column_li.find('li.is-wide.active').length > 0 ? 'yes' : 'no',
				isShort: $column_li.find('li.is-short.active').length > 0 ? 'yes' : 'no',
			};

			columnsSettings[ i ] = columnSettings;
		});

		return columnsSettings;
	}

	function columnRemoveClick( event ){
		event.preventDefault();

		$(this).closest('li.column-item').remove();
	}

	/*************************************************
			COLUMNS - END
	*************************************************/


	/*************************************************
			LOGS - START
	*************************************************/
	
	function logShowLogs( event ){
		event.preventDefault();
		$(event.target).closest( '.log-item' ).addClass('show-logs');
	}

	function logHideLogs( event ){
		event.preventDefault();
		$(event.target).closest( '.log-item' ).removeClass('show-logs');
	}

	function logRevertRow( event ){
		event.preventDefault();
		$(event.target).closest( '.logs-log-item' ).addClass('revert-pending');
	}

	function logRevertCancelRow( event ){
		event.preventDefault();
		$(event.target).closest( '.logs-log-item' ).removeClass('revert-pending');
	}

	function logRevertAll( event ){
		event.preventDefault();
		logShowLogs( event );
		if( $(event.target).closest( '.log-item' ).find('.logs-log-item.revert-pending').length > 0 ){
			$(event.target).closest( '.log-item' ).find('.logs-log-item.revert-pending').removeClass('revert-pending');
		} else {
			$(event.target).closest( '.log-item' ).find('.logs-log-item').addClass('revert-pending');
		}
	}
	
	/*************************************************
			LOGS - END
	*************************************************/



	/*************************************************
			FORM SUBMIT HANDLING - START
	*************************************************/
	
	function check_hot_before_submit(){
		
		// database icon
		createInput( 'icon', $("i.database-icon").attr('data-icon-class') );

		// columns
		var columns_data = columnGetData();
		$.each( columns_data, function( column_index, column_data ){

			createInput( 'columns[]', column_data.id );

			$.each( column_data, function( column_data_key, column_data_value ){
				createInput( 'column_' + column_data.id + '_' +  column_data_key, column_data_value );				
			} );

		});

		// logs
		$post.find(".logs-log-item.revert-pending").each(function( i, e ){
			var $e = $(e);
			var log_id = $e.attr('data-log-id');
			createInput( 'revert_logs[]', log_id );
		});
		
		return true;
	}

	function createInput( name, value ){
		var $input = $("<input/>");
		$input.attr('name', '_database_' + name);
		$input.attr('value', value);
		$input.attr('type', 'hidden');

		$post.append( $input );
	}
	
	/*************************************************
			FORM SUBMIT HANDLING - END
	*************************************************/


	/*************************************************
			micro jQuery plugin - START
	*************************************************/
	
	$.fn.ignore = function(sel){
		return this.clone().find(sel||">*").remove().end();
	};
	
	/*************************************************
			micro jQuery plugin - END
	*************************************************/

	/*************************************************
			UTILS - START
	*************************************************/
	
	function uniqid(pr, en) {
		var pr = pr || '', en = en || false, result;
  
		this.seed = function (s, w) {
			s = parseInt(s, 10).toString(16);
			return w < s.length ? s.slice(s.length - w) : (w > s.length) ? new Array(1 + (w - s.length)).join('0') + s : s;
		};

		result = pr + this.seed(parseInt(new Date().getTime() / 1000, 10), 8) + this.seed(Math.floor(Math.random() * 0x75bcd15) + 1, 5);
  
		if (en) result += (Math.random() * 10).toFixed(8).toString();

		return result;
	};
	
	/*************************************************
			UTILS - END
	*************************************************/

	init();

})(jQuery);