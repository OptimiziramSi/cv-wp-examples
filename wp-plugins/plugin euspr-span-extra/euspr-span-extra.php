<?php
/**
* Plugin Name: Euspr / Span Extra functionality
* Description: Description
* Plugin URI: http://2gika.si
* Author: 2gika
* Author URI: http://2gika.si
* Version: 1.0
* License: GPL2
* Text Domain: ese
* Domain Path: languages
*/

/*
Copyright (C) 2016  2gika  info@2gika.si

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/**
 * Snippet Wordpress Plugin Boilerplate based on:
 *
 * - https://github.com/purplefish32/sublime-text-2-wordpress/blob/master/Snippets/Plugin_Head.sublime-snippet
 * - http://wordpress.stackexchange.com/questions/25910/uninstall-activate-deactivate-a-plugin-typical-features-how-to/25979#25979
 *
 * By default the option to uninstall the plugin is disabled,
 * to use uncomment or remove if not used.
 *
 * This Template does not have the necessary code for use in multisite. 
 *
 * Also delete this comment block is unnecessary once you have read.
 *
 * Version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'ESE_VERSION', '1.0' );
define( 'ESE_FILE', __FILE__ );

add_action( 'plugins_loaded', array( 'EusprSpanExtra_Plugin', 'getInstance' ) );
register_activation_hook( __FILE__, array( 'EusprSpanExtra_Plugin', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'EusprSpanExtra_Plugin', 'deactivate' ) );
// register_uninstall_hook( __FILE__, array( 'EusprSpanExtra_Plugin', 'uninstall' ) );

require_once dirname( __FILE__ ) . '/libs/titan-framework-checker.php';


require_once dirname( __FILE__ ) . '/user-access-manager/user-access-manager.php';
require_once dirname( __FILE__ ) . '/board-members/board-members.php';
require_once dirname( __FILE__ ) . '/contact-form-materialize-fields/contact-form-materialize-fields.php';
require_once dirname( __FILE__ ) . '/supporting-organisations/supporting-organisations.php';
require_once dirname( __FILE__ ) . '/work-packages/work-packages.php';
require_once dirname( __FILE__ ) . '/partners/partners.php';
require_once dirname( __FILE__ ) . '/google-recaptcha/google-recaptcha.php';
require_once dirname( __FILE__ ) . '/table-of-content/table-of-content.php';
require_once dirname( __FILE__ ) . '/prevention-science-databases-v2/prevention-science-databases-v2.php';
require_once dirname( __FILE__ ) . '/scroll-to-me/scroll-to-me.php';
require_once dirname( __FILE__ ) . '/multi-panel-background/multi-panel-background.php';
require_once dirname( __FILE__ ) . '/insert-content/insert-content.php';
require_once dirname( __FILE__ ) . '/panel/panel.php';
require_once dirname( __FILE__ ) . '/ul-style/ul-style.php';
require_once dirname( __FILE__ ) . '/anchor-btn-normal/anchor-btn-normal.php';
require_once dirname( __FILE__ ) . '/wp-editor/wp-editor.php';
require_once dirname( __FILE__ ) . '/redirect-after-login/redirect-after-login.php';
require_once dirname( __FILE__ ) . '/google-analytics-no-cookie/google-analytics-no-cookie.php';
require_once dirname( __FILE__ ) . '/tool-replace-btn-shortcode/tool-replace-btn-shortcode.php';
require_once dirname( __FILE__ ) . '/tool-remove-clear-shortcode/tool-remove-clear-shortcode.php';
require_once dirname( __FILE__ ) . '/tool-remove-improper-html/tool-remove-improper-html.php';
require_once dirname( __FILE__ ) . '/tool-add-target-to-file-anchors/tool-add-target-to-file-anchors.php';
require_once dirname( __FILE__ ) . '/enable-menu-editing-for-editors/enable-menu-editing-for-editors.php';
require_once dirname( __FILE__ ) . '/cors/cors.php';
require_once dirname( __FILE__ ) . '/prevent-deactivating-plugins/prevent-deactivating-plugins.php';
require_once dirname( __FILE__ ) . '/editor-help-icon/editor-help-icon.php';
require_once dirname( __FILE__ ) . '/settings-page/settings-page.php';

class EusprSpanExtra_Plugin {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {

		// activate 2gika updater
		add_filter( '2repo_plugins', array( $this, 'register_updater' ) );

		// register styles
		// font awesome for later use when needed
		add_action( 'wp_enqueue_scripts', array( $this, 'register_styles_and_scripts') );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_styles_and_scripts') );

		// add menu pages for EUSPR and SPAN
		add_action( 'admin_menu', array( $this, 'add_ES_admin_menu' ) );

		// add additional functionality to plugin "User Access Manager"
		EusprSpanExtra_UserAccessManager::getInstance();

		// add cors on local development
		EusprSpanExtra_Cors::getInstance();

		// add board members list functionality
		EusprSpanExtra_BoardMembers::getInstance();

		// add supporting organisations list functionality
		EusprSpanExtra_SupportingOrganisations::getInstance();

		// add work packages list functionality
		EusprSpanExtra_WorkPackages::getInstance();

		// add partners list functionality
		EusprSpanExtra_Partners::getInstance();

		// add table of content functionality
		EusprSpanExtra_TableOfContent::getInstance();

		// add table of content functionality
		EusprSpanExtra_PreventionScienceDatabasesV2::getInstance();

		// add Multi Background Panel shortcode functionality
		EusprSpanExtra_MultiPanelBackground::getInstance();

		// add Insert Content shortcode functionality
		EusprSpanExtra_InsertContent::getInstance();

		// add wrap with panel div functionality
		EusprSpanExtra_Panel::getInstance();

		// add ul styles to editor functionality
		EusprSpanExtra_UlStyle::getInstance();

		// add anchor btn normal functionality
		EusprSpanExtra_AnchorBtnNormal::getInstance();

		// add google reCaptcha
		EusprSpanExtra_GoogleRecaptcha::getInstance();

		// add scroll to me JS
		EusprSpanExtra_ScrollToMe::getInstance();

		// jetpack form fields customize for materialize look
		EusprSpanExtra_ContactFormMaterializeFields::getInstance();

		// remove BTN shortcodes
		EusprSpanExtra_ToolReplaceBtnShortcode::getInstance();

		// remove CLEAR shortcodes
		EusprSpanExtra_ToolRemoveClearShortcode::getInstance();

		// remove improper html - left from attack
		EusprSpanExtra_TollRemoveImproperHtml::getInstance();

		// add target="_blank" to anchors which show link to documents
		EusprSpanExtra_ToolAddTargetToFileAnchors::getInstance();

		// remember login redirect and redirect on successful login back to page
		EusprSpanExtra_RedirectAfterLogin::getInstance();

		// enable menu editing to editors
		EusprSpanExtra_EnableMenuEditingForEditors::getInstance();

		// google analytics
		EusprSpanExtra_GoogleAnalyticsNoCookie::getInstance();

		// add wp editor shortcodes
		EusprSpanExtra_WpEditor::getInstance();

		// prevent deactivation plugins
		EusprSpanExtra_PreventDeactivatingPlugins::getInstance();

		// editor help icon
		EusprSpanExtra_EditorHelpIcon::getInstance();

		// settings page
		EusprSpanExtra_SettingsPage::getInstance();
	}

	public static function activate() {
		$databases_v2 = EusprSpanExtra_PreventionScienceDatabasesV2::getInstance();
		$databases_v2->cron_setup( true );
	}

	public static function deactivate() {
		$databases_v2 = EusprSpanExtra_PreventionScienceDatabasesV2::getInstance();
		$databases_v2->cron_deactivate();
	}

	public static function uninstall() {
		if ( __FILE__ != WP_UNINSTALL_PLUGIN )
			return;
	}


	/*************************************************
			AUTOMATIC UPDATES - START
	*************************************************/
	
	// hook for 2repo / 2gika automatic updates
	function register_updater( $sources ){
		$sources[ __FILE__ ] = 'http://euspr-span.3giki.si/';
		return $sources;
	}
	
	/*************************************************
			AUTOMATIC UPDATES - END
	*************************************************/

	/*************************************************
			REGISTER CSS - START
	*************************************************/
	
	function register_styles_and_scripts(){

		wp_register_style( 'ese-font-awesome', plugin_dir_url( __FILE__ ) . 'libs/font-awesome/font-awesome.css', array(), ESE_VERSION );

		wp_register_script( 'ese-select2', plugin_dir_url(  __FILE__ ) . 'libs/_select2.full.min.js', array(), ESE_VERSION, true );

		wp_register_script( 'ese-handsontable', plugin_dir_url(  __FILE__ ) . 'libs/handsontable/handsontable.full.js', array(), ESE_VERSION, true );
		wp_register_style( 'ese-handsontable', plugin_dir_url( __FILE__ ) . 'libs/handsontable/handsontable.full.css', array(), ESE_VERSION );

	}
	
	/*************************************************
			REGISTER CSS - END
	*************************************************/


	/*************************************************
			ADMIN MENU - START
	*************************************************/
	
	function add_ES_admin_menu(){

		add_menu_page(
			__( 'EUSPR', 'ese' ),
			__( 'EUSPR', 'ese' ),
			'edit_posts',
			'edit.php?post_type=ese_bmember',
			'',
			plugin_dir_url(  __FILE__ ) . 'assets/images/euspr-icon.png',
			'25.1'
		);

		add_menu_page(
			__( 'SPAN', 'ese' ),
			__( 'SPAN', 'ese' ),
			'edit_posts',
			'edit.php?post_type=ese_partner',
			'',
			plugin_dir_url(  __FILE__ ) . 'assets/images/span-icon.png',
			'25.2'
		);

	}
	
	/*************************************************
			ADMIN MENU - END
	*************************************************/


	/*************************************************
			UTILS - START
	*************************************************/
	
	static function is_localhost()
	{
		$server_name = $_SERVER['SERVER_NAME'];

		if( strpos($server_name, 'euspr-span.localhost') !== false ){
			return true;
		}

		return false;
	}

	static function have_required_plugins( $required_plugins ) {
        if (empty($required_plugins))
            return true;
        $active_plugins = (array) get_option('active_plugins', array());
        if (is_multisite()) {
            $active_plugins = array_merge($active_plugins, get_site_option('active_sitewide_plugins', array()));
        }
        foreach ($required_plugins as $key => $required) {
            $required = (!is_numeric($key)) ? "{$key}/{$required}.php" : "{$required}/{$required}.php";
            if (!in_array($required, $active_plugins) && !array_key_exists($required, $active_plugins))
                return false;
        }
        return true;
    }
	
	/*************************************************
			UTILS - END
	*************************************************/


}
