<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

$insert_post_query = new WP_Query(array(
	'posts_per_page' => 1,
	'page_id' => $id,
	'post_type' => array('post','page'),
	));

if( ! $insert_post_query->have_posts() ){

	echo '<div class="insert-content-not-found panel">';
	_e( 'Selected post not found', 'ese' );
	echo '</div>';
} else {
	$insert_post_query->the_post();

	echo '<div class="insert-content-content">';
		the_content();
	echo '</div>';
	wp_reset_postdata();
}
?>
