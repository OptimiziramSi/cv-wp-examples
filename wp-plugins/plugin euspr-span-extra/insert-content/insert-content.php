<?php

// TODO MAtija - zamenjaj ikono za gumb, ki se prikazuje v editorju, ki je v "PLUGIN/insert-content/images/wp_editor_btn.png"

class EusprSpanExtra_InsertContent {

	private static $instance = null;

	public $shortcode_tag = 'ese_insert_content';

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		add_shortcode( $this->shortcode_tag, array( $this, 'shortcode_handler' ) );

		if( ! is_admin() ){
			return;
		}

		// enable WP EDITOR functionality
		add_filter('ese_wpe_mce_external_plugins_atts', array( $this, 'ese_wpe_mce_external_plugins_atts' ) );
		add_filter('ese_wpe_mce_external_plugins', array( $this, 'ese_wpe_mce_external_plugins' ) );
		add_filter('ese_wpe_mce_buttons', array( $this, 'ese_wpe_mce_buttons' ) );
		add_action('ese_wpe_admin_enqueue_scripts', array( $this, 'ese_wpe_admin_enqueue_scripts' ) );
		add_filter('ese_wpe_mce_css', array( $this, 'ese_wpe_mce_css' ) );
	}

	function shortcode_handler($atts){
		extract( shortcode_atts(
			array(
				'id' => '',
			), $atts )
		);
		
		ob_start();
		include dirname( __FILE__ ) . '/views/shortcode.php';
		$output = ob_get_clean();
		
		return $output;
	}


	/*************************************************
			WP EDITOR - START
	*************************************************/
	
	function ese_wpe_mce_external_plugins_atts( $plugins_atts ){
		$pages_options = array();
		$posts_options = array();

		$page_ids = get_all_page_ids();
		foreach($page_ids as $page_id){
			$pages_options[ $page_id ] = esc_html( get_the_title( $page_id ) );
		}
		$post_ids = get_posts(array( 'fields' => 'ids', 'posts_per_page' => -1, ) );
		foreach($post_ids as $post_id){
			$posts_options[ $post_id ] = esc_html( get_the_title( $post_id ) );
		}


		$plugins_atts[$this->shortcode_tag ] = array(
			'name' => __( 'Insert Page / Post content', 'ese' ),
			'button_tooltip' => __( 'Insert page/post content', 'ese' ),
			'button_image' => plugins_url( 'images/wp_editor_btn.png' , __FILE__ ),
			'double_click_to_edit' => __( 'Double click on me to edit content', 'ese' ),
			'preview_text' => __( 'Here will be contents for:', 'ese' ),
			'preview_title_none' => __( 'No content selected yet', 'ese' ),
			'preview_title_not_found' => __( 'Selected content not found', 'ese' ),
			'page_label' => __( 'Page', 'ese' ),
			'post_label' => __( 'Post', 'ese' ),
			'shortcode_attributes' => array(
				'id' => array(
					'pages' => array(
						'group' => __( 'Pages', 'ese' ),
						'options' => $pages_options,
					),
					'posts' => array(
						'group' => __( 'Posts', 'ese' ),
						'options' => $posts_options,
					),
				)
			),
		);
		return $plugins_atts;
	}
	function ese_wpe_mce_external_plugins( $plugin_array ){
		$plugin_array[$this->shortcode_tag ] = plugins_url( 'js/wp_editor.js' , __FILE__ );
		return $plugin_array;
	}

	function ese_wpe_mce_buttons( $buttons ){
		array_push( $buttons, $this->shortcode_tag );
		return $buttons;
	}

	function ese_wpe_admin_enqueue_scripts(){
		// wp_enqueue_style($this->shortcode_tag, plugins_url( 'css/wp_editor.css' , __FILE__ ) );
	}

	function ese_wpe_mce_css( $mce_css ) {
		if ( ! empty( $mce_css ) )
			$mce_css .= ',';

		$mce_css .= plugins_url( 'css/wp_editor.css', __FILE__ );
		return $mce_css;
	}
	
	/*************************************************
			WP EDITOR - END
	*************************************************/





}