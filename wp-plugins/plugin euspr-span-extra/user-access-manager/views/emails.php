<?php

if ( ! defined( 'ABSPATH' ) ) { 
	exit; // Exit if accessed directly
}

?>

<div class="wrap">
	<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
		<?php wp_nonce_field('ese-uam-emails', 'ese-uam-emails-nonce'); ?>
		<input type="hidden" value="ese_uam_save" name="action" />
		<h2><?php echo TXT_ESE_UAM_EDIT_EMAILS; ?></h2>
		<p><?php echo TXT_ESE_UAM_EDIT_EMAILS_DESCRIPTION; ?></p>
		<div class="tablenav">
			<div class="alignleft">
				<input type="submit" class="button button-primary" name="save" value="<?php echo TXT_ESE_UAM_SAVE; ?>" />
			</div>
			<br class="clear" />
		</div>

		<br class="clear" />
		<table class="widefat">
			<thead>
				<tr class="thead">
					<th scope="col"><?php echo TXT_ESE_UAM_GROUP_NAME; ?></th>
					<th scope="col"><?php echo TXT_ESE_UAM_GROUP_EMAILS; ?></th>
				</tr>
			</thead>
			<tbody>
			
			<?php
			if (isset($aUamUserGroups)):
			foreach ($aUamUserGroups as $oUamUserGroup) :
			?>
			<tr class="alternate" id="group-<?php echo $oUamUserGroup->getId(); ?>">
				<td>
					<strong><?php echo $oUamUserGroup->getGroupName(); ?></strong>
				</td>
				<td>
					<!-- TODO - escape text for security -->
					<textarea rows="5" name="ese_uam_emails[<?php echo esc_attr( $oUamUserGroup->getId() ); ?>]" class="large-text code"><?php echo is_array( $oUamUserGroup->emails ) ? implode( ", ", $oUamUserGroup->emails ) : ""; ?></textarea>
				</td>
			</tr>
			<?php
			endforeach;
			endif;
			?>
			</tbody>
		</table>

		<p>
			<label for="ese-uam-options[access-info-email]">
				<?php _e( 'More info contact email', 'ese' ); ?>
				<input name="ese-uam-options[access-info-email]" type="email" id="ese-uam-options[access-info-email]" class="regular-text ltr" value="<?php echo esc_attr( $options['access-info-email'] ); ?>">
				<?php echo sprintf( __( 'Otherwise admin email will be used (%s)', 'ese' ), get_option( 'admin_email' ) ); ?>
			</label>
		</p>

		<p>
			<label for="check-all-users">
				<input name="check-all-users" type="checkbox" id="check-all-users" value="yes">
				<?php _e( 'Check all mails and users access groups (can take a while)', 'ese' ); ?>
			</label>
		</p>
		
		<div class="tablenav bottom">
			<div class="alignleft">
				<input type="submit" class="button button-primary" name="save" value="<?php echo TXT_ESE_UAM_SAVE; ?>" />
			</div>
			<br class="clear" />
		</div>
	</form>
</div>