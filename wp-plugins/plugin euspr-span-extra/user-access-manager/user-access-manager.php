<?php

// TODO - preveri koliko mailov je v resnici v posameznem dostopu (če govorimo nad 1000, moramo nekaj spremenit)
// TODO - vse nastavitve, ki jih imamo nastavljene forsiraj, da ne bo kasneje težav

class EusprSpanExtra_UserAccessManager {

	private static $instance = null;

	const EMAILS_OPTION = "ese-uam-emails";
	const OPTION = "ese-uam";

	const POST_CONTENT = "ESE_UAM_ACCESS_NOT_ALLOWED_FOR_POST_CONTENT";
	const POST_COMMENT_CONTENT = "ESE_UAM_ACCESS_NOT_ALLOWED_FOR_POST_COMMENT_CONTENT";

	const PAGE_CONTENT = "ESE_UAM_ACCESS_NOT_ALLOWED_FOR_PAGE_CONTENT";
	const PAGE_COMMENT_CONTENT = "ESE_UAM_ACCESS_NOT_ALLOWED_FOR_PAGE_COMMENT_CONTENT";

	const LOCK_ICON = "ESE_UAM_LOCK_ICON";
	const UNLOCK_ICON = "ESE_UAM_UNLOCK_ICON";

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! EusprSpanExtra_Plugin::have_required_plugins( array('wp-session-manager', 'user-access-manager' ) ) ){
			return;
		}

		require_once dirname( __FILE__ ) . '/language.define.php';

		// check which pages are blocked and set the right content
		add_filter( 'get_pages', array( $this, 'showAccessRequiredContent'), 20 );
		add_filter( 'the_posts', array( $this, 'showAccessRequiredContent'), 20 );
		// custom excerpt for access denied content
		add_filter( 'the_excerpt', array( $this, 'excerptAccessRequiredContent' ), 9999 );

		// remove wpautop when content locked
		add_filter( 'the_content', array( $this, 'remove_wpautop_filter_on_locked_content' ), 1 );
		add_filter( 'the_content', array( $this, 'return_wpautop_filter_on_locked_content' ), 9999 );

		// handle account request form
		add_action( 'admin_post_ese_uam_request_account', array( $this, 'requestAccountHandle' ) );
		add_action( 'admin_post_nopriv_ese_uam_request_account', array( $this, 'requestAccountHandle' ) );

		// change message on lost password form
		add_filter( 'wp_login_errors' , array( $this, 'custom_message_on_reset_password' ), 10, 2 );

		// show locks in menu
		add_filter( 'nav_menu_css_class', array( $this, 'nav_menu_css_class'), 10, 2 );
		// show locks on title
		add_filter( 'document_title_parts', array( $this, 'document_title_parts' ), 10 );
		add_filter( 'wpseo_title', array( $this, 'wpseo_title' ), 10 );
		add_filter( 'the_title', array( $this, 'the_title' ), 999 );

		// TODO - remember redirect on login request
		
		// TODO - redirect on site after login, if redirect exists
		
		// redirect to front if subscriber only

		
		if( is_admin() ){
			// add submenu
			add_action( 'admin_menu', array( $this, 'add_submenu_page' ) );

			// force some extra settings on UAM plugin on save
			add_action( 'uam_update_options', array( $this, 'uam_update_options' ), 20, 1 );
		}
	}


	/*************************************************
			EMAILS PAGE - START
	*************************************************/
	
	public function add_submenu_page(){

		// add_submenu_page( 'uam_usergroup', TXT_ESE_UAM_SUBMENU_TITLE, TXT_ESE_UAM_SUBMENU_TITLE, 'manage_options', 'ese-uam-emails', array( $this, 'renderEmailsPage' ) );
		add_submenu_page( 'edit.php?post_type=ese_bmember', TXT_ESE_UAM_SUBMENU_TITLE, TXT_ESE_UAM_SUBMENU_TITLE, 'edit_posts', 'ese-uam-emails', array( $this, 'renderEmailsPage' ) );

	}

	public function renderEmailsPage(){

		if( ! is_admin() ){
			return;
		}

		$this->handleEmailsSaveRequest();

		global $oUserAccessManager;
		$aUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();

		// add emails to access groups params
		if( is_array( $aUamUserGroups ) ){
			foreach ($aUamUserGroups as $oUamUserGroup_key => $oUamUserGroup) {
				$oUamUserGroup->emails = $this->getEmails( $oUamUserGroup->getId() );
				$aUamUserGroups[ $oUamUserGroup_key ] = $oUamUserGroup;
			}
		}

		// options params
		$options = $this->getOptions();

		// echo tempplate
		include dirname( __FILE__ ) . '/views/emails.php';
	}

	private function optionsDefaults(){
		return array(
			'access-info-email' => ''
		);
	}
	private function getOptions(){
		return shortcode_atts(
				$this->optionsDefaults(),
				get_option( self::OPTION )
			);
	}


	private function handleEmailsSaveRequest(){

		global $oUserAccessManager;
		$aUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();

		// handle save request
		if ( isset( $_POST['action'] ) && 'ese_uam_save' == $_POST['action'] ) {
			if ( ! isset( $_POST['ese-uam-emails-nonce'] ) || ! wp_verify_nonce( $_POST['ese-uam-emails-nonce'], 'ese-uam-emails') ) {
				 wp_die( TXT_ESE_UAM_NONCE_FAILURE );
			}

			$new_emails = array();
			if( isset( $_POST['ese_uam_emails'] ) && is_array( $_POST['ese_uam_emails'] ) ){
				if( is_array( $aUamUserGroups ) ){
					foreach ($aUamUserGroups as $oUamUserGroup_key => $oUamUserGroup) {
						$group_id = $oUamUserGroup->getId();
						$new_emails[ $group_id ] = isset( $_POST['ese_uam_emails'][$group_id] ) ? $this->extractEmails( $_POST['ese_uam_emails'][$group_id] ) : array();
					}
				}
			}

			// handle email changes and automaticaly asign new access values for users
			$old_emails = $this->getEmails();
			foreach ( $new_emails as $new_group_id => $new_group_emails ) {
				/*******************  find removed emails and remove group access from existing users  *******************/
				if( isset( $old_emails[ $new_group_id ] ) && is_array( $old_emails[ $new_group_id ] ) ){
					$old_group_emails = $old_emails[ $new_group_id ];
				} else {
					$old_group_emails = array();
				}
				$group_removed_emails = array_diff( $old_group_emails, $new_group_emails );
				foreach ( $group_removed_emails as $group_removed_email ) {
					$remove_group_from_user = get_user_by( 'email', $group_removed_email );
					if( $remove_group_from_user ){
						$this->removeGroupFromUser( $remove_group_from_user->ID, $new_group_id );
						// TODO - add message / count / report for later notice
					}
				}

				/*******************  find added mails and add groups access to existing users  *******************/				
				if( isset( $old_emails[ $new_group_id ] ) && is_array( $old_emails[ $new_group_id ] ) ){
					$old_group_emails = $old_emails[ $new_group_id ];
				} else {
					$old_group_emails = array();
				}
				$group_added_emails = array_diff( $new_group_emails, $old_group_emails );
				foreach ( $group_added_emails as $group_added_email ) {
					$add_group_to_user = get_user_by( 'email', $group_added_email );
					if( $add_group_to_user ){
						$this->addGroupToUser( $add_group_to_user->ID, $new_group_id );
						// TODO - add message / count / report for later notice
					}
				}
			}

			// save new emails
			update_option( self::EMAILS_OPTION, $new_emails );


			// save other options
			$options = shortcode_atts( $this->optionsDefaults(), isset( $_POST['ese-uam-options'] ) ? $_POST['ese-uam-options'] : array() );
			update_option( self::OPTION, $options );

			// check if
			if( isset( $_POST['check-all-users'] ) && "yes" == $_POST['check-all-users'] ){
				$this->checkAllEmailUsers();
				// TODO - add notice / report
			}

			// TODO - add success message
			// TODO - add report message / how many user removed from groups and how many added
		}

	}

	private function removeGroupFromUser( $user_id, $remove_from_group_id ){
		$sObjectType = 'user';
		$iObjectId = $user_id;

		global $oUserAccessManager;
		$oUamAccessHandler = $oUserAccessManager->getAccessHandler();
		$aRemoveUserGroups = $oUamAccessHandler->getUserGroupsForObject($sObjectType, $iObjectId);
		$aUamUserGroups = $oUamAccessHandler->getUserGroups();
		$blRemoveOldAssignments = true;

		foreach ($aUamUserGroups as $sGroupId => $oUamUserGroup) {
			if ( $remove_from_group_id == $sGroupId ) {
				$oUamUserGroup->removeObject($sObjectType, $iObjectId);
				$oUamUserGroup->save($blRemoveOldAssignments);
				return;
			}
		}
	}

	private function addGroupToUser( $user_id, $add_to_group_id ){
		$sObjectType = 'user';
		$iObjectId = $user_id;

		global $oUserAccessManager;
		$oUamAccessHandler = $oUserAccessManager->getAccessHandler();
		$aRemoveUserGroups = $oUamAccessHandler->getUserGroupsForObject($sObjectType, $iObjectId);
		$aUamUserGroups = $oUamAccessHandler->getUserGroups();
		$blRemoveOldAssignments = true;

		foreach ($aUamUserGroups as $sGroupId => $oUamUserGroup) {
			if ( $add_to_group_id == $sGroupId ) {
				$oUamUserGroup->addObject($sObjectType, $iObjectId);
				$oUamUserGroup->save($blRemoveOldAssignments);
				return;
			}
		}
	}

	private function checkAllEmailUsers(){
		$groups_by_mail = array();

		// group access groups by email
		$allEmails = $this->getEmails();
		foreach ( $allEmails as $group_id => $group_emails ) {
			foreach ( $group_emails as $group_email ) {
				if( ! is_email( $group_email ) ){
					continue;
				}
				
				if( ! isset( $groups_by_mail[ $group_email ] ) || ! is_array( $groups_by_mail[ $group_email ] ) ){
					$groups_by_mail[ $group_email ] = array();
				}

				$groups_by_mail[ $group_email ][] = $group_id;
			}
		}

		// set groups access to emails that are also wp users
		foreach ( $groups_by_mail as $email => $groups ) {
			$wp_user = get_user_by( 'email', $email );
			if( ! $wp_user ){
				continue;
			}
			$this->assignGroupsToUser( $wp_user->ID, $groups );
		}

		// check other wp users and remove all groups
		$args = array(
			'role'         => 'subscriber',
		); 
		$wp_users = get_users( $args );
		foreach ( $wp_users as $wp_user ) {
			if( isset( $groups_by_mail[ $wp_user->user_email ] ) ){
				// we already manage that user
				continue;
			}
			// remove all group access
			$this->assignGroupsToUser( $wp_user->ID, array() );
		}
		
	}
	
	/*************************************************
			EMAILS PAGE - END
	*************************************************/


	/*************************************************
			HANDLING EMAILS - START
	*************************************************/
	
	private function getEmails( $groupId = null ){
		$allMails = get_option( self::EMAILS_OPTION );

		if( ! is_array( $allMails ) ){
			$allMails = array();
		}

		if( is_null( $groupId ) ){
			return $allMails;
		} else {
			return isset( $allMails[ $groupId ] ) && is_array( $allMails[ $groupId ] ) ? $allMails[ $groupId ] : array();
		}
	}

	private function extractEmails( $emails ){
		if( ! is_string( $emails ) ){
			return array();
		}
		$emails = str_replace( array(' ', ';', "\n\r", "\n", PHP_EOL ), ",", $emails );
		$emails_arr = explode( ",", $emails );
		$emails_arr = array_map('trim', $emails_arr );
		$emails_arr = array_filter( $emails_arr );
		$emails_arr = array_values( $emails_arr );
		$emails_arr = array_map('is_email', $emails_arr);
		return $emails_arr;
	}

	private function groupsWithEmail( $email ){
		if( ! is_email( $email ) ){
			return array();
		}

		$groupsWithEmail = array();

		$allEmails = $this->getEmails();

		global $oUserAccessManager;
		$aUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();

		foreach ( $aUamUserGroups as $aUamUserGroup ) {
			if( ! isset( $allEmails[ $aUamUserGroup->getId() ] ) ){
				continue;
			}

			if( is_array( $allEmails[ $aUamUserGroup->getId() ] ) && in_array( $email, $allEmails[ $aUamUserGroup->getId() ] ) ){
				$groupsWithEmail[] = $aUamUserGroup->getId();
			}
		}

		return $groupsWithEmail;
	}

	/*************************************************
			HANDLING EMAILS - END
	*************************************************/



	/*************************************************
			CHECK UAM OPTIONS - START
	*************************************************/
	
	public function uam_update_options( $aUamOptions ){
		$aUamOptions_override = array(
			'post_content' => self::POST_CONTENT,
			'post_comment_content' => self::POST_COMMENT_CONTENT,
			'page_content' => self::PAGE_CONTENT,
			'page_comment_content' => self::PAGE_COMMENT_CONTENT,
			'show_post_content_before_more' => 'false',
		);

		$aUamOptions = array_merge( $aUamOptions, $aUamOptions_override );

		global $oUserAccessManager;
		update_option( $oUserAccessManager->getAdminOptionsName(), $aUamOptions );
	}
	
	/*************************************************
			CHECK UAM OPTIONS - END
	*************************************************/




	/*************************************************
			CONTENT FOR ACCESS REQUIRED - START
	*************************************************/
	
	public function showAccessRequiredContent( $pages = array() ){
		if( is_admin() ){
			return $pages;
		}

		global $oUserAccessManager;
		$oUamAccessHandler = $oUserAccessManager->getAccessHandler();

		foreach ( $pages as $key => $page ) {
			if( $this->isPageAccessDenied( $page->post_content ) ){
				$page->access_denied = true;
				$page->post_content = $this->getAccessRequiredContent( $page );
			}
			$page->post_title = $this->getAccessRequiredTitle( $page );
			$pages[ $key ] = $page;
		}
		return $pages;
	}

	public function excerptAccessRequiredContent( $content ){
		global $post;

		if( isset( $post->access_denied ) && $post->access_denied ){
			$content = $this->getAccessRequiredContent( $post );
		}
		return $content;
	}

	private function isPageAccessDenied( $content ){
		return in_array( $content, array(
				self::POST_CONTENT,
				self::POST_COMMENT_CONTENT,
				self::PAGE_CONTENT,
				self::PAGE_COMMENT_CONTENT,
			) );
	}

	private function getAccessRequiredTitle( $page ){
		$title_prefix = "";
		
		global $oUserAccessManager;
		$oUamAccessHandler = $oUserAccessManager->getAccessHandler();

		if( count($oUamAccessHandler->getUserGroupsForObject($page->post_type, $page->ID)) > 0 ){
			if( !$oUamAccessHandler->checkObjectAccess($page->post_type, $page->ID) ){
				$title_prefix = self::LOCK_ICON;
				// $title_prefix = '<i class="icon-lock"></i>';
			} else {
				$title_prefix = self::UNLOCK_ICON;
				// $title_prefix = '<i class="icon-unlock"></i>';
			}
		}

		return $title_prefix . $page->post_title;
	}

	private function getAccessRequiredContent( $page ){

		$content = "";

		$content .= '<div class="content-access-warning-wrapper">';

			$content .= '<p>' . esc_html__( 'This content is only available for:', 'ese' ) . '</p>';
			$content .= '<ul>';
				$post_access_groups = $this->getPostAccessGroups( $page );
				foreach ( $post_access_groups as $post_access_group ) {
					$content .= '<li>'.esc_html( $post_access_group ).'</li>';
				}
			$content .= '</ul>';

			/*******************  USE LONG CONTENT / SINGLE PAGE OR POST *******************/
			if( is_singular() ){
				/*******************  user is logged in  *******************/
				if( is_user_logged_in() ){
					$content .= '<p>' . esc_html__( 'You can only view contents available for:', 'ese' ) . '</p>';
					$content .= '<ul>';
						$user_access_groups = $this->getUserAccessGroups( get_current_user_id() );
						foreach ( $user_access_groups as $user_access_group ) {
							$content .= '<li>'.esc_html( $user_access_group ).'</li>';
						}
					$content .= '</ul>';

					$access_info_email = $this->getAccessContactEmail();
					$content .= "<p>";
					$content .= sprintf( __( 'For more information please contact <a href="mailto:%s">%s</a>.', 'ese' ), esc_attr( $access_info_email ), esc_html( $access_info_email ) );
					$content .= "</p>";
				}
				/*******************  user is not logged in  *******************/
				else {
					$access_info_email = $this->getAccessContactEmail();
					$content .= "<p>";
					$content .=  __( 'If you already have account, please use form to login. To get access, please "Request account" (form on the right), use your email that you gave as atenddee at the conference or in the member request form.', 'ese' );
					$content .= "</p>";
					$content .= $this->getLoginRegisterForm();
					$content .= "<p>";
					$content .= sprintf( __( 'For more information please contact <a href="mailto:%s">%s</a>.', 'ese' ), esc_attr( $access_info_email ), esc_html( $access_info_email ) );
					$content .= "</p>";
				}
			}

			/*******************  USE SHORT CONTENT / LOOPS AND LISTS *******************/
			else {
				/*******************  user is logged in  *******************/
				if( is_user_logged_in() ){
					$content .= "<p>";
					$content .= sprintf( __( 'Click <a href="%s">here</a> for more information.', 'ese' ), esc_url( get_permalink( $page->ID ) ) );
					$content .= "</p>";
				}
				/*******************  user is not logged in  *******************/
				else {
					$content .= "<p>";
					$content .= sprintf( __( 'Click <a href="%s">here</a> for more information.', 'ese' ), esc_url( get_permalink( $page->ID ) ) );
					$content .= "</p>";
				}
			}

		$content .= '</div>';

		return $content;
	}

	private function getLoginRegisterForm(){
		ob_start();

		echo '<div class="access-login-wrapper">';

			echo '<div class="login-form-wrapper half-c">';

				/*******************  LOGIN FORM  *******************/
				echo "<h3>";
					echo  __( 'Login with your account', 'ese' );
				echo "</h3>";
				add_filter( 'login_form_middle', array( $this, 'loginFormCustomRememberMeField' ), 999 );
				add_filter( 'login_form_middle', array( $this, 'loginRedirectInput' ), 999 );
				add_filter( 'login_form_middle', array( $this, 'loginFormLostPasswordLink' ), 999 );
				add_filter( 'login_form_middle', array( $this, 'loginGoogleRecaptcha' ), 999 );
				$login_form_args = array(
					'echo' => true,
					'remember' => false,
					'label_username' => __( 'Username (Your email address)', 'ese' )

				);
				wp_login_form( $login_form_args );
				remove_filter( 'login_form_middle', array( $this, 'loginFormCustomRememberMeField' ), 999 );
				remove_filter( 'login_form_middle', array( $this, 'loginRedirectInput' ), 999 );
				remove_filter( 'login_form_middle', array( $this, 'loginFormLostPasswordLink' ), 999 );
				remove_filter( 'login_form_middle', array( $this, 'loginGoogleRecaptcha' ), 999 );

			echo "</div>";

			/*******************  REGISTER / REQUIRE ACCOUNT  *******************/
			?>
			<div class="request-account-form-wrapper half-c">
				<h3><?php _e( 'Request account', 'ese' ); ?></h3>
				<p>
					<?php
					$access_info_email = $this->getAccessContactEmail();
					$sprintf_string = __('<strong>Accessing locked content:</strong> If you don not have an account yet and you became a member or you were atendee at conference, use your email (that you used to register with) and we will send you access data.<br><br> <strong>Editing "Mapping prevention" databases:</strong> If you are requesting to edit or add new data in "Mapping Prevention Database", first send a request email at <a target="_blank" href="mailto:%s">%s</a> with a mail request first.<br><br>When you are approved to edit the databases, you can use "Request account" form and account will be generated for you. Thank you.', 'ese' );
					echo sprintf( $sprintf_string, esc_attr( $access_info_email ), esc_html( $access_info_email ) );
					?>
					
				<?php $session = $this->session(); ?>

				<?php if( $this->session_get( 'request-account-error' ) ){
					$errors = is_array( $this->session_get( 'request-account-error' ) ) ? $this->session_get( 'request-account-error' ) : array();
					$this->session_unset( 'request-account-error' );
					foreach ( $errors as $error ) {
						?>
						<div class="card-panel red lighten-1" data-scroll-to-me><?php echo $error; ?></div>
						<?php
					}
				} ?>

				<?php if( $this->session_get( 'request-account-message' ) ){
					$messages = is_array( $this->session_get( 'request-account-message' ) ) ? $this->session_get( 'request-account-message' ) : array();
					unset( $session[ 'request-account-message' ] );
					$this->session_unset( 'request-account-message' );
					foreach ( $messages as $message ) {
						?>
						<div class="card-panel green lighten-1" data-scroll-to-me><?php echo $message; ?></div>
						<?php
					}
				} ?>

				<form method="post" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
					<div class="input-field">
						<input id="request-account-email" type="email" name="request-account-email" class="validate">
						<label for="request-account-email"><?php esc_html_e( 'Email', 'ese' ); ?></label>
					</div>
					<?php
					// add google reCaptcha
					$reCaptcha = $this->reCaptcha();
					$reCaptcha->render( true );
					?>
					<input type="submit" value="<?php esc_attr_e( 'Request', 'ese' ); ?>" class="button-primary">
					<input type="hidden" name="action" value="ese_uam_request_account">
					<input type="hidden" name="redirect" value="<?php echo esc_url( $this->currentUrl() ); ?>">
					<?php wp_nonce_field( 'ese-uam-request-account', 'ese-uam-request-account-nonce' ); ?>
				</form>
			</div>
			<?php

		echo "</div>";

		return ob_get_clean();
	}

	private function currentUrl(){
		return home_url( add_query_arg( NULL, NULL ) );
	}

	public function loginFormCustomRememberMeField( $content ){
		ob_start(); ?><p class="login-remember"><input type="checkbox" class="filled-in" value="forever" id="rememberme" name="rememberme" /><label for="rememberme"><?php _e( 'Remember Me' ); ?></label></p><?php
		$content .= ob_get_clean();
		return $content;
	}

	public function loginRedirectInput( $content ){
		ob_start(); ?><input type="hidden" name="ese_login_redirect_url" value="<?php echo esc_attr( add_query_arg(null, null) ); ?>"><?php
		$content .= ob_get_clean();
		return $content;
	}

	public function loginFormLostPasswordLink( $content ){
		ob_start(); ?><p class="lost-password"><a href="<?php echo esc_attr( wp_lostpassword_url() ); ?>"><?php _e( 'Lost password?', 'ese' ); ?></a></p><?php
		$content .= ob_get_clean();
		return $content;
	}

	public function loginGoogleRecaptcha( $content ){
		ob_start();
		$reCaptcha = $this->reCaptcha();
		$reCaptcha->render( true );
		$content .= ob_get_clean();
		return $content;
	}

	private function getPostAccessGroups( $post ){
		global $oUserAccessManager;
		$aUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();
		$aUserGroupsForObject = $oUserAccessManager->getAccessHandler()->getUserGroupsForObject($post->post_type, $post->ID);

		$post_groups = array();
		foreach ( $aUamUserGroups as $group ) {
			if ( ! isset($aUserGroupsForObject[$group->getId()])) {
				continue;
			}
			$group_title = $group->getGroupName();
			$group_desc = $group->getGroupDesc();

			if( ! empty( $group_desc ) ){
				$post_groups[] = $group_desc;
			} else {
				$post_groups[] = $group_title;
			}
		}
		return $post_groups;
	}

	private function getUserAccessGroups( $user_id ){
		global $oUserAccessManager;
		$aUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();
		$aUserGroupsForObject = $oUserAccessManager->getAccessHandler()->getUserGroupsForObject('user', $user_id);

		$user_groups = array();
		foreach ( $aUamUserGroups as $group ) {
			if ( ! isset($aUserGroupsForObject[$group->getId()])) {
				continue;
			}
			$group_title = $group->getGroupName();
			$group_desc = $group->getGroupDesc();

			if( ! empty( $group_desc ) ){
				$user_groups[] = $group_desc;
			} else {
				$user_groups[] = $group_title;
			}
		}
		return $user_groups;
	}

	private function getAccessContactEmail(){
		$options = get_option( self::OPTION );
		$email = isset( $options['access-info-email'] ) && is_email( $options['access-info-email'] ) ? $options['access-info-email'] : get_option( 'admin_email' );
		return $email;
	}

	public function remove_wpautop_filter_on_locked_content( $content ){
		global $post;
		if( isset( $post->access_denied ) && $post->access_denied ){
			remove_filter( 'the_content', 'wpautop' );
		}
		return $content;
	}

	public function return_wpautop_filter_on_locked_content( $content ){
		global $post;
		if( isset( $post->access_denied ) && $post->access_denied ){
			add_filter( 'the_content', 'wpautop' );
		}
		return $content;
	}

	/*************************************************
			CONTENT FOR ACCESS REQUIRED - END
	*************************************************/





	/*************************************************
			REQUEST ACCOUNT - START
	*************************************************/
	
	public function requestAccountHandle(){

		if( ! isset( $_POST['action'] ) || "ese_uam_request_account" != $_POST['action'] || ! isset( $_POST['ese-uam-request-account-nonce'] ) || ! wp_verify_nonce( $_POST['ese-uam-request-account-nonce'], 'ese-uam-request-account' ) ){
			$errors[] = __( 'Not using it right!', 'ese' );
			$this->requestAccountResponde( 'error', $errors );
		}

		$errors = array();
		$messages = array();

		// check google recaptcha
		$reCaptcha = $this->reCaptcha();
		if( ! $reCaptcha->check() ){
			$errors[] = __( 'You have entered an incorrect reCAPTCHA value.', 'ese' );
			$this->requestAccountResponde( 'error', $errors );
		}

		if( is_user_logged_in() ){
			$errors[] = __( 'You are already logged in and can not request account.', 'ese' );
			$this->requestAccountResponde( 'error', $errors );
		}		

		$email = isset( $_POST['request-account-email'] ) ? $_POST['request-account-email'] : '';

		if( ! is_email( $email ) ){
			$errors[] = __( 'Email format is not ok.', 'ese' );
			$this->requestAccountResponde( 'error', $errors );
		}

		if( email_exists( $email ) ){
			$errors[] = __( 'This account already exists. Please use login form to login.', 'ese' );
			$this->requestAccountResponde( 'error', $errors );
		}

		$groupsWithEmail = $this->groupsWithEmail( $email );

		if( 0 == count( $groupsWithEmail ) ){
			$access_info_email = $this->getAccessContactEmail();
			$errors[] = __( 'You email is not listed in any access group. Be sure to use email you used on conference or as a member.', 'ese' );
			$errors[] = sprintf( __( 'For more information please contact <a href="mailto:%s">%s</a>.', 'ese' ), esc_attr( $access_info_email ), esc_html( $access_info_email ) );
			$this->requestAccountResponde( 'error', $errors );
		}

		// we can now create account
		$random_password = wp_generate_password( 12, true, false );
		$created_user_id = wp_create_user( $email, $random_password, $email );
		if( is_wp_error( $created_user_id ) ){
			$access_info_email = $this->getAccessContactEmail();
			$errors[] = __( 'There was an error when creating account for you.', 'ese' );
			$errors[] = __( 'Erro message:', 'ese' ) . ' ' . $created_user_id->get_error_message();
			$errors[] = sprintf( __( 'For more information please contact <a target="_blank" href="mailto:%s">%s</a>.', 'ese' ), esc_attr( $access_info_email ), esc_html( $access_info_email ) );
			$this->requestAccountResponde( 'error', $errors );
		}

		// asign groups to that account
		$this->assignGroupsToUser( $created_user_id, $groupsWithEmail );

		// send email with access data
		wp_new_user_notification( $created_user_id, null, 'both' );

		// all ok
		$messages[] = __( 'Account created. You should recieve email with login information on your email.', 'ese' );
		$this->requestAccountResponde( 'message', $messages );
	}

	private function assignGroupsToUser( $user_id, $groups ){
		$sObjectType = 'user';
		$iObjectId = $user_id;
		$aUserGroups = $groups;

		$aAddUserGroups = array_flip($aUserGroups);

		global $oUserAccessManager;
		$oUamAccessHandler = $oUserAccessManager->getAccessHandler();
		$aRemoveUserGroups = $oUamAccessHandler->getUserGroupsForObject($sObjectType, $iObjectId);
		$aUamUserGroups = $oUamAccessHandler->getUserGroups();
		$blRemoveOldAssignments = true;

		foreach ($aUamUserGroups as $sGroupId => $oUamUserGroup) {
			if (isset($aRemoveUserGroups[$sGroupId])) {
				$oUamUserGroup->removeObject($sObjectType, $iObjectId);
			}

			if (isset($aAddUserGroups[$sGroupId])) {
				$oUamUserGroup->addObject($sObjectType, $iObjectId);
			}

			$oUamUserGroup->save($blRemoveOldAssignments);
		}
	}

	private function requestAccountRedirect(){
		$redirect_url = isset( $_POST['_wp_http_referer'] ) ? $_POST['_wp_http_referer'] : '';

		if( isset( $_POST['redirect'] ) && ! empty( $_POST['redirect'] ) ){
			$redirect_url = $_POST['redirect'];
		}

		return $redirect_url;
	}

	private function requestAccountResponde( $param, $data ){
		$redirect_url = $this->requestAccountRedirect();

		$session = $this->session();
		$session[ 'request-account-' . $param ] = $data;

		wp_redirect( $redirect_url );

		exit();
	}

	function custom_message_on_reset_password( $errors, $redirect_link ){
		if( isset( $errors->errors['confirm'] ) && isset( $errors->errors['confirm'][0] ) ){
			$errors->errors['confirm'][0] = __( 'Check your email for the confirmation link (check also spam folder just in case)', 'ese' );
		}

		return $errors;
	}
	
	/*************************************************
			REQUEST ACCOUNT - END
	*************************************************/

	/*************************************************
			LOCKS ON CONTENTS / TITLES - START
	*************************************************/
	
	function nav_menu_css_class( $classes, $item ){

		global $oUserAccessManager;
		$oUamAccessHandler = $oUserAccessManager->getAccessHandler();

		if( count($oUamAccessHandler->getUserGroupsForObject($item->object, $item->object_id)) > 0 ){
			if( !$oUamAccessHandler->checkObjectAccess($item->object, $item->object_id) ){
				$classes[] = 'ese-locked';
			} else {
				$classes[] = 'ese-unlocked';
			}
		}

		return $classes;
	}

	function the_title( $title ){
		if( is_admin() ){
			return $title;
		}
		return str_replace(array(self::LOCK_ICON, self::UNLOCK_ICON), array('<i class="fa fa-lock"></i> ', '<i class="fa fa-unlock"></i> '), $title);
	}

	function document_title_parts( $title_parts ){
		if( is_admin() ){
			return $title_parts;
		}

		foreach ( $title_parts as $key => $value) {
			$title_parts[ $key ] = str_replace(array(self::LOCK_ICON, self::UNLOCK_ICON), '', $value);
		}

		return $title_parts;
	}

	function wpseo_title( $title ){
		if( is_admin() ){
			return $title;
		}

		$title = str_replace(array(self::LOCK_ICON, self::UNLOCK_ICON), '', $title);

		return $title;
	}
	
	/*************************************************
			LOCKS ON CONTENTS / TITLES - END
	*************************************************/

	/*************************************************
			UTILS - START
	*************************************************/
	
	private function debug( $param, $die = false ){
		if( ! WP_DEBUG ){
			return;
		}

		echo "<pre>";
		var_dump( $param );
		echo "</pre>";

		if( $die ){
			die("DEBUG END");
		}
	}

	private function session(){
		return WP_Session::get_instance();
	}

	private function session_get( $param ){
		$session = $this->session();

		if( isset( $session[ $param ] ) && is_object( $session[ $param ] ) && "Recursive_ArrayAccess" == get_class( $session[ $param ] ) ){
			return $session[ $param ]->toArray();
		}

		if( isset( $session[ $param ] ) ){
			return $session[ $param ];
		}
	}

	private function session_unset( $param ){
		$session = $this->session();

		if( isset( $session[ $param ] ) ){
			unset( $session[ $param ] );
		}
	}

	private function reCaptcha(){
		return EusprSpanExtra_GoogleRecaptcha::getInstance();
	}
	
	/*************************************************
			UTILS - END
	*************************************************/
}