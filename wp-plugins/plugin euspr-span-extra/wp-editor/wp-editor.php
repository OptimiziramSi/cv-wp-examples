<?php

class EusprSpanExtra_WpEditor {


	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() ){
			return;
		}

		add_action('admin_head', array( $this, 'admin_head') );
		add_action( 'admin_enqueue_scripts', array($this , 'admin_enqueue_scripts' ) );

		/*
		Here will be custom functionality for WP EDITOR
		http://solislab.com/blog/how-to-make-shortcodes-user-friendly/
		http://wordpress.stackexchange.com/questions/63222/how-do-i-preview-the-result-of-a-shortcode-in-the-tinymce-editor
		http://stackoverflow.com/questions/13687604/wordpress-shortcode-preview-in-tinymce
		 */
	}

	function admin_head() {
		// check user permissions
		if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
			return;
		}
		
		// check if WYSIWYG is enabled
		if ( 'true' == get_user_option( 'rich_editing' ) ) {
			add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_before_init' ), 99 );
			add_filter( 'mce_external_plugins', array( $this ,'mce_external_plugins' ), 99 );
			add_filter( 'mce_buttons', array( $this, 'mce_buttons' ), 99 );
			add_filter( 'mce_css', array( $this, 'mce_css' ), 99 );
		}
	}

	function tiny_mce_before_init( $init ){
		// $init['plugins'] = ",noneditable";
		$init['noneditable_noneditable_class'] = "ese_wpe";

		return $init;
	}

	function mce_external_plugins( $plugin_array ) {
		$plugins_atts = apply_filters( 'ese_wpe_mce_external_plugins_atts', array() );
		?>
		<script>
			var ese_wpe = <?php echo json_encode( $plugins_atts ); ?>;
		</script>
		<?php

		$plugin_array['noneditable'] = plugins_url( 'tinymce_plugins/noneditable/plugin.min.js' , __FILE__ );

		$plugin_array = apply_filters( 'ese_wpe_mce_external_plugins', $plugin_array );
		return $plugin_array;
	}

	function mce_buttons( $buttons ) {
		$buttons = apply_filters( 'ese_wpe_mce_buttons', $buttons );
		return $buttons;
	}

	function admin_enqueue_scripts(){
		do_action('ese_wpe_admin_enqueue_scripts');
	}

	function mce_css( $mce_css ){
		if ( ! empty( $mce_css ) )
			$mce_css .= ',';

		$mce_css .= plugins_url( 'libs/font-awesome/font-awesome.css', dirname( __FILE__ ) );


		$mce_css = apply_filters( 'ese_wpe_mce_css', $mce_css );

		return $mce_css;
	}



}