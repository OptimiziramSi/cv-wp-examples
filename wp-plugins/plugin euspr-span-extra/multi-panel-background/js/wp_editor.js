(function( $ ) {
	var shortcode_tag = "ese_multi_panel_background";
	var settings = ese_wpe[ shortcode_tag ] ? ese_wpe[ shortcode_tag ] : {};
	// $(document),on('click', 'div.ese_wpe.'+shortcode_tag, )

	tinymce.PluginManager.add(shortcode_tag, function( editor, url ) {

		function replaceShortcodes( content ) {
			return wp.shortcode.replace( shortcode_tag, content, processShortcode );
		}

		function processShortcode( shortcode ){
			var attrs = {
				class: "ese_wpe " + shortcode_tag + " panel panel-multi panel-bg",
			};
			var shortcode_atts = settings.shortcode_attributes;
			$.each( shortcode_atts, function(){
				i = this;
				attrs['data-sh-attr-' + i] = shortcode.attrs.named[ i ] ? shortcode.attrs.named[ i ] : "";
			});

			/*
			<div class="panel panel-multi panel-bg">
				<h4><a href="" class="a-link">2015 Conference</a></h4>
				<a href="#" class="button link float-left">Speaker presentations (Plenary and Parallel)</a>
				<a href="#" class="button link float-left">Speaker presentations (Plenary and Parallel)</a>
				<a href="#" class="button link float-left">Speaker presentations (Plenary and Parallel)</a>
			</div>
			*/

			var $content = $('<div/>');
			$content.append( $('<div/>').addClass('hover').append( $('<span/>').addClass('instructions').text( settings.double_click_to_edit ) ) );
			$content.append( $('<h4/>').append( $('<span/>').addClass('a').text( attrs['data-sh-attr-title'] ) ) );
			for (var i = 1; i <= 6; i++) {
				var url = attrs['data-sh-attr-link_' + i + '_url'];
				var text = attrs['data-sh-attr-link_' + i + '_title'];

				if( "" != url && "" != title ){
					$content.append( $('<span/>').addClass('a button link float-left').text( text ) );
				}
			};

			// Set up the codeblock with the parsed shortcode attrs.
			return wp.html.string({
				tag: "div",
				attrs: attrs,
				content: $content.html(),
			});
		}

		function restoreShortcodes( content ) {
			var $holder = $('<div/>');
			$holder.html( content );
			$holder.find("div.ese_wpe."+shortcode_tag).each(function( i, e ){
				var $e = $(e);

				var shortcode_text = "["+shortcode_tag;

				$.each(e.attributes, function() {
					// this.attributes is not a plain object, but an array
					// of attribute nodes, which contain both the name and value
					if(this.specified && 0 === this.name.indexOf('data-sh-attr-')) {
						shortcode_text += ' ' + this.name.substr( ' data-sh-attr-'.length - 1 ) + '="'+this.value+'"';
					}
				});

				shortcode_text += "]";

				var $newContent = $('<p/>');
				$newContent.text( shortcode_text );

				$e.replaceWith( $newContent );
			});

			return $holder.html();
		}

		//add popup
		editor.addCommand(shortcode_tag+'_popup', function(ui, v) {
			var body = [];
			$.each(settings.shortcode_attributes, function(){
				var attr_name = this;
				body.push({
					type: 'textbox',
					name: attr_name,
					label: attr_name,
					value: v[ attr_name ] ? v[ attr_name ] : "",
					tooltip: settings.blank_for_none,
					width: '100px',
				});
			});

			editor.windowManager.open( {
				title: settings.name,
				body: body,
				onsubmit: function( e ) {
					var shortcode_str = '[' + shortcode_tag;
					$.each(settings.shortcode_attributes, function(){
						var attr_name = this;
						var attr_value = typeof e.data[ attr_name ] != 'undefined' && e.data[ attr_name ].length ? e.data[ attr_name ] : "";
						shortcode_str += ' '+attr_name+'="' + attr_value + '"';
					});
					//add panel content
					shortcode_str += ']';
					//insert shortcode to tinymce
					editor.selection.setContent( shortcode_str );
					// editor.insertContent( shortcode_str);
					
					// TODO - zdaj ko dodaš element, na njem ne deluje NOEDITABLE, dokler ne greš na text in nazaj v VIEW
					// morda kak render ali kaj takega
				}
			});
	    });

		//add button
		editor.addButton(shortcode_tag, {
			image: settings.button_image,
			tooltip: settings.button_tooltip,
			onclick: function() {
				editor.execCommand(shortcode_tag+'_popup','',{});
			}
		});

		//replace from shortcode to an image placeholder
		editor.on('BeforeSetcontent', function(event){ 
			event.content = replaceShortcodes( event.content );
		});

		//replace from image placeholder to shortcode
		editor.on('GetContent', function(event){
			event.content = restoreShortcodes(event.content);
		});

		//open popup on placeholder double click
		editor.on('DblClick',function(e) {
			var $e = $(e.target);
			if ( $e.closest('.ese_wpe').length > 0 && $e.closest('.ese_wpe').hasClass(shortcode_tag) ) {
				var $target = $e.closest('.ese_wpe');
				tinymce.activeEditor.selection.select($target.get(0));
				var attrs = {};
				$.each(settings.shortcode_attributes, function(){
					var attr_name = this;
					attrs[ attr_name ] = $target.attr('data-sh-attr-'+attr_name) ? $target.attr('data-sh-attr-'+attr_name) : "";
				});
				editor.execCommand(shortcode_tag+'_popup','',attrs);
			}
		});
	});
})( jQuery );