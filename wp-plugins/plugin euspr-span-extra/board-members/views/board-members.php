<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

foreach ( $members as $members_title => $members_posts ) {
	?>
	<div class="members-group">

		<?php foreach ( $members_posts as $members_post ) { ?>

			<div class="board-member-holder">
			
				<div class="board-wrapper">
					<h3 class="title"><?php echo esc_html( $members_title ); ?></h3>

					<?php if( has_post_thumbnail( $members_post->ID ) ){
						$member_img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $members_post->ID ), 'ese-board-member' );
						?>
						<figure class="member"><img width="100" src="<?php echo esc_url( $member_img_src[0] ); ?>" alt="<?php echo esc_attr( $members_post->post_title ); ?>"></figure>
						<?php
					} ?>

					<h4><?php echo esc_html( $members_post->post_title ); ?></h4>

					<?php if( ! empty( $members_post->facebook ) || ! empty( $members_post->linkedin ) || ! empty( $members_post->twitter ) ){ ?>
						<div class="social-holder">
							<?php if ( ! empty( $members_post->facebook ) ) { ?>
								<a href="<?php echo esc_url( $members_post->facebook ); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a>
							<?php } ?>
							<?php if (! empty( $members_post->twitter )) {?>
								<a href="<?php echo esc_url( $members_post->twitter ); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a>
							<?php }?>
							<?php if (! empty( $members_post->linkedin )) {?>
								<a href="<?php echo esc_url( $members_post->linkedin ); ?>" target="_blank"><i class="fa fa-linkedin-square"></i></a>
							<?php }?>
						</div>
						<div class="clearfix"></div>
					<?php }?>

					<p class="title"><?php echo esc_html( $members_post->position ); ?></p>

					<div class="desc">
						<?php echo apply_filters( 'the_content', $members_post->post_content ); ?>
					</div>
					<div class="clearfix"></div>

				</div>
			</div>

		<?php } ?>
	</div>
	<?php
}


?>