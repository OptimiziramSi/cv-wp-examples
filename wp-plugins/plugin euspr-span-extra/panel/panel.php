<?php

/**
 * Ads panel option to WP EDITOR
 * it wraps content into div with panel class
 */

class EusprSpanExtra_Panel {

	private static $instance = null;

	public $shortcode_tag = 'ese_panel';

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() ){
			return;
		}

		// enable WP EDITOR functionality
		add_filter('ese_wpe_mce_external_plugins_atts', array( $this, 'ese_wpe_mce_external_plugins_atts' ) );
		add_filter('ese_wpe_mce_external_plugins', array( $this, 'ese_wpe_mce_external_plugins' ) );
		add_filter('ese_wpe_mce_buttons', array( $this, 'ese_wpe_mce_buttons' ) );
		add_action('ese_wpe_admin_enqueue_scripts', array( $this, 'ese_wpe_admin_enqueue_scripts' ) );
		add_filter('ese_wpe_mce_css', array( $this, 'ese_wpe_mce_css' ) );
	}

	/*************************************************
			WP EDITOR - START
	*************************************************/
	
	function ese_wpe_mce_external_plugins_atts( $plugins_atts ){
		$plugins_atts[$this->shortcode_tag ] = array(
			'name' => __( 'Panel', 'ese' ),
			'button_tooltip' => __( 'Show as panel', 'ese' ),
			'button_image' => plugins_url( 'images/wp_editor_btn.png' , __FILE__ ),
		);
		return $plugins_atts;
	}
	function ese_wpe_mce_external_plugins( $plugin_array ){
		$plugin_array[$this->shortcode_tag ] = plugins_url( 'js/wp_editor.js' , __FILE__ );
		return $plugin_array;
	}

	function ese_wpe_mce_buttons( $buttons ){
		array_push( $buttons, $this->shortcode_tag );
		return $buttons;
	}

	function ese_wpe_admin_enqueue_scripts(){
		// wp_enqueue_style($this->shortcode_tag, plugins_url( 'css/wp_editor.css' , __FILE__ ) );
	}

	function ese_wpe_mce_css( $mce_css ) {
		if ( ! empty( $mce_css ) )
			$mce_css .= ',';

		$mce_css .= plugins_url( 'css/wp_editor.css', __FILE__ );
		return $mce_css;
	}
	
	/*************************************************
			WP EDITOR - END
	*************************************************/





}