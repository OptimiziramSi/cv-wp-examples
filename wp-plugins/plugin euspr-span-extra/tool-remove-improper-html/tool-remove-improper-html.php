<?php

class EusprSpanExtra_TollRemoveImproperHtml {

	const OPTION_REPLACED = "ese_trih_replaced";

	private $slug = "ese-remove-improper-html";

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	private function __construct(){

		if( ! is_admin() || ! current_user_can( 'manage_options' ) ){
			return;
		}

		// create submenu page
		add_action( 'admin_menu', array( $this, 'addSubmenuToToolsMenu' ) );

	}

	function addSubmenuToToolsMenu(){
		// create submenu under TOOLS menu
		add_submenu_page(
			'tools.php',
			__( 'Remove improper html', 'ese' ),
			__( 'Remove improper html', 'ese' ),
			'manage_options',
			$this->slug,
			array( $this, 'renderPage' )
		);
	}

	function renderPage(){
		$data = $this->processRequest();

		include dirname( __FILE__ ) . '/views/tool.php';
	}

	private function processRequest(){
		$data = array(
			'state' => '',
			'errors' => array(),
			'messages' => array(),
		);

		$data['state'] = '';

		// DEVELOP
		// $data['state'] = 'scan_report';
		// $data['scan_report'] = $this->scanContents( $this->scanContentsQuery() );
		// return $data;


		if( isset( $_POST['action'] ) && $this->slug."-scan" == $_POST['action'] ){

			$data['state'] = 'scan_report';

			if( ! isset( $_POST[$this->slug.'-scan-nonce'] ) || ! wp_verify_nonce( $_POST[$this->slug.'-scan-nonce'], $this->slug.'-scan' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', 'ese' );
				$data['state'] = '';

				return $data;
			}

			$data['scan_report'] = $this->scanContents( $this->scanContentsQuery() );

			return $data;
		}




		if( isset( $_POST['action'] ) && $this->slug."-process" == $_POST['action'] ){

			$data['state'] = 'process_report';

			if( ! isset( $_POST[$this->slug.'-process-nonce'] ) || ! wp_verify_nonce( $_POST[$this->slug.'-process-nonce'], $this->slug.'-process' ) ){
				$data['errors'][] = __( 'Request not ok. Please retry using this tool the way it is ment to be used.', 'ese' );
				$data['state'] = '';

				return $data;
			}

			$data['process_report'] = $this->processContents( $this->scanContentsQuery() );
			
			return $data;
		}

		$data['replaced'] = $this->get_replaced();

		return $data;
	}


	private function scanContents( $query ){

		$contents = array();

		foreach ( $query->posts as $post ) {
			$improper_content = $this->improperContentExists( $post->post_content );

			// if we are sure that there is no bad html, we skip it
			if( false === $improper_content ){
				continue;
			}

			$new_content = $this->removeImproperHtml( $post->post_content );

			$contents[] = array(
				'title' => strtoupper($post->post_type) . ':' . esc_html( $post->post_title ),
				'view_url' => get_the_permalink( $post->ID ),
				'edit_url' => add_query_arg(array('post' => $post->ID), '/wp-admin/post.php?post=6602&action=edit'),
				'old_content' => $post->post_content,
				'new_content' => $new_content,
			);
		}

		usort( $contents, array( $this, 'sort_null_contents_first') );

		return $contents;
	}

	private function sort_null_contents_first( $content_1, $content_2 ){

		if( is_null( $content_1['new_content'] ) && ! is_null( $content_2['new_content'] ) ){
			return -1;
		}

		if( ! is_null( $content_1['new_content'] ) && is_null( $content_2['new_content'] ) ){
			return 1;
		}

		return 0;
	}


	private function processContents( $query ){
		$contents = array();
		$replaced = $this->get_replaced();

		foreach ( $query->posts as $post ) {
			$improper_content = $this->improperContentExists( $post->post_content );

			// if we are not sure, that there is some bad html, we skip it
			if( true !== $improper_content ){
				continue;
			}

			$new_content = $this->removeImproperHtml( $post->post_content );

			$contents[] = array(
				'title' => strtoupper($post->post_type) . ':' . esc_html( $post->post_title ),
				'view_url' => get_the_permalink( $post->ID ),
				'edit_url' => add_query_arg(array('post' => $post->ID), '/wp-admin/post.php?post=6602&action=edit'),
				'old_content' => $post->post_content,
				'new_content' => $new_content,
			);

			wp_update_post( array(
				'ID' => $post->ID,
				'post_content' => $new_content,
			) );

			if( ! isset( $replaced[ $post->ID ] ) ){
				$replaced[ $post->ID ] = array();
			}

			$replaced[ $post->ID ][] = date('Y-m-d H:i:s');
		}

		$this->set_replaced( $replaced );


		return $contents;
	}

	private function removeImproperHtml( $content, $check_only = false ){

		$found = false;

		if( "" == trim( $content ) ){
			if( $check_only ){
				// no bad content here
				return false;
			} else {
				// we don't need to remove anything
				return "";
			}
		}

		$dom = new DOMDocument();
		$html_loaded = @$dom->loadHTML( '<?xml encoding="UTF-8">' . $content );
		if( ! $html_loaded ){
			return null;
		}

		foreach ( $dom->getElementsByTagName('div') as $div ) {

			$div_style = $div->getAttribute('style');
			// if div contains POSITION ABSOLUTE, we take it as hacked content
			if( false !== strpos($div_style, 'absolute') ){

				if( $check_only ){
					return true;
				}

				$found = true;
				$div->parentNode->removeChild( $div );
			}
		}

		if( $check_only ){
			return false;
		}

		if( $found ){
			$mock = new DOMDocument;
			$body = $dom->getElementsByTagName('body')->item(0);
			foreach ($body->childNodes as $child){
				$mock->appendChild($mock->importNode($child, true));
			}

			$content = $mock->saveHTML();
			$content = html_entity_decode( $content );
		}

		return $content;
	}

	private function improperContentExists( $content ){
		return $this->removeImproperHtml( $content, true );
	}





	private function scanContentsQuery(){
		$query_args = array(
			'nopaging' => true,
			'post_type' => array('post','page'),
			'post_status' => 'any',
		);

		$query = new WP_Query( $query_args );

		return $query;
	}

	private function get_replaced(){
		$replaced = get_option( self::OPTION_REPLACED );
		if( ! is_array( $replaced )){
			$replaced = array();
		}
		return $replaced;
	}

	private function set_replaced( $replaced ){
		update_option( self::OPTION_REPLACED, $replaced );
	}

}