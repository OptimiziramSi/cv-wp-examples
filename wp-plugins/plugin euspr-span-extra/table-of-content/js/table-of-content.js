(function($){

	var $post = $("form#post");

	function init(){
		$post.submit( check_table_of_content_before_submit );
		hide_toc_data();
	}

	function hide_toc_data(){
		$post.find("textarea#ese-table-of-content_data").closest('tr').hide();
	}

    function check_table_of_content_before_submit(){

    	var enable_toc = $post.find("input#ese-table-of-content_enable").is(':checked');

    	var tMCE_content = get_tinymce_content();

    	var $tMCE_content_tmp = $("<div />").html( tMCE_content );

    	$tMCE_content_tmp.find( "h1,h2,h3,h4,h5,h6" ).removeClass('scrollspy').removeAttr('id');

    	var toc_list = {};

    	if( enable_toc ){
    		var heading_selector = $post.find("select[name='ese-table-of-content_headers']").val();
    		$tMCE_content_tmp.find( heading_selector ).removeClass('scrollspy').removeAttr('id').each(function(i,e){
    			var $heading_tag = $(e);
    			var heading_id = slugify( $heading_tag.text() );
    			var use_heading_id = heading_id;

    			var heading_id_postfix = 1;
    			while( toc_list.hasOwnProperty( use_heading_id ) ){
    				heading_id_postfix++;
    				use_heading_id = heading_id + "-" + heading_id_postfix;
    			}

    			$heading_tag.attr('id', use_heading_id).addClass('scrollspy');

    			toc_list[ use_heading_id ] = {
    				'tag': $heading_tag.prop("tagName").toLowerCase(),
    				'text': $heading_tag.text()
    			};
    		});
    	}

    	tMCE_content = $tMCE_content_tmp.html();

    	set_tinymce_content( tMCE_content );

    	$post.find("textarea#ese-table-of-content_data").val( JSON.stringify( toc_list ) );

    	return true;
    }

    function get_tinymce_content() {
	    if ( $post.find("#wp-content-wrap").hasClass("tmce-active")){
	        return tinyMCE.activeEditor.getContent();
	    }else{
	        return $post.find('textarea#content').val();
	    }
	}

	function set_tinymce_content(content) {
	    if ( $post.find("#wp-content-wrap").hasClass("tmce-active")){
	        tinyMCE.activeEditor.setContent( content );
	    }else{
	        $post.find('textarea#content').val( content );
	    }
	}

	function slugify(text){
	  return text.toString().toLowerCase()
	    .replace(/\s+/g, '-')           // Replace spaces with -
	    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
	    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
	    .replace(/^-+/, '')             // Trim - from start of text
	    .replace(/-+$/, '');            // Trim - from end of text
	}


    init();

})(jQuery);