<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

// helpers
$v = OpSi_ProductCheckout_View::getInstance();

?>
Hej <?php echo $v->old('name'); ?>!<br><br>

Tvoje naročilo smo uspešno prejeli. Hvala!<br><br>

Tvoja številka naročila: #<?php echo $order->get_order_number(); ?><br><br>

POVZETEK NAROČILA #<?php echo $order->get_order_number(); ?><br>
<?php $sum = 0.0; ?>
<?php foreach( $order->get_items() as $item ): ?>
    <?php echo $item['qty'] ?>x <?php echo $item['name'] ?> - <?php echo $v->formated_price( $item['line_total'] + $item['line_tax'] ); ?><br>
<?php endforeach; ?>
<?php foreach( $order->get_fees() as $item): ?>
    <?php echo $item['qty'] ?>x <?php echo $item['name'] ?> - <?php echo $v->formated_price( $item['line_total'] + + $item['line_tax'] ); ?><br>
<?php endforeach; ?>

<?php if( 'paypal' == $v->old('payment_option') ){ ?>
	<strong>SKUPAJ PLAČANO PREKO PAYPAL: <?php echo $v->formated_price( $order->get_total() ); ?></strong><br><br>
<?php } elseif( 'paymill' == $v->old('payment_option') ){ ?>
	<strong>SKUPAJ PLAČANO PREKO KREDITNE KARTICE: <?php echo $v->formated_price( $order->get_total() ); ?></strong><br><br>
<?php } else { ?>
	<strong>SKUPAJ ZA PLAČILO: <?php echo $v->formated_price( $order->get_total() ); ?></strong><br><br>
<?php } ?>

Podatki za račun: <?php echo $v->old('name'); ?>, <?php echo $v->old('street'); ?>, <?php echo $v->old('postcode'); ?> <?php echo $v->old('city'); ?><br>
Naslov za dostavo: <?php echo $v->old('name'); ?>, <?php echo $v->old('street'); ?>, <?php echo $v->old('postcode'); ?> <?php echo $v->old('city'); ?><br>
Dostava in plačilo: Pošta Slovenije - hitra dostava 1-2 dni, po povzetju<br><br>

Hitra pošta večino paketov dostavi že naslednji delovni dan. <br><br>

Dopoldan pričakuj klic kurirja, da se dogovorite o uri dostave.<br><br>

Hvala za tvoje zaupanje!<br><br>

Lep pozdrav,<br>
Miha Geršič in ekipa PopolnaPostava.com<br>
<a href='http://www.popolnapostava.com/'>http://www.popolnapostava.com/</a>