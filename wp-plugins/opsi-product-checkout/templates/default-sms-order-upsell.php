<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

// helppers
$v = OpSi_ProductCheckout_View::getInstance();

?>

<?php echo $v->old('name'); ?>, tvoje naročilo smo uspešno posodobili. Vse dodatne informacije o tvojem naročilu boš prejel na svoj email naslov: <?php echo $v->old('email'); ?> Hvala za tvoje zaupanje. Lep pozdrav, Miha Geršič in ekipa PopolnaPostava.com