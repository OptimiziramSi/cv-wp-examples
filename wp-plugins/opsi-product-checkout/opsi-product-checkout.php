<?php
/**
* Plugin Name: Product Page with Checkout and Upsell
* Description: Product page with checkout, custom redirect to page with upsell product option
* Plugin URI: http://optimiziram.si
* Author: Matevž Novak - Optimiziram.Si
* Author URI: http://optimiziram.si
* Version: 1.0
* License: GPL2
* Text Domain: opsi-pc
* Domain Path: languages
*/

if ( ! defined( 'ABSPATH' ) ) exit;

define('OPSI_PRODUCTCHECKOUT_VERSION', '1.0');
define('OPSI_PRODUCTCHECKOUT_FILE', __FILE__);
define('OPSI_PRODUCTCHECKOUT_PATH', dirname(OPSI_PRODUCTCHECKOUT_FILE));

require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/metabox.php');
require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/options.php');
require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/upsell.php');
require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/view.php');
require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/shortcode.php');
require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/woocommerce-pip.php');
require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/affiliate.php');
require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/mail.php');
require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/sms.php');
require_once(OPSI_PRODUCTCHECKOUT_PATH . '/includes/handler.php');

add_action( 'plugins_loaded', array( 'OpSi_ProductCheckout', 'getInstance' ) );

class OpSi_ProductCheckout {
	
	/*************************************************
			PLUGIN INSTANCE - START
	*************************************************/
	
	private static $instance = null;
	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {
		if(!$this->is_plugin_active('woocommerce/woocommerce.php')){
			return;
		}

		$this->load_textdomain();
		
		OpSi_ProductCheckout_Options::getInstance();
		OpSi_ProductCheckout_Metabox::getInstance();
		OpSi_ProductCheckout_Upsell::getInstance();
		OpSi_ProductCheckout_View::getInstance();
		OpSi_ProductCheckout_Shortcode::getInstance();
		OpSi_ProductCheckout_Affiliate::getInstance();
		OpSi_ProductCheckout_WoocommercePip::getInstance();
		OpSi_ProductCheckout_Mail::getInstance();
		OpSi_ProductCheckout_Sms::getInstance();
		
		OpSi_ProductCheckout_Handler::getInstance();

	}
	
	/*************************************************
			PLUGIN INSTANCE - END
	*************************************************/

	public function load_textdomain(){
		load_plugin_textdomain( 'opsi_pc', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
	}

	/*************************************************
			UTILS - START
	*************************************************/

	private function is_plugin_active($plugin_name){
		if(!function_exists('is_plugin_active')){
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		return is_plugin_active($plugin_name);
	}
	
	/*************************************************
			UTILS - END
	*************************************************/
}
