<?php

class PaypalHelper {

	// singleton
	private static $instance;
	public static function getInstance(){
		if( ! isset( self::$instance ) ){
			self::$instance = new self();
		}
		return self::$instance;
	}


	private $api_url;
	private $paypal_url;
	private $username;
	private $password;
	private $signature;
	private $version;

	protected $errorReportingEnabled = true;
	protected $errors = array(); //Here you can find errors for your last API call 
	protected $lastServerResponse; //Here you can find PayPal response for your last successfull API call
	protected $curl;

	private function __construct(){
		// helpers
		$o = OpSi_ProductCheckout_Options::getInstance();

		// get paypal config
		$paypal_config = $o->get_paypal_config();

		$this->api_url = $paypal_config['api_url'];
		$this->paypal_url = $paypal_config['url'];
		$this->version = $paypal_config['version'];

		$this->username = $paypal_config['username'];
		$this->password = $paypal_config['password'];
		$this->signature = $paypal_config['signature'];
	}


	/**
	 * The SetExpressCheckout API operation initiates an Express Checkout transaction. Look here to see method parameters: https://developer.paypal.com/docs/classic/api/merchant/SetExpressCheckout_API_Operation_NVP/
	 * @param array $request Array should contain key value pairs defined by PayPal
	 * @return array Response from the PayPal saved in the array and returned from the function
	 */
	public function SetExpressCheckout($request) {
		return $this->sendRequest($request, "SetExpressCheckout");
	}
	/**
	 * The DoExpressCheckoutPayment API operation completes an Express Checkout transaction. If you set up a billing agreement in your SetExpressCheckout API call, the billing agreement is created when you call the DoExpressCheckoutPayment API operation.The DoExpressCheckoutPayment API operation completes an Express Checkout transaction. If you set up a billing agreement in your SetExpressCheckout API call, the billing agreement is created when you call the DoExpressCheckoutPayment API operation. Look here to see method parameters: https://developer.paypal.com/docs/classic/api/merchant/DoExpressCheckoutPayment_API_Operation_NVP/
	 * @param array $request Array should contain key value pairs defined by PayPal
	 * @return array Response from the PayPal saved in the array and returned from the function
	 */
	public function DoExpressCheckoutPayment($request) {
		return $this->sendRequest($request, "DoExpressCheckoutPayment");
	}
	/**
	 * Calles GetExpressCheckoutDetails PayPal API method. This method gets buyer and transaction data. This method WILL NOT- make transaction itself. Look here for method parameters: https://developer.paypal.com/docs/classic/api/merchant/GetExpressCheckoutDetails_API_Operation_NVP/
	 * @param array $request Array should contain key value pairs defined by PayPal
	 * @return array Response from the PayPal saved in the array and returned from the function
	 */
	public function GetExpressCheckoutDetails($request) {
		return $this->sendRequest($request, "GetExpressCheckoutDetails");
	}



	/**
	 * This method makes calls PayPal method provided as argument.
	 * @param array $requestData
	 * @param string $method
	 * @return array 
	 */
	private function sendRequest($requestData, $method) {
		if (!isset($method)) {
			array_push($this->errors, "Method name can not be empty");
		}
		if (!isset($requestData)) {
			array_push($this->errors, "Request data can not be empty");
		}
		if ($this->checkForErrors()) {//If there are errors, STOP
			if ($this->errorReportingEnabled())//If error reporting is enabled, show errors
				$this->showErrors();
			$this->lastServerResponse = null;
			return false; //Do not send a request
		}
		$requestParameters = array(
			"USER" => $this->username,
			"PWD" => $this->password,
			"SIGNATURE" => $this->signature,
			"METHOD" => $method,
			"VERSION" => $this->version,
		);
		$requestParameters += $requestData;
		$finalRequest = http_build_query( $requestParameters );
		$ch = curl_init();
		$this->curl = $ch;
		
		$curlOptions = $this->getcURLOptions();
		$curlOptions[ CURLOPT_POSTFIELDS ] = $finalRequest;
		
		curl_setopt_array( $ch, $curlOptions );
		$serverResponse = curl_exec( $ch );
		if ( curl_errno( $ch ) ) {
			$this->errors = curl_error( $ch );
			curl_close( $ch );
			if ( $this->errorReportingEnabled ) {
				$this->showErrors();
			}
			$this->lastServerResponse = null;
			return false;
		} else {
			curl_close( $ch );
			$result = array();
			parse_str( $serverResponse, $result );
			$this->lastServerResponse = $result;
			return $this->lastServerResponse;
		}
	}
	/**
	 * Returns latest result from the PayPal servers
	 * @return array
	 */
	public function getLastServerResponse() {
		return $this->lastServerResponse;
	}
	/** 
	 * Call this function if you want to retreave errors occured during last API call
	 * @return void Prints all errors during last API call.
	 */
	public function showErrors() {
		echo "<pre>";
		var_dump( $this->errors );
		echo "</pre>";
	}



	/**
	 * 
	 * @return boolean Checks if there are errors and returns the true/false
	 */
	private function checkForErrors() {
		
		if( ! is_array( $this->errors ) && "" != $this->errors ){
			return true;
		}
		
		if ( count( $this->errors ) > 0 ) {
			return true;
		}
		return false;
	}
	/**
	 * Returns an array of options to initialize cURL
	 * @return array
	 */
	private function getcURLOptions() {
		return array(
			CURLOPT_URL => $this->api_url,
			CURLOPT_VERBOSE => 1,
			//Have a look at this: http://stackoverflow.com/questions/14951802/paypal-ipn-unable-to-get-local-issuer-certificate
			//You can download a fresh cURL pem file from here http://curl.haxx.se/ca/cacert.pem
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_SSL_VERIFYHOST => 2,
			CURLOPT_CAINFO => OPSI_PRODUCTCHECKOUT_PATH . '/libs/paypal-cert/cacert.pem', //CA cert file
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_POST => 1,
		);
	}
	
	/**
	 * If you want to set cURL with additional parameters, use this function. NOTE: Call this function prior sendRequest method
	 * @param int $option
	 * @param mixed $value
	 */
	public function setCURLOption($option, $value){
		curl_setopt($this->curl, $option, $value);
	}



	public function log($message,$success,$end=false){
		// Timestamp
		$text = '['.date('m/d/Y g:i A').'] - '.(($success)?'SUCCESS :':'FAILURE :').$message. "\n";

		if ($end) {
			$text .= "\n------------------------------------------------------------------\n\n";
		}

		// Write to log
		$fp=fopen( dirname( __FILE__ ) . '/debug.log','a');
		fwrite($fp, $text );
		fclose($fp);  // close file
	}

	public function redirect_to_paypal_url( $token ){
		return $paypal_url = add_query_arg( array( 'cmd' => '_express-checkout', 'token' => $token ), $this->paypal_url);
	}

	public function redirectToPayPal( $token ){

		wp_redirect( $this->redirect_to_paypal_url( $token ) );
		exit();
	}


}