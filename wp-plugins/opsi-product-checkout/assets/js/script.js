(function($){

	var $form;
	var $payment_option_inputs;

	function init(){

		$payment_option_inputs = $("input[name='" + opsi_pc.payment_option_input_name + "']");
		$payment_option_inputs.change( on_payment_option_change );
		on_payment_option_change();

		$("input[name='"+opsi_pc.post_numbers_input_name+"']").change( set_post_from_postcode );

		$form = $payment_option_inputs.closest('form');
		$form.submit( check_and_handle_paymill_token );

		if( opsi_pc.terms_open_as_window ){
			$( "." + opsi_pc.terms_anchor_class ).click( open_terms_page_window );
		}

		if( opsi_pc.upsell_confirm_enabled ){
			$('input[name="'+opsi_pc.upsell_confirm_input+'"]')
				.filter(function(){
					return ( $(this).val() != '-1' );
				})
				.closest('form').submit( upsell_product_confirm );
		}
	}

	function on_payment_option_change(){
		var $selected_payment_option = $payment_option_inputs.filter(':checked').val();
		$(".payment_option_content").hide('fast');
		$(".payment_option_content." + $selected_payment_option).show('fast');
	}

	function check_and_handle_paymill_token(){
		// we only need this for paymill
		if( 'paymill' != $payment_option_inputs.filter(':checked').val() ){
			// payment option not paymill, continue with normal form submit
			return true;
		}

		// if paymill token is already set, submit form
		if( '' != $("input[name='" + opsi_pc.paymill_token_input_name + "']").val() ){
			return true;
		}

		// otherwise request token first
		request_paymill_token();

		// prevent form submit
		return false;
	}

	function request_paymill_token(){
		var data = {
			number: $("#" + opsi_pc.paymill_card_number_input_id).val(),
			exp_month: $("#" + opsi_pc.paymill_card_expiry_month_input_id).val(),
			exp_year: $("#" + opsi_pc.paymill_card_expiry_year_input_id).val(),
			cvc: $("#" + opsi_pc.paymill_card_cvc_input_id).val(),
			cardholder: $("#" + opsi_pc.paymill_card_holder_name_input_id).val(),

			amount_int: opsi_pc.paymill_amount_int,
			currency: opsi_pc.paymill_currency,
		};

		paymill.createToken( data, on_paymill_response_handler );
	}

	function on_paymill_response_handler( error, result ){
		if (error) {
			// Shows the error above the form
			alert( opsi_pc.paymill_error_getting_token + error.apierror );
			// TODO $("#checkout_forma .checkout_submit_narocilo").removeAttr( "disabled" );
		} else {
			// Insert token into form in order to submit to server
			$("input[name='" + opsi_pc.paymill_token_input_name + "']").val( result.token );
			// naredimo submit forme
			$form.submit();
		}
	}

	function set_post_from_postcode(){
		var postcode = $("input[name='"+opsi_pc.post_numbers_input_name+"']").val();

		for (var i = 0; i < opsi_pc.post_numbers.length; i++) {
			if( postcode == opsi_pc.post_numbers[i].post_number) {
				$("input[name='"+opsi_pc.city_input_name+"']").val( opsi_pc.post_numbers[i].city );
				break;
			}
		}
	}

	function open_terms_page_window( event ){
		event.preventDefault();
		window.open( $(this).attr('href'), $(this).attr('title'), 'width=' + opsi_pc.terms_window_width +',height=' + opsi_pc.terms_window_height );
	}

	function upsell_product_confirm(){
		if( confirm( opsi_pc.upsell_confirm_content ) )
			return true;

		return false;
	}

	$(document).ready(init);
})(jQuery);