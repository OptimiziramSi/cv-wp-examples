# razčlenitev po sklopih

## procesiranje
- vse podatke polnimo v temu namenjen objekt,
  da so ti podatki kasneje dosegljivi v pogledih
- naročilnica / primarni produkt
    + preverimo, če je zahtevek sploh za nas
    + preverjanje podatkov v formi
        * nabor napak, če so
    + shramba poslanih podatkov in / ali napak
    + procesiranje naročila (če ni napak)
        * plačilo ob prevzemu
            - 
        * paymill
            - opravimo zahtevek za plačilo
            - preverimo napake
            - če je vse ok, shranimo token / payment id
        * paypall
            - pripravimo povezavo za preusmeritev
            - preumerimo
- paypall callback
    + preverjanje za napake / če jih je paypal vrnil
    + plačilo je bilo uspešno izvedeno
    + izvedemo plačilo
- upsell produkt

## nastavitve
- default pogoji poslovanja url / page id
- paymill vpisni podatki
- paypal vpisni podatki
- vklopi razvoj
    + paymill vpisni podatki
    + paypal vpisni podatki
- seznam pošt / poštnih številk za autocomplete
- prikaz uporabljene valute (za info)
- če so potrebni dodatni parametri za woocommerce
    + pošlji email o naročilu
    + ustvari uporabnika

## data objekt
je objekt, kjer hranimo vse potrebne podatke za izpis-e

## izpis
- post metabox
    + enabled
    + template id / code
    + selling product id
    + upsell product id
    + shipment price
    + enable plačilo ob prevzemu
    + enable paymill payment
    + enable paypall payment
    + prikaz valute in cene izdelka
    + prikaz valute in cene upsell izdelka
    + pogoji poslovanja link
    + redirect to page on checkout
- shortcode za naročilnico
- shortcode za upsell


# NOVE FUNKCIONALNOSTI - dogovorjeno z Matejem, skype, 26.5.2017
## Definicija
- podpora za custom maile
    + mail, ki se pošlje ob oddaji naročila
    + mail, ki se pošlje ob dodajanju upsell produkta
- podpora za sms
    + s trenutnim ponudnikom
    + po enkem principu
- možnost izbire večih uspell produktov
