<?php
/**
* Plugin Name: Export WooCommerce orders for Storžič program
* Description: Export WooCommerce orders for Storžič program
* Plugin URI: http://2gika.si
* Author: 2gika
* Author URI: http://2gika.si
* Version: 1.0
* License: GPL2
* Text Domain: wher
* Domain Path: languages
*/

/*
Copyright (C) 2015  2gika  info@2gika.si

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/**
 * Snippet Wordpress Plugin Boilerplate based on:
 *
 * - https://github.com/purplefish32/sublime-text-2-wordpress/blob/master/Snippets/Plugin_Head.sublime-snippet
 * - http://wordpress.stackexchange.com/questions/25910/uninstall-activate-deactivate-a-plugin-typical-features-how-to/25979#25979
 *
 * By default the option to uninstall the plugin is disabled,
 * to use uncomment or remove if not used.
 *
 * This Template does not have the necessary code for use in multisite. 
 *
 * Also delete this comment block is unnecessary once you have read.
 *
 * Version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// enable development export
// define('WHER_OUTPUT_DOWNLOAD', true);

add_action( 'plugins_loaded', array( 'Wp_Hujsanje_Export_Strozic', 'getInstance' ) );
register_activation_hook( __FILE__, array( 'Wp_Hujsanje_Export_Strozic', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Wp_Hujsanje_Export_Strozic', 'deactivate' ) );

class Wp_Hujsanje_Export_Strozic {

	/** @var array tab IDs / titles */
	private $tabs;

	/** @var string page slug */
	private $page_slug;

	/** @var array storing messages */
	private $messages;

	/** @var array storing errors */
	private $errors;

	/** plugin version number */
	const VERSION = '1.0.0';


	/*************************************************
			INSTANCE - START
	*************************************************/
	
	/** @var class instance */
	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}
	
	/*************************************************
			INSTANCE - END
	*************************************************/




	/*************************************************
			ACITVATE / DEACTIVATE - START
	*************************************************/
	
	public static function activate() {}

	public static function deactivate() {}
	
	/*************************************************
			ACITVATE / DEACTIVATE - END
	*************************************************/
	

	

	/*************************************************
			CONTRUCT CLASS - START
	*************************************************/
	
	private function __construct() {

		if( ! $this->is_woocommerce_active() ){
			return;
		}
		
		// return pip in API call
		add_filter( 'woocommerce_api_order_response', array( $this, 'add_pip_to_api_order' ), 10, 4 );

		// mark order as not exported when created
		add_action( 'wp_insert_post',  array( $this, 'mark_order_not_exported' ), 10, 2 );

		if( ! is_admin() ){
			return;
		}

		$this->tabs = array(
			'export' => __( 'Export', 'wher' ),
			'settings' => __( 'Settings', 'wher' ),
		);

		$this->page_slug = 'wp_hujsanje_export_racunovodstvo';

		$this->messages = array();
		$this->errors = array();

		/** General Admin Hooks */

		// enable session
		add_action( 'admin_init', array( $this, 'register_session' ) );

		// process prepare export submit
		add_action( 'admin_init', array( $this, 'process_prepare_export' ) );
		add_action( 'admin_init', array( $this, 'process_prepare_export_for_order' ) );

		// process download export submit
		add_action( 'admin_init', array( $this, 'process_download_export' ) );

		// Load WC styles / scripts
		add_filter( 'woocommerce_screen_ids', array( $this, 'load_wc_styles_scripts' ) );

		// enqueue scripts
		add_action( 'admin_enqueue_scripts', array( $this, 'load_styles_scripts' ) );

		// register menu
		add_action( 'admin_menu', array( $this, 'add_menu_link') );

		/** Order hooks */

		// Add 'Export Status' orders page column header
		add_filter( 'manage_edit-shop_order_columns', array( $this, 'add_order_status_column_header' ), 20 );

		// Add 'Export Status' orders page column content
		add_action( 'manage_shop_order_posts_custom_column', array( $this, 'add_order_status_column_content' ) );

		// Add 'Export to CSV' action on orders page
		add_action( 'woocommerce_admin_order_actions_end', array( $this, 'add_order_action' ), 10, 2 );

		// Add 'Export to CSV' order meta box order action
		add_action( 'woocommerce_order_actions',       array( $this, 'add_order_meta_box_actions' ) );

		// Process 'Export to CSV' order meta box order action
		add_action( 'woocommerce_order_action_wp_hujsanje_export_racunovodstvo_download', array( $this, 'process_order_meta_box_actions' ) );

		// Add bulk order filter for exported / non-exported orders
		add_action( 'restrict_manage_posts', array( $this, 'filter_orders_by_export_status') , 20 );
		add_filter( 'request',               array( $this, 'filter_orders_by_export_status_query' ) );

		// Add bulk action to download multiple orders to CSV and mark them as exported / not-exported
		add_action( 'admin_footer-edit.php', array( $this, 'add_order_bulk_actions' ) );
		add_action( 'load-edit.php',         array( $this, 'process_order_bulk_actions' ) );
	}
	
	/*************************************************
			CONTRUCT CLASS - END
	*************************************************/


	
	/*************************************************
			ADD PIP TO API ORDER - START
	*************************************************/
	
	public function add_pip_to_api_order( $order_data, $order, $fields, $server ){

		$order_data['pip_invoice_number'] = get_post_meta( $order->id, '_pip_invoice_number', true );

		return $order_data;
	}
	
	/*************************************************
			ADD PIP TO API ORDER - END
	*************************************************/
	
	


	/*************************************************
			SESSION HANDLING - START
	*************************************************/
	
	public function register_session(){
		if( !session_id() ){
			session_start();
		}
	}

	private function set_session_data( $export_id, $data ){
		if( ! isset( $_SESSION['wher'] ) || ! is_array( $_SESSION['wher'] ) ){
			$_SESSION['wher'] = array();
		}

		$_SESSION['wher'][$export_id] = $data;
	}

	private function get_session_data( $export_id ){

		if( isset( $_SESSION['wher'] ) && isset( $_SESSION['wher'][$export_id] ) ){
			return $_SESSION['wher'][$export_id];
		}

		return null;
	}
	
	/*************************************************
			SESSION HANDLING - END
	*************************************************/

	




	/*************************************************
			ADMIN MENU - START
	*************************************************/
	
	private $page;
	public function add_menu_link(){
		$this->page = add_submenu_page(
			'woocommerce',
			__( 'Storžič Export', 'wher' ),
			__( 'Storžič Export', 'wher' ),
			'manage_woocommerce',
			$this->page_slug,
			array( $this, 'render_submenu_pages' )
		);
	}

	public function render_submenu_pages() {

		// permissions check
		if ( ! current_user_can( 'manage_woocommerce' ) ) {
			return;
		}

		$current_tab = ( empty( $_GET[ 'tab' ] ) ) ? 'export' : urldecode( $_GET[ 'tab' ] );

		// process save settings
		if ( ! empty( $_POST ) && 'settings' == $current_tab ) {

			// security check
			if ( ! wp_verify_nonce( $_POST['_wpnonce'], __FILE__ ) ) {

				wp_die( __( 'Action failed. Please refresh the page and retry.', 'wher' ) );
			}
			// save settings
			woocommerce_update_options( $this->get_settings_settings() );

			$this->add_message( __( 'Your settings have been saved.', 'wher' ) );
		}

		?>
		<div class="wrap woocommerce">
		<form method="post" id="mainform" action="" enctype="multipart/form-data">
			<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
				<?php
				foreach ( $this->tabs as $tab_id => $tab_title ) :

					$class = ( $tab_id == $current_tab ) ? array( 'nav-tab', 'nav-tab-active' ) : array( 'nav-tab' );
					$url   = add_query_arg( 'tab', $tab_id, admin_url( 'admin.php?page=' . $this->page_slug ) );

					printf( '<a href="%s" class="%s">%s</a>', esc_url( $url ), implode( ' ', array_map( 'sanitize_html_class', $class ) ), esc_html( $tab_title ) );

				endforeach;
			?> </h2> <?php

		$this->show_messages();

		if ( 'settings' == $current_tab ) {

			$this->render_settings_page();

		} else {

			$this->render_export_page();
		}

		?> </form>
		</div> <?php
	}

	private function render_export_page() {

		// permissions check
		if ( ! current_user_can( 'manage_woocommerce' ) ) {
			return;
		}

		// render download buttons
		if( isset( $_GET['export-id'] ) ){

			$session_data = $this->get_session_data( $_GET['export-id'] );

			if( is_null( $session_data ) ){
				_e( 'Session of export does not exist any more. Please go back in retry.', 'wher' );
				return;
			}

			$download_clients_url = add_query_arg( 'download', 'clients' );
			$download_orders_url = add_query_arg( 'download', 'orders' );
			$download_orders_items_url = add_query_arg( 'download', 'orders-items' );

			$download_clients_url = wp_nonce_url( $download_clients_url, __FILE__ );
			$download_orders_url = wp_nonce_url( $download_orders_url, __FILE__ );
			$download_orders_items_url = wp_nonce_url( $download_orders_items_url, __FILE__ );

			echo isset( $session_data['params']['content'] ) ? $session_data['params']['content'] : '';
			?>
			<p>
				<?php echo sprintf( __( 'Number of orders to export: %s', 'wher' ), count( $session_data['order_ids'] ) ); ?>
			</p>
			<p>
				<a href="<?php echo esc_attr( $download_clients_url ); ?>" target="_blank" class="button button-primary"><?php _e( 'Download clients file', 'wher' ); ?></a>
				<a href="<?php echo esc_attr( $download_orders_url ); ?>" target="_blank" class="button button-primary"><?php _e( 'Download orders file', 'wher' ); ?></a>
				<a href="<?php echo esc_attr( $download_orders_items_url ); ?>" target="_blank" class="button button-primary"><?php _e( 'Download orders items file', 'wher' ); ?></a>
			</p>
			<?php
		}

		// render export form
		else {

			// show export form
			woocommerce_admin_fields( $this->get_export_settings() );

			// helper input
			?><input type="hidden" name="wp_hujsanje_export_racunovodstvo_bulk_export" value="1" /><?php

			wp_nonce_field( __FILE__ );
			submit_button( __( 'Prepare export', 'wher' ) );

		}
	}

	private function render_settings_page(){
		
		// render settings fields
		woocommerce_admin_fields( $this->get_settings_settings() );

		wp_nonce_field( __FILE__ );
		submit_button( __( 'Save settings', 'wher' ) );

	}
	
	/*************************************************
			ADMIN MENU - END
	*************************************************/



	/*************************************************
			ORDER SPECIFIC THINGS - START
	*************************************************/
	
	public function mark_order_not_exported( $post_id, $post ) {

		if ( 'shop_order' == $post->post_type ) {

			// force unique, because oddly this can be invoked when changing the status of an existing order
			add_post_meta( $post_id, '_wp_hujsanje_export_racunovodstvo_is_exported', 0, true );
		}
	}
	
	/*************************************************
			ORDER SPECIFIC THINGS - END
	*************************************************/




	/*************************************************
			PROCESS PREPARE EXPORT - START
	*************************************************/
	
	public function process_prepare_export_for_order(){

		if( ! isset( $_GET['page'] ) || ! isset( $_GET['create-order-export'] ) ){
			return;
		}

		$nonce = isset( $_GET['_wpnonce'] ) ? $_GET['_wpnonce'] : '';
		if ( ! wp_verify_nonce( $nonce, 'wp_hujsanje_export_racunovodstvo_export_order' ) ) {

			wp_die( __( 'Action failed. Please refresh the page and retry.', 'wher' ) );
		}

		$order_ids = array( (int)$_GET['create-order-export'] );

		ob_start();
		// TODO - make text dinamic and with more information for user
		?>
		<h2><?php __( 'Single order export', 'wher' ); ?></h2>
		<?php
		$content = ob_get_clean();

		// TODO - set export params for later use
		$export_params = array(
			'export_type' => 'storzic',
			'content' => $content,
		);

		$export_id = $this->create_export_session( $order_ids, $export_params );
		$this->redirect_to_export_download_page( $export_id );

	}

	public function process_prepare_export() {

		if ( ! isset( $_POST['wp_hujsanje_export_racunovodstvo_bulk_export'] ) || ! isset( $_POST['wp_hujsanje_export_racunovodstvo_type'] ) ) {
			return;
		}

		// security check
		if ( ! wp_verify_nonce( $_POST['_wpnonce'], __FILE__ ) ) {

			wp_die( __( 'Action failed. Please refresh the page and retry.', 'wher' ) );
		}

		$export_type = $_POST['wp_hujsanje_export_racunovodstvo_type'];

		$query_args = array(
			'fields'         => 'ids',
			'post_type'      => 'shop_order',
			'post_status'    => ( ! empty( $_POST['wp_hujsanje_export_racunovodstvo_statuses'] ) && 'storzic' == $export_type ) ? $_POST['wp_hujsanje_export_racunovodstvo_statuses'] : 'any',
			'posts_per_page' => empty( $_POST['wp_hujsanje_export_racunovodstvo_limit'] )  ? -1 : absint( $_POST['wp_hujsanje_export_racunovodstvo_limit'] ),
			'offset'         => empty( $_POST['wp_hujsanje_export_racunovodstvo_offset'] ) ? 0  : absint( $_POST['wp_hujsanje_export_racunovodstvo_offset'] ),
			'date_query'  => array(
				array(
					'before'    => empty( $_POST['wp_hujsanje_export_racunovodstvo_end_date'] )   ? date( 'Y-m-d 23:59', current_time( 'timestamp' ) )    : $_POST['wp_hujsanje_export_racunovodstvo_end_date'] . ' 23:59:59.99',
					'after'     => empty( $_POST['wp_hujsanje_export_racunovodstvo_start_date'] ) ? date( 'Y-m-d 00:00', 0 ) : $_POST['wp_hujsanje_export_racunovodstvo_start_date'],
					'inclusive' => true,
				),
			),
		);

		// allow offset to be used with "no" posts limit
		if ( $query_args['offset'] > 0 && -1 === $query_args['posts_per_page'] ) {
			$query_args['posts_per_page'] = 999999999999; // a really large number {@link http://dev.mysql.com/doc/refman/5.7/en/select.html#idm140195560794688}
		}

		/**
		 * Allow actors to change the WP_Query args used for selecting orders to export in the admin.
		 *
		 * @since 3.0.6
		 * @param array $query_args - WP_Query arguments
		 * @param string $export_type - either `customers` or `orders`
		 * @param \WC_Customer_Order_CSV_Export_Admin $this class instance
		 */
		$query_args = apply_filters( 'wp_hujsanje_export_racunovodstvo_admin_query_args', $query_args, $export_type, $this );

		// get order IDs
		$query = new WP_Query( $query_args );

		$order_ids = $query->posts;

		if ( count( $order_ids ) ) {

			ob_start();
			// TODO - make text dinamic and with more information for user
			?>

			<h2><?php __( 'Custom set filter', 'wher' ); ?></h2>
			<p>Your filter options are ...</p>

			<?php
			$content = ob_get_clean();

			// TODO - set export params for later use
			$export_params = array(
				'export_type' => $export_type,
				'content' => $content,
			);

			$export_id = $this->create_export_session( $order_ids, $export_params );
			$this->redirect_to_export_download_page( $export_id );

		} else {

			$this->add_message( sprintf( __( 'No %s found to export', 'wher' ), $export_type ) );
		}
	}

	private function create_export_session( $order_ids, $params ){
		
		$export_id = uniqid();

		$this->set_session_data(
			$export_id,
			array(
				// export params for later use
				'params' => $params,
				// order ids
				'order_ids' => $order_ids,
			)
		);
		
		return $export_id;
	}

	private function export_page_url(){
		return admin_url( 'admin.php?page=wp_hujsanje_export_racunovodstvo' );
	}

	private function export_download_page_url( $export_id ){
		return add_query_arg( 'export-id', $export_id, $this->export_page_url() );
	}

	private function redirect_to_export_download_page( $export_id ){
		wp_redirect( $this->export_download_page_url( $export_id ) );
		exit();

	}
	
	/*************************************************
			PROCESS PREPARE EXPORT - END
	*************************************************/




	/*************************************************
			PROCESS DOWNLOAD EXPORT - START
	*************************************************/
	
	public function process_download_export(){

		if( ! isset( $_GET['export-id'] ) || ! isset( $_GET['download'] ) ){
			return;
		}

		// security check
		$nonce = isset( $_GET['_wpnonce'] ) ? $_GET['_wpnonce'] : '';
		if ( ! wp_verify_nonce( $nonce, __FILE__ ) ) {
			wp_die( __( 'Action failed. Please return to export form page, prepare new export and retry.', 'wher' ) );
		}

		$export_id = $_GET['export-id'];
		
		$session_data = $this->get_session_data( $export_id );
		if( is_null( $session_data ) ){
			wp_die( __( 'Requested data not found in session any more. Please return to export form page, prepare new export and retry.', 'wher' ) );
		}
		
		// get data from session
		$order_ids = $session_data['order_ids'];

		// get data from session
		$export_type = $session_data['params']['export_type'];

		// check download file type
		$download_file_type = isset( $_GET['download'] ) && in_array( $_GET['download'], array( 'clients', 'orders', 'orders-items' ) ) ? $_GET['download'] : false;
		if( ! $download_file_type ){
			wp_die( __( 'We can not export requested file type', 'wher' ) );
		}
		
		require_once dirname( __FILE__ ) . '/wp-hujsanje-export-racunovodstvo-handler.php';
		$export = new Wp_Hujsanje_Export_Strozic_Handler( $order_ids, $export_type, $download_file_type );
		$export->download();

	}
	
	/*************************************************
			PROCESS DOWNLOAD EXPORT - END
	*************************************************/




	/*************************************************
			EXPORT SETTINGS - START
	*************************************************/
	
	public static function get_export_settings() {

		$order_statuses = wc_get_order_statuses();

		$settings = array(

			array(
				'name' => __( 'Export', 'wher' ),
				'type' => 'title',
			),

			array(
				'id'      => 'wp_hujsanje_export_racunovodstvo_type',
				'name'    => __( 'Export for', 'wher' ),
				'type'    => 'radio',
				'options' => array(
					'storzic' => __( 'Storžič software', 'wher' ),
				),
				'default'  => 'storzic',
			),

			array( 'type' => 'sectionend' ),

			array(
				'name' => __( 'Export Options', 'wher' ),
				'type' => 'title',
			),

			array(
				'id'       => 'wp_hujsanje_export_racunovodstvo_statuses',
				'name'     => __( 'Order Statuses', 'wher' ),
				'desc_tip' => __( 'Orders with these statuses will be included in the export.', 'wher' ),
				'type'     => 'multiselect',
				'options'  => $order_statuses,
				'default'  => '',
				'class'    => 'wc-enhanced-select chosen_select show_if_orders',
				'css'      => 'min-width: 250px',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_start_date',
				'name' => __( 'Start Date', 'wher' ),
				'desc' => __( 'Start date of customers or orders to include in the exported file, in the format <code>YYYY-MM-DD.</code>', 'wher' ),
				'type' => 'text',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_end_date',
				'name' => __( 'End Date', 'wher' ),
				'desc' => __( 'End date of customers or orders to include in the exported file, in the format <code>YYYY-MM-DD.</code>', 'wher' ),
				'type' => 'text',
			),

			array(
				'id'                => 'wp_hujsanje_export_racunovodstvo_limit',
				'name'              => __( 'Limit Records', 'wher' ),
				'desc'              => __( 'Limit the number of rows to be exported. Use this option when exporting very large files that are unable to complete in a single attempt.', 'wher' ),
				'type'              => 'number',
				'custom_attributes' => array(
					'min' => 0,
				),
			),

			array(
				'id'                => 'wp_hujsanje_export_racunovodstvo_offset',
				'name'              => __( 'Offset Records', 'wher' ),
				'desc'              => __( 'Set the number of records to be skipped in this export. Use this option when exporting very large files that are unable to complete in a single attempt.', 'wher' ),
				'type'              => 'number',
				'custom_attributes' => array(
					'min' => 0,
				),
			),

			array( 'type' => 'sectionend' ),

		);

		/**
		 * Allow actors to add or remove settings from the CSV export pages.
		 *
		 * @since 3.0.6
		 * @param array $settings an array of settings for the given tab
		 * @param string $tab_id current tab ID
		 */
		return $settings;
	}

	public static function get_settings_settings() {

		$settings = array(

			array(
				'name' => __( 'Export user info', 'wher' ),
				'type' => 'title',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_CODE',
				'name' => __( 'Export id', 'wher' ),
				'desc' => __( 'What client id should be used on export files for customer. Default: "1"', 'wher' ),
				'type' => 'text',
				'default' => '1',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_DISPLAY',
				'name' => __( 'Display name', 'wher' ),
				'desc' => __( 'Display name for internal use in Strožič software. Default: "web store"', 'wher' ),
				'type' => 'text',
				'default' => 'web store',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_NAME',
				'name' => __( 'Official name, surname, company name', 'wher' ),
				'desc' => __( 'Official clients or company name. Max 80 characters will be exported. Default: "web store"', 'wher' ),
				'type' => 'text',
				'default' => 'web store',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_ADDRESS',
				'name' => __( 'Address', 'wher' ),
				'desc' => __( 'Client street address. Only 50 characters will be exported. Default: ""', 'wher' ),
				'type' => 'text',
				'default' => '',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_CNT_CODE',
				'name' => __( 'Country code', 'wher' ),
				'desc' => __( 'Strožič software country code (Strožič software developer must provide that info). Code must be 3 characters long. Default: "SLO"', 'wher' ),
				'type' => 'text',
				'default' => 'SLO',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_ZIP_ID',
				'name' => __( 'Zip code', 'wher' ),
				'desc' => __( 'Zip code for customer. Max 10 characters long. For SLO use only zip code, for HR or other countries, use "HR-10000".', 'wher' ),
				'type' => 'text',
				'default' => '',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_TAXNUM',
				'name' => __( 'Tax number', 'wher' ),
				'desc' => __( 'Clients tax number. Example: "SI99999999". This field can be empty. Default: ""', 'wher' ),
				'type' => 'text',
				'default' => '',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_TAXCUST',
				'name' => __( 'Is tax customer', 'wher' ),
				'desc' => __( 'If client is tax customer. 0 = no, 1 = yes. Default: "0"', 'wher' ),
				'type' => 'text',
				'default' => '0',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_FINCUST',
				'name' => __( 'Final customer', 'wher' ),
				'desc' => __( 'Is client final customer. 0 = no, 1 = yes. Default: "1"', 'wher' ),
				'type' => 'text',
				'default' => '1',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_TYPE',
				'name' => __( 'Client type', 'wher' ),
				'desc' => __( '1 = buyer, 2 = supplier, 3 = both. Default: "1"', 'wher' ),
				'type' => 'text',
				'default' => '1',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_PAYDUE',
				'name' => __( 'Pay due', 'wher' ),
				'desc' => __( 'Clients default pay due in days. Default: "7"', 'wher' ),
				'type' => 'text',
				'default' => '7',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_COM_ACCOUNT',
				'name' => __( 'Client bank account', 'wher' ),
				'desc' => __( 'Client account. Not requered. Default: ""', 'wher' ),
				'type' => 'text',
				'default' => '',
			),




			array( 'type' => 'sectionend' ),






			array(
				'name' => __( 'Other params', 'wher' ),
				'type' => 'title',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_INV_ID_COUNTRY_ID',
				'name' => __( 'Invoice export id country code', 'wher' ),
				'desc' => __( '3rd character in invoice export id<br>Invoice export id has 8 characters.<br>1st,2nd for invoice year (if year 2015, value is "15"),<br>3rd for country code, like 1 = SLO and 2 = HR,<br>4th,5th,6th,7th,8th for invoice number.<br>Invoice number is extracted from invoice PIP number ex "004856-2015", will become "4856".<br>Final Invoice export id for year 2015, country SLO and invoice id "004856-2015" will be "15" + <b>"1"</b> + "04856" => "15<b>1</b>04856"', 'wher' ),
				'type' => 'text',
				'default' => '1',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_CUR_ID',
				'name' => __( 'Currency code', 'wher' ),
				'desc' => __( 'Currency code in Storžič software. Must be 2 characters long. "01" for EUR. Default: "01"', 'wher' ),
				'type' => 'text',
				'default' => '01',
			),

			array(
				'id'   => 'wp_hujsanje_export_racunovodstvo_TAX_ID',
				'name' => __( 'Tax percent id', 'wher' ),
				'desc' => __( 'Tax id code. Must be 2 characters long. "01" = 0%, "02" = 9.5%, "03" = 22%. Default: "02"', 'wher' ),
				'type' => 'text',
				'default' => '02',
			),

			array( 'type' => 'sectionend' ),

		);

		/**
		 * Allow actors to add or remove settings from the CSV export pages.
		 *
		 * @since 3.0.6
		 * @param array $settings an array of settings for the given tab
		 * @param string $tab_id current tab ID
		 */
		return $settings;
	}
	
	/*************************************************
			EXPORT SETTINGS - END
	*************************************************/





	/*************************************************
			SYTLES AND SCRIPTS - START
	*************************************************/
	
	public function load_wc_styles_scripts( $screen_ids ) {

		$screen_ids[] = 'woocommerce_page_' . $this->page_slug;

		return $screen_ids;

	}

	public function load_styles_scripts( $hook_suffix ){
		global $wp_scripts;

		// only load on settings / view orders pages
		if ( $this->page == $hook_suffix || 'edit.php' == $hook_suffix ) {

			// Admin CSS
			wp_enqueue_style( 'wp-hujsanje-export-racunovodstvo_admin', plugin_dir_url(__FILE__) . 'assets/css/admin/wp-hujsanje-export-racunovodstvo-admin.min.css', array( 'dashicons' ), Wp_Hujsanje_Export_Strozic::VERSION );

			// settings/export page
			if ( $this->page == $hook_suffix ) {

				// jQuery Timepicker JS
				wp_enqueue_script( 'wp-hujsanje-export-racunovodstvo-jquery-timepicker', plugin_dir_url(__FILE__) . 'assets/js/jquery-timepicker/jquery.timepicker.min.js', array(), Wp_Hujsanje_Export_Strozic::VERSION, true );

				// admin JS
				wp_enqueue_script( 'wp-hujsanje-export-racunovodstvo-admin', plugin_dir_url(__FILE__) . 'assets/js/admin/wp-hujsanje-export-racunovodstvo-admin.min.js', array(), Wp_Hujsanje_Export_Strozic::VERSION, true );

				// calendar icon
				wp_localize_script( 'wp-hujsanje-export-racunovodstvo-admin', 'wp_hujsanje_export_racunovodstvo_admin_params', array( 'calendar_icon_url' => WC()->plugin_url() . '/assets/images/calendar.png' ) );

				// datepicker
				wp_enqueue_script( 'jquery-ui-datepicker' );

				// get jQuery UI version
				$jquery_version = isset( $wp_scripts->registered['jquery-ui-core']->ver ) ? $wp_scripts->registered['jquery-ui-core']->ver : '1.9.2';

				// enqueue UI CSS
				wp_enqueue_style( 'jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/' . $jquery_version . '/themes/smoothness/jquery-ui.css' );

				// enhanced-select
				wp_enqueue_script( 'wc-enhanced-select' );
			}
		}
	}
	
	/*************************************************
			SYTLES AND SCRIPTS - END
	*************************************************/




	/*************************************************
			ORDERS VIEW - START
	*************************************************/
	
	public function add_order_status_column_header( $columns ) {

		$new_columns = array();

		foreach ( $columns as $column_name => $column_info ) {

			$new_columns[ $column_name ] = $column_info;

			if ( 'order_status' == $column_name ) {

				$new_columns['storzic_export_status'] = __( 'Storžič Export Status', 'wher' );
			}
		}

		return $new_columns;
	}

	public function add_order_status_column_content( $column ) {
		global $post;

		if ( 'storzic_export_status' == $column ) {

			$order = wc_get_order( $post->ID );

			$is_exported = false;

			// if ( $order->wp_hujsanje_export_racunovodstvo_is_exported ) {
			if ( get_post_meta( $post->ID, '_wp_hujsanje_export_racunovodstvo_is_exported', true ) ) {

				$is_exported = true;
			}

			printf( '<mark class="%s">%s</mark>', $is_exported ? 'storzic_exported' : 'storzic_not_exported', $is_exported ? __( 'Exported', 'wher' ) : __( 'Not Exported', 'wher' ) );
		}
	}

	public function add_order_action( $order ) {

		// if ( ! $order->wp_hujsanje_export_racunovodstvo_is_exported ) {
		if ( ! get_post_meta( $order->get_id(), '_wp_hujsanje_export_racunovodstvo_is_exported', true ) ) {

			$order_export_url = add_query_arg( 'create-order-export', $order->id, $this->export_page_url() );

			$action = 'export_to_csv';
			$url = wp_nonce_url( $order_export_url, 'wp_hujsanje_export_racunovodstvo_export_order' );
			$name = __( 'Download for Storžič', 'wher' );

			printf( '<a class="button tips %s" href="%s" data-tip="%s" target="_blank">%s</a>', $action, esc_url( $url ), $name, $name );
		}

	}

	public function add_order_meta_box_actions( $actions ) {

		// add download to CSV action
		$actions['wp_hujsanje_export_racunovodstvo_download'] = __( 'Download for Storžič', 'wher' );

		return $actions;
	}

	public function process_order_meta_box_actions( $order ) {

		$url = add_query_arg( array( 'create-order-export' => $order->id, '_wpnonce' => wp_create_nonce('wp_hujsanje_export_racunovodstvo_export_order') ), $this->export_page_url() );
		wp_redirect( $url );
		exit();
	}

	public function filter_orders_by_export_status() {
		global $typenow;

		if ( 'shop_order' == $typenow ) :

			$count = $this->get_order_count();

			$terms = array(
				0 => (object) array( 'count' => $count['wher_not_exported'], 'term' => __( 'Storžič - Not Exported', 'wher' ) ),
				1 => (object) array( 'count' => $count['wher_exported'], 'term' => __( 'Storžič - Exported', 'wher' ) )
			);

			?>
		<select name="_wher_export_status" id="dropdown_shop_order_export_status">
			<option value=""><?php _e( 'Storžič - Show all orders', 'wher' ); ?></option>
			<?php foreach ( $terms as $value => $term ) : ?>
			<option value="<?php echo $value; ?>" <?php echo esc_attr( isset( $_GET['_wher_export_status'] ) ? selected( $value, $_GET['_wher_export_status'], false ) : '' ); ?>>
				<?php printf( '%s (%s)', $term->term, $term->count ); ?>
			</option>
			<?php endforeach; ?>
		</select>
		<?php

		endif;
	}

	public function filter_orders_by_export_status_query( $vars ) {
		global $typenow;

		if ( 'shop_order' == $typenow && isset( $_GET['_wher_export_status'] ) && is_numeric( $_GET['_wher_export_status'] ) ) {

			$vars['meta_key']   = '_wp_hujsanje_export_racunovodstvo_is_exported';
			$vars['meta_value'] = (int) $_GET['_wher_export_status'];
		}

		return $vars;
	}

	public function add_order_bulk_actions() {
		global $post_type, $post_status;

		if ( $post_type == 'shop_order' && $post_status != 'trash' ) {

			?>
			<script type="text/javascript">
				jQuery( document ).ready( function ( $ ) {
						  var $exported = $('<option>').val('wher_mark_exported').text('<?php _e( 'Storžič - Mark exported', 'wher' )?>'),
							  $not_exported = $('<option>').val('wher_mark_not_exported').text('<?php _e( 'Storžič - Mark not exported', 'wher' )?>'),
							  $download_to_csv = $('<option>').val('wher_download').text('<?php _e( 'Storžič - Download', 'wher' )?>');

						  $( 'select[name^="action"]' ).append( $exported, $not_exported, $download_to_csv );
					  });
			</script>
			<?php
		}
	}

	public function process_order_bulk_actions() {
		global $typenow;

		if ( 'shop_order' == $typenow ) {

			// get the action
			$wp_list_table = _get_list_table( 'WP_Posts_List_Table' );
			$action        = $wp_list_table->current_action();

			// return if not processing our actions
			if ( ! in_array( $action, array( 'wher_download', 'wher_mark_exported', 'wher_mark_not_exported' ) ) ) {

				return;
			}

			// security check
			check_admin_referer( 'bulk-posts' );

			// make sure order IDs are submitted
			if ( isset( $_REQUEST['post'] ) ) {

				$order_ids = array_map( 'absint', $_REQUEST['post'] );
			}

			// return if there are no orders to export
			if ( empty( $order_ids ) ) {

				return;
			}

			// give ourselves an unlimited timeout if possible
			@set_time_limit( 0 );

			if ( 'wher_download' == $action ) {
				
				$export_params = array(
					'export_type' => 'storzic',
					'content' => '<p>' . __( 'Custom picked orders from orders list.', 'wher' ) . '</p>',
				);

				$export_id = $this->create_export_session( $order_ids, $export_params );
				$this->redirect_to_export_download_page( $export_id );

			} else {

				// mark each order as exported / not exported
				foreach( $order_ids as $order_id ) {
					update_post_meta( $order_id, '_wp_hujsanje_export_racunovodstvo_is_exported', ( 'wher_mark_exported' == $action ) ? 1 : 0 );
				}
			}
		}
	}

	private function get_order_count() {

		$query_args = array(
			'fields'      => 'ids',
			'post_type'   => 'shop_order',
			'post_status' => isset( $_GET['post_status'] ) ? $_GET['post_status'] : 'any',
			'meta_query'  => array(
				array(
					'key'   => '_wp_hujsanje_export_racunovodstvo_is_exported',
					'value' => 0
				)
			),
			'nopaging'    => true,
		);

		$not_exported_query = new WP_Query( $query_args );

		$query_args['meta_query'][0]['value'] = 1;

		$exported_query = new WP_Query( $query_args );

		return array( 'wher_not_exported' => $not_exported_query->found_posts, 'wher_exported' => $exported_query->found_posts );
	}


	
	/*************************************************
			ORDERS VIEW - END
	*************************************************/





	/*************************************************
			MESSAGES - START
	*************************************************/
	
	public function show_messages(){
		if ( ! current_user_can( 'manage_woocommerce' ) ) {
			return;
		}

		if ( $this->error_count() > 0 )
			echo '<div id="wp-admin-message-handler-error" class="error"><ul><li><strong>' . implode( '</strong></li><li><strong>', $this->get_errors() ) . "</strong></li></ul></div>";

		if ( $this->message_count() > 0 )
			echo '<div id="wp-admin-message-handler-message"  class="updated"><ul><li><strong>' . implode( '</strong></li><li><strong>', $this->get_messages() ) . "</strong></li></ul></div>";
	}

	public function add_message( $message ){
		$this->messages[] = $message;
	}

	public function add_error( $error ) {
		$this->errors[] = $error;
	}

	public function get_messages() {
		return $this->messages;
	}

	public function get_errors() {
		return $this->errors;
	}

	public function message_count() {
		return sizeof( $this->messages );
	}

	public function error_count() {
		return sizeof( $this->errors );
	}
	
	/*************************************************
			MESSAGES - END
	*************************************************/



	/*************************************************
			UTILS - START
	*************************************************/
	
	private $active_plugins;
	private function get_active_plugins(){
		if( ! isset( $this->active_plugins ) ){
			$this->active_plugins = (array) get_option( 'active_plugins', array() );

			if ( is_multisite() )
				$this->active_plugins = array_merge( $this->active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		return $this->active_plugins;
	}

	private function is_woocommerce_active(){
		return in_array( 'woocommerce/woocommerce.php', $this->get_active_plugins() ) || array_key_exists( 'woocommerce/woocommerce.php', $this->get_active_plugins() );
	}
	
	/*************************************************
			UTILS - END
	*************************************************/
}
