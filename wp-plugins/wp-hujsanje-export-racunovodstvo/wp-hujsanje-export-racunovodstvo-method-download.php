<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

require_once dirname( __FILE__ ) . '/wp-hujsanje-export-racunovodstvo-method.php';
class Wp_Hujsanje_Export_Racunovodstvo_Method_Download implements Wp_Hujsanje_Export_Racunovodstvo_Method {


	private function set_header( $filename ){
		// set headers for download
		header( 'Content-Type: text/plain; charset=' . get_option( 'blog_charset' ) );
		header( sprintf( 'Content-Disposition: attachment; filename="%s"', $filename ) );
		header( 'Pragma: no-cache' );
		header( 'Expires: 0' );

		// clear the output buffer
		@ini_set( 'zlib.output_compression', 'Off' );
		@ini_set( 'output_buffering', 'Off' );
		@ini_set( 'output_handler', '' );
	}

	public function perform_action( $filename, $file_content ) {

		if( defined('WHER_OUTPUT_DOWNLOAD') && WHER_OUTPUT_DOWNLOAD ){
			header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
			$file_content = str_replace( ' ', '&nbsp;', $file_content );
			$file_content = nl2br( $file_content );
		} else {
			$this->set_header( $filename );
		}

		// open the output buffer for writing
		$fp = fopen( 'php://output', 'w' );

		// write the generated CSV to the output buffer
		fwrite( $fp, $file_content );

		// close the output buffer
		fclose( $fp );

		exit;
	}
}
