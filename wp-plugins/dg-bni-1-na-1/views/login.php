<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}


/******************************************************************
*
*       LOGIN CONTENT
*
******************************************************************/
?>


<?php
/******************************************************************
*
*       DESCRIPTION TEXT
*
******************************************************************/
?>
<div>
	<!-- TODO - text / nagovor -->
	<h3>Dobrodošli v spletnem orodju BNI skupine Karantanija.</h3>
	<p>Orodje je razvito z namenom pomagati najboljši skupini BNI postati še boljša ...</p>
</div>






<?php
/******************************************************************
*
*       LOST PASSWORD COLUMN
*
******************************************************************/
?>
<div class="one-half first">
	<!-- TODO - tekst naslov in opis -->
	<h3>Ste prvič tu? Ste pozabili geslo?</h3>
	<p>
		Kot članu BNI Karantanije je tvoj uporabniški račun že pripravljen za uporabo. Vse kar moraš storiti je, da vpišeš svoj email v spodnje polje in klikneš na NASTAVI GESLO.
	</p>
	<?php
	$is_reset = ( isset( $_GET['reset'] ) && 'true' == $_GET['reset'] );

	if( $is_reset ){
		?>
		<div class="alert alert-success">
			<h3>Uspešno ste oddali zahtevo</h3>
			<p>
				V kratkem boste prejeli email sporočilo z navodili za dostop / ponastavitev gesla.
			</p>
			<p>
				Za vsak slučaj preverite tudi v mapi "Nezaželena pošta".
			</p>
		</div>
		<?php
	} else {
		?>
		<form method="post" action="<?php echo site_url('wp-login.php?action=lostpassword', 'login_post') ?>" class="wp-user-form">
			<div class="username">
				<label for="user_login" class="hide">
					Email
				</label>
				<input type="text" name="user_login" value="" size="20" id="user_login" tabindex="1001" />
				<small>(Uporabi enak email, kot ga uporabljaš za BNI Connect)</small>
			</div>
			<div class="login_fields">
				<?php do_action('login_form', 'resetpass'); ?>

				<input type="submit" name="user-submit" value="<?php echo esc_attr( 'Nastavi geslo' ); ?>" class="user-submit" tabindex="1002" />

				<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>?reset=true" />
				<input type="hidden" name="user-cookie" value="1" />
			</div>
		</form>
		<?php
	}
	?>
</div>





<?php
/******************************************************************
*
*       LOGIN COLUMN
*
******************************************************************/
?>
<div class="one-half">
	<!-- TODO - tekst naslov in opis -->
	<h3>Prijavi se</h3>
	<p>
		Če ste v aplikacijo že vstopali in imate geslo za vstop, dobrodošli.
	</p>
	<?php
		$login_args = array(
			'label_username' => 'Email',
			'label_password' => 'Geslo',
			'label_remember' => 'Zapomni si me',
			'label_log_in' => 'Vstopi',
			'remember' => true
			);
		wp_login_form( $login_args );
	?>	
</div>