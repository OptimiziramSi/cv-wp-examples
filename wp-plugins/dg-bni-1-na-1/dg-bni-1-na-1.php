<?php
/**
* Plugin Name: BNI orodje za usklajevanje sestankov 1 na 1
* Description: Pripomoček, ki pomaga pri suklajevanju sestankov 1 na 1
* Plugin URI: http://2gika.si
* Author: 2gika
* Author URI: http://2gika.si
* Version: 1.0
* License: GPL2
* Text Domain: b121
* Domain Path: languages
*/

/*
Copyright (C) 2016  2gika  info@2gika.si

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/**
 * Snippet Wordpress Plugin Boilerplate based on:
 *
 * - https://github.com/purplefish32/sublime-text-2-wordpress/blob/master/Snippets/Plugin_Head.sublime-snippet
 * - http://wordpress.stackexchange.com/questions/25910/uninstall-activate-deactivate-a-plugin-typical-features-how-to/25979#25979
 *
 * By default the option to uninstall the plugin is disabled,
 * to use uncomment or remove if not used.
 *
 * This Template does not have the necessary code for use in multisite. 
 *
 * Also delete this comment block is unnecessary once you have read.
 *
 * Version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'plugins_loaded', array( 'DgBni1na1', 'getInstance' ) );
register_activation_hook( __FILE__, array( 'DgBni1na1', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'DgBni1na1', 'deactivate' ) );
// register_uninstall_hook( __FILE__, array( 'DgBni1na1', 'uninstall' ) );

define( 'BNI_1na1_FILE', __FILE__ );

require_once dirname( BNI_1na1_FILE ) . '/users/users.php';
require_once dirname( BNI_1na1_FILE ) . '/shortcode/shortcode.php';
require_once dirname( BNI_1na1_FILE ) . '/theme/theme.php';

// TODO - spremeni email sporočilo za ponastavitev gesla in dodaj nek jasen opis
// TODO - login forma čudno poravnava REMEBER ME checkbox
// TODO - wp-login.php naj vedno preusmeri na "aplikacijo", razen za formo, kjer ponastavljaš geslo.
// TODO - v primeru, da je password reset imel napako, preusmeri nazaj na "aplikacijo", in ne da pristanemo na LOGIN povezavi
// TODO - skrij admin vrstico za navadne uporabnike
// TODO - prikaži gumb LOGOUT
// TODO - pomanjšaj header
// TODO - na splošno po potrebi spremeni design
// TODO - edit mode omogočimo samo na desktop-u / ni touch friendly in ni mobile friendly
// TODO - meeting mode je mobile in touch friendly

class DgBni1na1 {

	private static $instance = null;

	public static function getInstance() {
		if ( ! isset( self::$instance ) )
			self::$instance = new self;

		return self::$instance;
	}

	private function __construct() {

		DgBni1na1_Theme::getInstance();
		DgBni1na1_Users::getInstance();
		DgBni1na1_Shortcode::getInstance();

		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts_and_styles' ) );

	}

	function wp_enqueue_scripts_and_styles(){
		wp_enqueue_script('bni-1-na-1', plugins_url('js/front.js', BNI_1na1_FILE), array('jquery') );
		wp_enqueue_style('bni-1-na-1', plugins_url('css/front.css', BNI_1na1_FILE) );
	}

	public static function activate() {}

	public static function deactivate() {}

/*
	public static function uninstall() {
		if ( __FILE__ != WP_UNINSTALL_PLUGIN )
			return;
	}
*/
}
